%% -*- coding: utf-8 -*-

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{NTNUeksamen}[2014/05/04 Klasse for å skrive eksamenssett ved NTNU]
\newtoks\langinfo\langinfo{}
\DeclareOption{langinfo}{\langinfo{\LANGINFO}}
\newif\ifoldcommands\oldcommandsfalse
\DeclareOption{oldcommands}{\oldcommandstrue}
\newif\ifoldlanguageselector\oldlanguageselectorfalse
\DeclareOption{oldlanguageselector}{\oldlanguageselectortrue}
\newif\ifnotitlepage\notitlepagetrue
\DeclareOption{titlepage}{\notitlepagefalse}
\newif\ifnotitle\notitlefalse
\DeclareOption{notitlepage}{\notitletrue}
\newif\if@oneside\@onesidefalse
\DeclareOption{oneside}{\PassOptionsToClass{oneside}{report}\@onesidetrue}
\def\@logo{NTNU-sort}
\DeclareOption{colorlogo}{\def\@logo{NTNU-blaa}}
\DeclareOption{colourlogo}{\def\@logo{NTNU-blaa}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions
\ifnotitlepage\else\ifnotitle\ClassError{NTNUeksamen}
  {Both titlepage and notitlepage present}
  {These options cannot be combined. Remove one or both and try again.}
  \notitlepagetrue\fi\fi
\LoadClass{report}
\ifnotitlepage\else\if@oneside\else\@twosidetrue\fi\fi
\advance\oddsidemargin-40pt \evensidemargin\oddsidemargin
\advance\textwidth90pt
\RequirePackage{babel}
\RequirePackage{graphicx}
\RequirePackage{etoolbox}
\RequirePackage{silence}
\WarningsOff[pageslts]
\RequirePackage[pagecontinue=false]{pageslts}
\AtBeginDocument{\WarningsOn[pageslts]}

\InputIfFileExists{NTNUeksamen.cfg}{}{\message{No file NTNUeksamen.cfg.}}

\ifnotitlepage\else\RequirePackage{geometry}\fi
\newcommand{\NTNUemblem}[1][16mm]{\includegraphics[height=#1]{ntnulogo}}
\newcommand{\NTNUlogo}[1][]{\includegraphics[#1]{\@logo-\@lang}}

\AtBeginDocument{\pagestyle{eks}\ifnotitle\else\thispagestyle{Eks}\fi}

{\escapechar=-1
 \let~\expandafter
  \def\do#1{\xdef#1{\gdef~\noexpand\csname @\string#1\endcsname}}
 \do\emnekode
 \do\emnenavn
 \do\eksamensdato
 \do\eksamenstid
 \do\hjelpemiddel
 \do\anneninfo
 \do\vedleggsider
 \do\antallsider
 \do\runninghead
}
\antallsider{\lastpageref{pagesLTS.arabic}}

\def\@fagligkontakt{}
\def\@fagligtelefon{}
\newif\if@faglig\@fagligfalse
\newif\if@multifaglig
\def\@sjekktelefon{\if@faglig\ClassError{NTNUeksamen}
  {Faglig kontakt mangler telefon}
  {Angi navn og telefon for hver faglig kontakt slik:
    \string\fagligkontakt{...}\string\fagligtelefon{...}.}\fi}
\AtBeginDocument{\@sjekktelefon}
\newcommand{\fagligkontakt}[1]{\@sjekktelefon\addto\@fagligkontakt{{#1}}\@fagligtrue}
\newcommand{\fagligtelefon}[1]{\if@faglig\else\ClassError{NTNUeksamen}
  {Telefon mangler faglig kontakt}
  {Angi navn og telefon for hver faglig kontakt slik:
    \string\fagligkontakt{...}\string\fagligtelefon{...}.}\fi
  \addto\@fagligtelefon{{#1}}\@fagligfalse}
\newcommand{\fagligkontaktinfo}[2]{\fagligkontakt{#1}\fagligtelefon{#2}}

\newif\ifvedlegg\vedleggfalse
\newcommand{\vedlegg}{\par\newpage
  \pagestyle{vedlegg}
  \pagenumbering{roman}
  \write\@mainaux{\global\noexpand\vedleggtrue}}
\vedleggsider{\ifvedlegg\lastpageref{pagesLTS.roman.local}\else0\fi}

\setlength{\parindent}{0pt}
\setlength{\parskip}{12pt plus 6pt}

\newtoks\firstpageupperright
\firstpageupperright{\PageText{\thepage}{\lastpageref{pagesLTS.arabic}}}

%\footline skal normalt være tom, men kan brukes til
%versjonsinformasjon for utkast
\def\footline{}
\def\ps@EKS{\let\@mkboth\@gobbletwo
     \def\@oddhead{}%
     \def\@oddfoot{\vbox to 0pt{\fontfamily{phv}
         \footnotesize\vss\hrule\kern2pt\TitleFoot\par\kern-12pt}}%
     \let\@evenfoot\@oddfoot
     \let\@evenhead\@oddhead}
\def\ps@Eks{\let\@mkboth\@gobbletwo
     \def\@oddhead{\vtop to 0pt{\normalsize\openup 2pt\bf
                   \hbox{\NTNUa}
                   \hbox{\NTNUi}
                   \vss}
                   \hfill \vtop to 0pt{\def\\{\cr}
                            \halign{\strut\hfil\sl####\cr
                                    \the\firstpageupperright\cr
                                    \noalign{\kern 24pt}\llap{\NTNUemblem}\cr
                                    \noalign{\kern 12pt}\the\langinfo\cr}\vss}}
     \let\@oddfoot\footline\let\@evenfoot\footline
     \let\@evenhead\@oddhead}
\def\ps@eks{\let\@mkboth\@gobbletwo
     \def\@oddhead{\textsl{\@runninghead\hfill
                           \PageText{\thepage}{\lastpageref{pagesLTS.arabic}}}}
     \def\@evenhead{\textsl{\PageText{\thepage}{\lastpageref{pagesLTS.arabic}}\hfill
                            \@runninghead}}
     \let\@oddfoot\footline\let\@evenfoot\footline}
\def\ps@vedlegg{\let\@mkboth\@gobbletwo
     \def\@oddhead{\textsl{\@runninghead\hfill
                           \PageText{\thepage}{\lastpageref{pagesLTS.roman}}}}
     \def\@evenhead{\textsl{\PageText{\thepage}{\lastpageref{pagesLTS.roman}}\hfill
                            \@runninghead}}
     \let\@oddfoot\footline\let\@evenfoot\footline}

\newcommand{\Eksamen}[2][20mm]{\par{\parskip=#1 \centering\large #2\par}}

\ifnotitlepage
\AtBeginDocument{\pagenumbering{arabic}}
\else
\AfterEndPreamble{\maketitlepage}
\newcommand{\maketitlepage}{
  \pagenumbering{Alph}
  \newgeometry{left=22mm,right=16mm,top=20mm,bottom=25mm,%
    includehead=false,includefoot=false}
  \vskip-34mm
  \thispagestyle{EKS}
  \if@titlepage\else\errmsg{foo!}\fi
  \vtop to\textheight
        {
  \fontfamily{phv}\selectfont
  \newcommand{\newitem}{\ifhmode\vadjust{\vspace{2mm}}\newline\fi}
  \NTNUlogo

  \vspace{5mm minus 2mm}
  \NTNUi

  \bfseries
  \vspace{19mm minus 6mm}
  \vbox to 28mm{
    \Large
    \def\0{{\mdseries\EksOppg} \@emnekode\space\@emnenavn}
    \setbox0\hbox{\0}
    \ifdim\wd0>\textwidth
      {\mdseries\EksOppg}\newline\@emnekode\space\@emnenavn\par
    \else\box0\fi
  \vss}

  \ifx\@fagligkontakt\@empty
    \def\@fagligkontakt{{}}
    \def\@fagligtelefon{{}}
    \ClassWarning{NTNUeksamen}{Faglig kontakt er ikke oppgitt.}
  \fi
  \ifx\@fagligtelefon\@empty\def\@fagligtelefon{{}}\fi
  \newcounter{faglig}\renewcommand{\thefaglig}{\alph{faglig}}
  \@multifagligfalse
  \def\faglige##1##2{%
    \if@multifaglig, \else\ifx\relax##2\relax\else\@multifagligtrue\fi\fi
    ##1\if@multifaglig\stepcounter{faglig}\textsuperscript{\,\thefaglig}\fi
    \ifx\relax##2\relax\else
      \def\next{\faglige{##2}}%
    \expandafter\next\fi}
  \FaglKont: {\mdseries\expandafter\faglige\@fagligkontakt{}}\newitem
  \setcounter{faglig}{0}
  \@multifagligfalse
  \def\faglige##1##2{%
    \message{[\string\faglige{##1}{##2}]}
    \if@multifaglig, \else\ifx\relax##2\relax\else\@multifagligtrue\fi\fi
    \if@multifaglig\stepcounter{faglig}\textsuperscript{\thefaglig\,}\fi##1%
    \ifx\relax##2\relax\else
      \def\next{\faglige{##2}}%
    \expandafter\next\fi}
  \Tlf: {\mdseries\spaceskip=0.667\fontdimen2\font\expandafter\faglige\@fagligtelefon{}}

  \vspace{13mm minus 6mm}
  \EksDato: {\mdseries\@eksamensdato}\newitem
  \EksTid: {\mdseries\@eksamenstid}\newitem
  \HjelpeMiddel: {\mdseries \@hjelpemiddel}

  \ifx\@anneninfo\undefined\else
  \vspace{3mm}
  \AnnenInfo:\newline{\mdseries\@anneninfo\par}
  \fi

  \vspace{18mm minus 15mm}
  \MalForm\newitem
  \AntallSider: {\mdseries \@antallsider}\newitem
  \AntallSiderVedlegg: {\mdseries \@vedleggsider}

  \vfill
  \raggedleft
  \KontrollertAv:\par\vspace{10mm}
  \mdseries\leavevmode
  \vbox{\leftskip\z@\hsize=65mm \hrule \quad\Dato\hfill\Sign\hfill\null\par}
  \par
  }
  {\pagestyle{empty}\cleardoublepage}\pagenumbering{arabic}
  \restoregeometry
}
\fi

\renewcommand{\theequation}{\@arabic\c@equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Fix a bug in babel's addto:
%%% Stuffing text in a token register doubles any # therein (?)
%%% so if the second arg contained any #, is was handled differently
%%% depending on whether #1 was defined or not.
\def\addto#1#2{{\ifx#1\undefined\toks@{#2}\else\toks@\expandafter{#1#2}\fi
                \xdef#1{\the\toks@}}}
\addto\extrasnorsk{%
  \def\@lang{nb}%
  \def\EksOppg{Eksamensoppgave i}%
  \def\FaglKont{Faglig kontakt under eksamen}%
  \def\Tlf{Tlf}%
  \def\EksDato{Eksamensdato}%
  \def\EksTid{Eksamenstid (fra--til)}%
  \def\HjelpeMiddel{Hjelpemiddelkode/Tillatte hjelpemidler}%
  \def\AnnenInfo{Annen informasjon}%
  \def\malform{bokm{\aa}l}%
  \def\MalForm{M{\aa}lform/spr{\aa}k: {\mdseries\malform}}%
  \def\AntallSider{Antall sider}%
  \def\AntallSiderVedlegg{Antall sider vedlegg}%
  \def\KontrollertAv{Kontrollert av}%
  \def\Dato{Dato}\def\Sign{Sign}%
  \def\PageText#1#2{Side #1 av #2}%
  \def\OPPGAVE{Oppgave}%
  \def\NTNUa{Norges teknisk--naturvitenskapelige universitet}%
  \def\NTNUi@d{Sett instituttnavn med \string\institutt-kommandoen}%
  \def\LANGINFO{Bokm{\aa}l}%
  \def\TitleFoot{Merk! Studenter finner sensur i Studentweb.
    Har du sp{\o}rsm{\aa}l om din sensur
    m{\aa} du kontakte instituttet ditt.
    Eksamenskontoret vil ikke kunne svare p{\aa} slike sp{\o}rsm{\aa}l.}%
}
\addto\extrasnynorsk{%
  \def\@lang{nn}%
  \def\EksOppg{Eksamensoppg{\aa}ve i}%
  \def\FaglKont{Fagleg kontakt under eksamen}%
  \def\Tlf{Tlf}%
  \def\EksDato{Eksamensdato}%
  \def\EksTid{Eksamenstid (fr{\aa}--til)}%
  \def\HjelpeMiddel{Hjelpemiddelkode/Tillatne hjelpemiddel}%
  \def\AnnenInfo{Annan informasjon}%
  \def\malform{nynorsk}%
  \def\MalForm{M{\aa}lform/spr{\aa}k: {\mdseries\malform}}%
  \def\AntallSider{Sidetal}%
  \def\AntallSiderVedlegg{Sidetal vedlegg}%
  \def\KontrollertAv{Kontrollert av}%
  \def\Dato{Dato}\def\Sign{Sign}%
  \def\PageText#1#2{Side #1 av #2}%
  \def\OPPGAVE{Oppg{\aa}ve}%
  \def\NTNUa{Noregs teknisk--naturvitskaplege universitet}%
  \def\NTNUi@d{Set instituttnamn med \string\institutt-kommandoen}%
  \def\LANGINFO{Nynorsk}%
  \def\TitleFoot{Merk! Studentane finn sensur i Studentweb.
    Har du sp{\o}rsm{\aa}l om sensuren
    m{\aa} du kontakte instituttet ditt.
    Eksamenskontoret vil ikkje kunne svare p{\aa} slike sp{\o}rsm{\aa}l.}%
}
\addto\extrasenglish{%
  \def\@lang{en}%
  \def\EksOppg{Examination paper for}%
  \def\FaglKont{Academic contact during examination}%
  \def\Tlf{Phone}%
  \def\EksDato{Examination date}%
  \def\EksTid{Examination time (from--to)}%
  \def\HjelpeMiddel{Permitted examination support material}%
  \def\AnnenInfo{Other information}%
  \def\malform{English}%
  \def\MalForm{Language: {\mdseries\malform}}%
  \def\AntallSider{Number of pages}%
  \def\AntallSiderVedlegg{Number pages enclosed}%
  \def\KontrollertAv{Checked by}%
  \def\Dato{Date}\def\Sign{Signature}%
  \def\PageText#1#2{Page #1 of #2}%
  \def\OPPGAVE{Problem}%
  \def\NTNUa{Norwegian University of Science and Technology}%
  \def\NTNUi@d{Set Department name with the \string\department\space command}%
  \def\LANGINFO{English}%
  \def\TitleFoot{}%
}
\let\NTNUi\NTNUi@d
\newcommand{\institutt}[1]{\def\NTNUi{#1}}
\let\department\institutt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Oppgaver
\newcounter{Oppg}
\newenvironment{oppgave}[1][]{\par
    \refstepcounter{Oppg}
    \ifnum\c@Oppg=1 \bigskip\bigskip\fi % Ekstra luft foran oppgave 1
    #1{\bf \OPPGAVE\ \theOppg}\qquad}%
  {\par\goodbreak\bigskip}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Punkter
\newcounter{Punkt}[Oppg]
\renewcommand{\thePunkt}{\alph{Punkt}}
\newcommand{\Punktkolon}{)}
\newcommand{\punktkolon}[1]{\renewcommand{\Punktkolon}{#1}}
\newcommand{\PunktInn}{2em}

\newenvironment{punkt}%
 {\refstepcounter{Punkt}%
  \list{}{\setlength{\topsep}{0pt}}
  \item[{\bf \thePunkt\Punktkolon}]}
 {\endlist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Bakoverkompatibilitet.
\ifoldcommands % kan velges med klasseopsjon: oldcommands
  \let\Oppgave\oppgave
  \let\endOppgave\endoppgave
  \newcommand{\bOppgave}{\begin{oppgave}}
  \newcommand{\eOppgave}{\end{oppgave}}
  \let\Punkt\punkt
  \let\endPunkt\endpunkt
  \newcommand{\bPunkt}{\begin{punkt}}
  \newcommand{\ePunkt}{\end{punkt}}
  \def\Start{\message{Advarsel: Avlegs kommando \string\Start\space ignorert.}}
  \def\Slutt{\message{Advarsel: Avlegs kommando \string\Slutt\space ignorert.}}
\fi
\ifoldlanguageselector
  \def\BOKMAAL{\selectlanguage{norsk}}
  \let\NORSK=\BOKMAAL
  \def\NYNORSK{\selectlanguage{nynorsk}}
  \def\ENGLISH{\selectlanguage{english}}
  \let\ENGELSK=\ENGLISH
\fi
