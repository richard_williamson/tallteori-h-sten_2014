\documentclass[a4paper]{scrartcl}

\usepackage{../../tallteori}

\externaldocument{../../tallteori}

\begin{document}

\title{Oversikt over bevis at det finnes uendelig mange primtall med bestemte egenskaper}
\date{\today}
\author{Richard Williamson}
 
\maketitle

\section*{Oppgave 1}

La $n$ være et naturlig tall. Bevis at det finnes et primtall $p$ slik at $p > n$ og $p \equiv 3 \pmod{4}$. {\em Tips}: La $q$ være produktet til alle primtallene som er mindre enn eller like $n$, og som er kongruent til $3$ modulo $4$. Benytt en primtallsfaktorisering til $4q - 1$.

\section*{Hvordan løse oppgaven?}

Begynnelsen og slutten på en slik oppgave er alltid den samme. Vi begynner med å benytte oss av aritmetikkens fundamentalteorem: det finnes en primtallsfaktorisering til $4q - 1$. Det vil si: det finnes et naturlig tall $t$ og primtall $p_{1}$, $p_{2}$, \ldots, $p_{t}$ slik at \[ 4q -1 = p_{1} p_{2} \cdots p_{t}. \] Nå ønsker vi å bevise at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ har det samme egenskapet som står i oppgaven, altså at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ er kongruent til 3 modulo 4. 

\section*{Hvorfor kan vi fullføre beviset gitt at dette er sant?}

La oss anta at $p_{i}$ har det samme egenskapet som står i oppgaven, altså at \[ p_{i} \equiv 3 \pmod{4}. \] Vi gjennomfører følgende argument for å vise at $p_{i} > n$, som i alt vesentlig er det samme når vi løser en hvilken som helst lignende oppgave. %
%
\begin{itemize}

\item[(1)] Anta at $p_{i} \leq n$. Vi ønsker å vise at dette fører til en motsigelse.

\item[(2)] Siden $p_{i}$ har det samme egenskapet som står i oppgaven, er $p_{i}$ et ledd av $q$. Derfor har vi: $p_{i} \mid q$.

\item[(3)] Siden $p_{i}$ er i tillegg et ledd av primtallsfaktoriseringen til $4q-1$, har vi i tillegg: $p_{i} \mid 4q -1$.  

\item[(4)] Vi benytter (2) og  (3) for å vise at $p_{i} \mid 1$. Vi kan for eksempel observere at det følger fra (2) at $p_{i} \mid 4q$, og at det følger fra (3) at $p_{i} \mid -(4q -1)$. Da følger det at \[ p_{i} \mid 4q + (-(4q - 1)), \] altså at $p_{i} \mid 1$.

\item[(5)] Nå observere vi at vi har en motsigelse: siden $p_{i} \mid 1$, er $p_{i} \leq 1$, men siden $p_{i}$ er et primtall, er $p_{i} \geq 2$. 

\end{itemize} %
%
Siden antakelsen at $p_{i} \leq n$ fører til en motsigelse, konkluderer vi at det ikke er sant at $p_{i} \leq n$, altså at $p_{i} > n$. 

\section*{Hvordan kan jeg bli fortrolig med dette argumentet?}

Prøv å gjennomføre det i alle de seks eksemplene på påstand som ligner på Oppgave 1 som vi har sett i kurset. %
%
\begin{itemize}

\item[(1)] Benytt en primtallsfaktorisering til uttrykket som er relevant, $4q -1$ i dette tilfellet.

\item[(2)] Anta at det kan vises at minst ett av primtallene i denne primtallsfaktorisering har det samme egenskapet som primtallene påstanden handler om. 

\item[(3)] Anta at dette primtallet er mindre enn er likt $n$. Vis da at vi får en motsigelse på en lignende måte som ovenfor. 

\item[(4)] Konkluder at dette primtallet er større enn $n$.

\end{itemize} %
%
Hvis en slik oppgave dukker opp på eksamen, blir du gitt en god del av poengene om du kan begynne og avslutte beviset riktig på denne måten.

\section*{Hvordan kommer jeg fram til det riktige utsagnet å se på?}

Det riktige utsagnet, $4q -1$ i dette tilfellet, er ikke lett å gjette i det hele tatt. Det blir alltid gitt som et tips i oppgaven.

\section*{Hvordan gjennomføre jeg midten av beviset?}

Det vil si: hvordan kan det vises at minst ett primtallene i en primtallsfaktorisering til uttrykket vi jobber med, $4q -1$ i dette tilfellet, har det samme egenskapet som står i oppgaven? Det finnes ingen generell oppskrift, men noen typer argumenter er ofte relevant.

\section*{Hva er disse?}

Når vi har et uttrykk som er lineær, som $4q-1$ her, pleier vi å dele i tilfeller modulo koeffisienten til $q$: hvert av primtallene $p_{i}$ i primtallsfaktoriseringen til $4q -1$ er kongruent til $0$, $1$, $2$, eller $3$ modulo $4$. Dette er ikke noe dypt: det sier egentlig at vi får $0$, $1$, $2$, eller $3$ som rest når vi deler $p_{i}$ med $4$. 

Først viser vi at det er umulig at $p_{i} \equiv 0 \pmod{4}$, og i tillegg umulig at $p_{i}$ er kongruent til et hvilket som helst av mulighetene $r$ slik at $\sfd{4,r} \not= 1$, nemlig $2$ i dette tilfellet. 

\section*{Hvordan viser jeg dette?}  

Dersom $p_{i} \equiv 0 \pmod{4}$, har vi: $4 \mid p_{i}$. Dette er umulig siden $p_{i}$ er et primtall. 

Dersom $p_{i} \equiv 2 \pmod{4}$, har vi: \[ p_{i} \equiv 0 \pmod{2}. \] Siden $p_{i}$ er ett ledd av primtallsfaktoriseringen til $4q-1$, er da \[ 4q -1 \equiv 0 \pmod{2}. \] Siden \[ 4 \equiv 0 \pmod{2}, \] er imidlertid \[ 4q -1 \equiv -1 \pmod{2}, \] altså er \[ 4q - 1 \equiv 1 \pmod{2}. \] 

Dermed er både \[ 4q -1 \equiv 0 \pmod{2} \] og \[ 4q -1 \equiv 1 \pmod{2}. \] Siden det ikke er sant at $0 \equiv 1 \pmod{2}$, er dette umulig.  

\subsection*{Hva gjør jeg da?}

Siden det er umulig at $p_{i} \equiv 0 \pmod{4}$ eller $p_{i} \equiv 2 \pmod{4}$, er enten $p_{i} \equiv 1 \pmod{4}$ eller $p_{i} \equiv 3 \pmod{4}$. Husk nå at målet er å vise at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ er kongruent til $3$ modulo $4$. 

Hvordan kan dette {\em ikke} være sant? Siden enten $p_{i} \equiv 1 \pmod{4}$ eller $p_{i} \equiv 3 \pmod{4}$ for alle primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$, er den eneste muligheten: $p_{i} \equiv 1 \pmod{4}$ for alle primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$. 

For å vise at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ er kongruent til $3$ modulo $4$, er det derfor nok å vise at det er umulig at alle er kongruent til $1$ modulo $4$.

Logikken er litt subtil her! Gå gjennom argumentet flere ganger, til du blir fortrolig med det.

\subsection*{Hvordan gjør jeg det?} 

Anta at $p_{i} \equiv 1 \pmod{4}$ for alle primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$. Da er \[ p_{1} p_{2} \cdots p_{t} \equiv \underbrace{1 \cdot 1 \cdots 1}_{\text{$t$ ganger}} = 1 \pmod{4}. \] Dermed er \[ 4q -1 \equiv 1 \pmod{4}. \] Siden $4 \equiv 0 \pmod{4}$, er imidlertid \[ 4q - 1 \equiv -1 \pmod{4}, \] altså er \[ 4q - 1 \equiv 3 \pmod{4}. \] 

Dermed er både \[ 4q -1 \equiv 1 \pmod{4} \] og \[ 4q -1 \equiv 3 \pmod{4}. \] Det er ikke sant at \[ 0 \equiv 3 \pmod{4}. \]

\subsection*{Konklusjon}

 Vi konkluderer at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ er kongruent til $3$ modulo $4$. Vi har rukket målet!

\subsection*{Hvordan kan je bli fortrolig med dette argumentet?}

Prøv å vise at at minst ett av primtallene i en primtallsfaktorisering til $6q -1$ er kongruent til $5$ modulo $6$. %
%
\begin{itemize}

\item[(1)] Vi at hvert primtall i primtallsfaktoriseringen til $6q-1$ er kongruent enten til $1$ modulo $6$ eller til $5$ modulo $6$, ved å vise at det er umulig at primtallet er kongruent til $0$, $2$, $3$, eller $4$ modulo $6$.

\item[(2)] Vis at det er umulig at alle primtallene i primtallsfaktoriseringen til $6q-1$ er kongruent til $1$ modulo 6.

\end{itemize}

\subsection*{Oppsummering}

Et svar på Oppgave 1 har tre deler. %
%
\begin{itemize}

\item[(1)] Begynnelsen: benytt en primtallsfaktorisering til $4q -1$. 

\item[(2)] Midten: vis at minst ett av primtallene i denne primtallsfaktoriseringen har det samme egenskapet som står i oppgaven, det vil si er kongruent til 3 modulo 4.

\item[(3)] Slutten: anta at dette primtallet er mindre enn eller likt $n$, og vis at vi da får en motsigelse. Konkluder at dette primtallet er større enn $n$.

\end{itemize} 

Et svar på en hvilken som helst oppgave som ligner på Oppgave 1 har de samme tre delene. Delene (1) og (3) er kan gjennomføres på nesten den samme måten i alle slike oppgaver.

Det er (2) som må tilpasses hver gang: hvis du ikke får dette steget til, er det helt fint på eksamen å anta at det kan vises, og forklare hvordan beviset kan fullføres deretter. 

\section*{Oppgave 2}

La $n$ være et naturlig tall. Bevis at det finnes et primtall $p$ slik at $p > n$ og $p \equiv 7 \pmod{8}$. {\em Tips}: La $q$ være produktet til alle primtallene som er mindre enn eller like $n$, og som er kongruent til $7$ modulo $8$. Benytt en primtallsfaktorisering til $8q^{2} - 1$. Benytt i tillegg at, dersom et primtall deler $8q^{2} - 1$, deler det også $(4q)^{2}  - 2$, og forklar hvorfor dette er sant.

\subsection*{Hvordan løse oppgaven?} 

Som nevnt ovenfor, kan begynnelsen og slutten på oppgaven gjennomføres som i svaret på Oppgave 1 ovenfor. Målet midten på beviset er å vise at det finnes minst ett primtall i primtallsfaktoriseringen \[ p_{1} p_{2} \cdots p_{t} \] til $8q^{2} - 1$ som er kongruent til $7$ modulo $8$.

\subsection*{Kan jeg ikke gjennomføre det samme argumentet som i Oppgave 1?}

Vi trenger et nytt argument. Hvis vi prøver å gjennomføre et argument som ligner på argumentet vi ga for å svare på Oppgave 1, har vi et problem: vi kan ikke vise at det er umulig at \[ p_{i} \equiv 3 \pmod{8} \] eller \[ p_{i} \equiv 5 \pmod{8}. \]  

\subsection*{Hva gjør jeg da?} 

Vi trenger et nytt argument. Idéen er å benytte kvadratisk gjensidighet som følger. %
%
\begin{itemize}

\item[(1)] Siden $p_{i}$ er et ledd av primtallsfaktoriseringen til $8q^{2} - 1$, har vi: \[ p_{i} \mid 8q^{2} - 1. \] Som tipset i oppgaven foreslår, har vi da: \[ p_{i} \mid (4q)^{2} - 2, \] siden \[ (4q)^{2} - 2 = 16q^{2} - 2 = 2\left( 8q^{2} - 1 \right). \] Da er \[ (4q)^{2} - 2 \equiv 0 \pmod{p_{i}}, \] altså er \[ (4q)^{2} \equiv 2 \pmod{p_{i}}. \] Dermed er $2$ en kvadratisk rest modulo $p_{i}$, altså er $\lgdsym{2,p_{i}} = 1$.

\item[(2)] Ut ifra regel (F) i oversikten over Legendresymboler og kvadratiske kongruenser, er da \[ p_{i} \equiv 1 \pmod{8} \] eller \[ p_{i} \equiv 7 \pmod{8}. \]

\end{itemize} 

Husk nå at målet er å vise at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ er kongruent til $7$ modulo $8$. Vi kan gjøre dette ved å gjennomføre akkurat det samme argumentet som i svaret vårt på Oppgave 1. Logikken er som følger.

Hvordan kan det {\em ikke} være sant at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ er kongruent til $7$ modulo $8$? Siden enten \[ p_{i} \equiv 1 \pmod{8} \] eller \[ p_{i} \equiv 7 \pmod{8} \] for alle primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$, er den eneste muligheten: \[ p_{i} \equiv 1 \pmod{8} \] for alle primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$. 

For å vise at minst ett av primtallene $p_{1}$, $p_{2}$, \ldots, $p_{t}$ er kongruent til $7$ modulo $8$, er det derfor nok å vise at det er umulig at alle er kongruent til $1$ modulo $8$. Vi gjør dette akkurat som i svaret på Oppgave 1.

\subsection*{Vi må være forsiktig!}

For å kunne benytte regel (F) i oversikten over Legendresymboler og kvadratiske kongruenser som ovenfor, må vi har: $p_{i} > 2$. For å kunne gjennomføre argumentet ovenfor, må vi derfor vise at det er sant at $p_{i} > 2$.  

Dette kan gjøres som i svaret på Oppgave 1. Siden $p_{i}$ er et ledd av primtallsfaktoriseringen til $8q^{2} - 1$, har vi: $p_{i} \mid 8q^{2} - 1$. Dersom $p_{i} = 2$, har vi da: $2 \mid 8q^{2} -1$, altså \[ 8q^{2} - 1 \equiv 0 \pmod{2}. \] Vi kan vise at dette er umulig akkurat i svaret vårt på Oppgave 1, ved å benytte at \[ 8 \equiv 0 \pmod{2}. \]

\subsection*{Oppsummering}

Et svar på Oppgave 2 har samme tre deler som et svar på Oppgave 1. %
%
\begin{itemize}

\item[(1)] Begynnelsen: benytt en primtallsfaktorisering til $8q^{2} -1$. 

\item[(2)] Midten: vis at minst ett av primtallene i denne primtallsfaktoriseringen har det samme egenskapet som står i oppgaven, det vil si er kongruent til 7 modulo 8.

\item[(3)] Slutten: anta at dette primtallet er mindre enn eller likt $n$, og vis at vi da får en motsigelse. Konkluder at dette primtallet er større enn $n$.

\end{itemize} %
%
For å gjennomføre (2), benytte vi et dypt regel om Legendresymboler i tillegg til noen av argumentene vi benytt oss av i svaret vårt på Oppgave 1. For å kunne gjøre dette, er det typisk nødvendig å vise at ingen av primtallene i primtallsfaktoriseringen er lik $2$. 

Et svar på en hvilken som helst oppgave som ligner på Oppgave 2, hvor uttrykket vi ser på, $8q^{2} -1$ i dette tilfellet, er kvadratisk, kan gjennomføres på denne måten. Vi benytter enten ett av reglene om Legendresymboler, eller noe som følger fra disse, som du blir bedt om å vise tidligere i oppgaven.

\subsection*{Hvordan kan jeg bli fortrolig med argumentet?}

Prøv æ vise at minst ett av primtallene, faktisk alle primtallene, i en primtallsfaktorisering til $(2q)^{2} + 1$ er kongruent til $1$ modulo $4$. %
%
\begin{itemize}

\item[(1)] Vis at $p_{1}$ ikke er lik $2$.

\item[(2)] Observer at det følger at $\lgdsym{-1,p_{1}} = 1$. Benytt da regel (E) i oversikten over Legendresymboler og kvadratiske kongruenser for å få at \[ p_{1} \equiv 1 \pmod{4}. \] 

\end{itemize} %
%
Det er ikke noe spesielt med $p_{1}$ her: argumentet kan gjennomføres for hvert $p_{i}$. 
  
\end{document}
