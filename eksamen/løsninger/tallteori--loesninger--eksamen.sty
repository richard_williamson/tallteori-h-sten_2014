\ProvidesPackage{tallteori--loesninger--eksamen}

%%%%%%%%%%
%Language
%%%%%%%%%%

\usepackage[utf8]{inputenc}

\usepackage[norsk]{babel}

%Change 'no-break space' characters (accidentally typed but not visible in vim, causing a font encoding error) into actual spaces. 
\DeclareUnicodeCharacter{00A0}{ }

%%%%%%%%%%%%%
%Diagrams
%%%%%%%%%%%%%

\usepackage{tikz}
\usetikzlibrary{matrix, arrows, calc, patterns, intersections, decorations.markings, decorations.pathreplacing, shapes.geometric}

%Extracting x and y coordinates from a point. The arguments {*1}{*2}{*3} consist of a point *1, a name for a macro *2, and a name for a macro *3, where *2 will contain the x-coordinate, and *3 will contain the y-coordinate. For example: \tikzcoordinates{(1,1)}{\xcoord}{\yccord}. After this, \xcoord and \ycoord can be called.

\makeatletter
\newcommand{\tikzcoordinates}[3]{%
  \tikz@scan@one@point\pgfutil@firstofone#1\relax
  \edef#2{\the\pgf@x}%
  \edef#3{\the\pgf@y}%
}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%
%Comment environment
%%%%%%%%%%%%%%%%%%%%%%

\usepackage{comment}

%%%%%%%%
%Tables
%%%%%%%%

\usepackage{booktabs}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Symbols, 'cases' environment, etc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{amssymb,amsmath}

%For 'tick' and 'cross' symbols.
\usepackage{pifont}
\newcommand{\tick}{\ding{51}}
\newcommand{\cross}{\ding{55}}

%%%%%%%%%%%%%%%%%%%%%%%%
%Does not divide symbol
%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{centernot}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Natural deduction proof trees
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{bussproofs}

%%%%%%%%%%%%%%%%%%%
%List appearance
%%%%%%%%%%%%%%%%%%%

\usepackage{enumitem}
\setlist[itemize]{align=left, leftmargin=*, labelindent=\parindent}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%References across documents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{xr-hyper}

%%%%%%%%%%%%%%%%%%
%References
%%%%%%%%%%%%%%%%%%

%Clickable references, with page references given in the bibliography. 
\usepackage{hyperref}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Numbering of Oppgaver chapters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{oppgaver}
\setcounter{oppgaver}{1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Numbering of Løsninger chapters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{loesninger}
\setcounter{loesninger}{1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%No sections numbered, but still show up in contents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setcounter{secnumdepth}{-1}

%%%%%%%%%%%%%%%%%%%%%%
%Theorem environments
%%%%%%%%%%%%%%%%%%%%%%

\usepackage{amsthm}

\theoremstyle{definition}

\newtheorem{defn}{Definisjon}[section]
\newtheorem{merknad}[defn]{Merknad}
\newtheorem{notn}[defn]{Notasjon}
\newtheorem{terminologi}[defn]{Terminologi}
\newtheorem{prpn}[defn]{Proposisjon}
\newtheorem{teom}[defn]{Teorem}
\newtheorem{lem}[defn]{Lemma}
\newtheorem{kor}[defn]{Korollar}
\newtheorem{eks}[defn]{Eksempel}

\newtheorem{oppgave}{Oppgave}[section] 
\renewcommand{\theoppgave}{\thesection.\arabic{oppgave}}

\newtheorem{loesningone}{Løsning til}[section]
\renewcommand{\theloesningone}{\thechapter.1.\arabic{loesningone}}

\newtheorem{loesningtwo}{Løsning til}[section]
\renewcommand{\theloesningtwo}{\thechapter.2.\arabic{loesningtwo}}

\newtheorem{loesning}{Løsning til Oppgave}[section] 
\renewcommand{\theloesning}{\arabic{loesning}}

\newtheorem*{merknad*}{Merknad}

%%%%%%%%%%%%%
%Technical
%%%%%%%%%%%%%

%Given list #1= {a_1,...,a_n} and 1 ≤ #2=j ≤ n, outputs a_j.
\newcommand{\nth}[2]{
	\foreach [count=\i] \item in {#1} {
		\ifnum\i=#2	
			\item
		\fi
	}	
}

%For centering a commutative diagram (with space above and below)
\newenvironment{diagram}{\medskip\centering}{\medskip\noindent\ignorespacesafterend}

%For displaying an equality with the same spacing as a diagram as above.
\newenvironment{equality}{\medskip\centering}{\medskip\noindent\ignorespacesafterend}

%For centering a figure (with morespace above than for a diagram)
\newenvironment{figuretikz}{\bigskip\bigskip\centering}{\bigskip\noindent\ignorespacesafterend}

%%%%%%%%%%%%%%%
%Abbreviations
%%%%%%%%%%%%%%%

\newcommand{\sfd}[1]{\mathsf{sfd}(\nth{#1}{1},\nth{#1}{2})}
\newcommand{\lgdsym}[1]{\mathbb{L}^{\nth{#1}{1}}_{\nth{#1}{2}}}

%%%%%%%%%%%%%%%%%%%%%%%%
%Diagram templates
%%%%%%%%%%%%%%%%%%%%%%%%

%All diagrams have as input a list #1={*1,*2,...,*n} of vertices and edges. The entry *j is selected via the command \nth defined above.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Arrow #2={*1,*2,*3} as below. The optional parameter #1 specifies the column sep value, with default #1 = 3.
%
%       *3
%   *1 ----> *2
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\ar}[2][3]{

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=#1 em, nodes={anchor=center}]
{ 
|(0-0)| \nth{#2}{1} \& |(1-0)| \nth{#2}{2} \\ 
};
	
\draw[->] (0-0) to node[auto] {$\nth{#2}{3}$} (1-0);
 
\end{tikzpicture} 

\end{diagram} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Pair of arrows #1={*1,*2,*3,*4} as below. The optional parameter #1 specifies the column sep value, with default #1 = 3.

%
%      *3
%     ---->
%  *1       *2
%     ---->
%      *4
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\pair}[2][3]{

\begin{diagram}

\begin{tikzpicture} [>=stealth]

\matrix [ampersand replacement=\&, matrix of math nodes, column sep=#1 em, nodes={anchor=center}]
{ 
|(0-0)| \nth{#2}{1} \& |(1-0)| \nth{#2}{2} \\ 
};
	
\draw[->]  ([yshift=0.75em]{0-0}.east) to node[auto] {$\nth{#2}{3}$}  ([yshift=0.75em]{1-0}.west);

\draw[->] ([yshift=-0.75em]{0-0}.east) to node[auto,swap] {$\nth{#2}{4}$} ([yshift=-0.75em]{1-0}.west);
 
\end{tikzpicture} 

\end{diagram} }
