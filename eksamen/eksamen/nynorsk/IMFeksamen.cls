%
% Underklasse av NTNUeksamen som setter instituttnavn korrekt
%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{IMFeksamen}[2004/05/11]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{NTNUeksamen}}
\ProcessOptions
\LoadClass{NTNUeksamen}
\addto\extrasnorsk{%
  \def\NTNUi{Institutt for matematiske fag}%
}
\addto\extrasnynorsk{%
  \def\NTNUi{Institutt for matematiske fag}%
}
\addto\extrasenglish{%
  \def\NTNUi{Department of Mathematical Sciences}%
}

