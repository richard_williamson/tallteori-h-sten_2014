\setcounter{chapter}{6}
\renewcommand{\thechapter}{\arabic{chapter}}

\chapter{Pytagoreiske tripler og Fermats siste teorem} \label{KapittelPytagoreiskeTriplerOgFermatsSisteTeorem}

\section{Ikke lineære diofantiske ligninger med ingen løsning}

\begin{merknad} I {\S}\ref{DelLineaereDiofantiskeLigninger} fikk vi en komplett forståelse for hvordan løse lineære diofantiske ligninger. I {\S}\ref{DelLineaereKongruenser} omformulerte vi denne teorien for å få en komplett forståelse for hvordan løse lineære kongruenser modulo et heltall. I Kapittel \ref{KapittelKvadratiskGjensidighet} fikk vi en komplett forståelse for når en kvadratisk kongruens modulo et primtall $p > 2$ har en løsning, og hvor mange løsninger den har.

Som nevnt i Merknad \ref{MerknadDiofantiskeLigningerDype}, handler en stor del av dagens forskning i tallteori, {\em aritmetisk geometri}, om å prøve å få en like god forståelse for heltallsløsninger til andre typer ligninger og kongruenser, for eksempel med flere variabler. Teorien er generelt sett svært dyp, og må tilpasses til de ulike ligningene og kongruensene man ser på. Ofte vet vi fremdeles lite, og jeg ser på dette som noe spennende.

Av og til kan vi si noe. For eksempel kan vi noen ganger benytte modulær aritmetikk for å fastslå at en bestemt ligning ikke har en heltallsløsning. La oss se på et eksempel.
 
\end{merknad} 

\begin{prpn} \label{ProposisjonLigningenY2LikX3Minus16XPluss22IngenLoesning} Ligningen \[ y^{2} = x^{3} -16x + 29 \] har ingen heltallsløsning.  \end{prpn} 

\begin{proof} Anta at \[ x=a \] og \[ y=b \] er en heltallsløsning, altså at \[ b^{2} = a^{3} -16a + 29. \] Da er \[ b^{2} \equiv a^{3} - 16a + 29 \pmod{3}.\] Siden \[ -16 \equiv 2 \pmod{3} \] og \[ 29 \equiv 2 \pmod{3}, \] er da \[ b^{2} \equiv a^{3} + 2a + 2 \pmod{3}. \] La oss se på følgende utsagn. %
%
\begin{itemize}

\item[(A)] $a \equiv 0 \pmod{3}$;

\item[(B)] $a \equiv 1 \pmod{3}$;

\item[(C)] $a \equiv 2 \pmod{3}$.

\end{itemize} %
%
Anta først at (A) er sant. Da er \[ a^{3} +  2a + 2 \equiv 0^{3} + 2 \cdot 0 + 2 = 2 \pmod{3}. \] Dermed er\[ b^{2} \equiv 2 \pmod{3}. \] Ut ifra Korollar \ref{KorollarKvadratiskGjensidighet2}, er imidlertid $\lgdsym{2,3} = -1$, altså er $2$ ikke en kvadratisk rest modulo $3$. Dermed finnes det ikke noe heltall $y$ slik at \[ y^{2} \equiv 2 \pmod{3}. \] Siden antakelsen at (A) er sant fører til motsigelsen at både \[ b^{2} \equiv 2 \pmod{3} \] og at det ikke finnes et heltall $y$ slik at \[ y^{2} \equiv 2 \pmod{3}, \] konkluderer vi at (A) ikke er sant.     

Anta nå at (B) er sant. Da er \[ a^{3} +  2a + 2 \equiv 1^{3} + 2 \cdot 1 + 2 = 5 \equiv 2 \pmod{3}. \] Dermed er \[ b^{2} \equiv 2 \pmod{3}. \] Som i tilfellet da vi antok at (A) var sant, konkluderer vi at (B) ikke er sant.  

Anta nå at (C) er sant.  Da er \[ a^{3} +  2a + 2 \equiv 2^{3} + 2 \cdot 2 + 2 = 14 \equiv 2 \pmod{3}. \] Dermed er \[ b^{2} \equiv 2 \pmod{3}. \] Som i tilfellet da vi antok at (A) var sant, konkluderer vi at (C) ikke er sant.  

Således har vi bevist: dersom ligningen \[  y^{2} = x^{3} -16x + 29 \] har en heltallsløsning, er ikke noe av (A) -- (C) sant.  Imidlertid fastslår Proposisjon \ref{ProposisjonAlleHeltalleneKongruentTilEtHeltallMindreEnnN} at ett av (A) -- (C) er sant. Siden antakelsen at ligningen \[  y^{2} = x^{3} -16x + 29 \] har en heltallsløsning fører til denne motsigelsen, konkluderer vi at ligningen \[ x = a \] og \[ y=b \] har ingen heltallsløsning. 

 \end{proof}

\begin{merknad} Det finnes ingen oppskrift som sier at det er en god idé å jobbe modulo $3$ i beviset for Proposisjon \ref{ProposisjonLigningenY2LikX3Minus16XPluss22IngenLoesning}. Vi kunne for eksempel ha først jobbet modulo $2$. Da hadde vi funnet noe: at enten både \[ a \equiv 0 \pmod{2} \] og \[ b \equiv 0 \pmod{2}, \] eller både \[ a \equiv 1 \pmod{2} \] og \[ b \equiv 1 \pmod{2}, \] altså enten både $a$ og $b$ er partall eller både $a$ og $b$ er oddetall. Imidlertid kunne vi ikke ha kommet fram til at \[  y^{2} = x^{3} -16x + 29 \] har ingen heltallsløsning.    \end{merknad}

\begin{merknad} En ligning \[ y^{2} = x^{3} + cx +d, \] hvor $c$ og $d$ er heltall, kalles en {\em elliptisk kurve}. Teorien for elliptiske kurver er en av de peneste i hele matematikken. Teorien dukker opp på overraskende måter overalt i matematikk: algebraisk topologi og harmonisk analyse for eksempel! Elliptiske kurver brukes også i kryptografi.  \end{merknad}

%TODO: Another example. Where need to work modulo several primes?

\begin{merknad} I denne delen av kapittelet har vi sett at vi noen ganger kan bevise at en bestemt ikke lineær ligning har ingen heltallsløsning ved å bevise at den har ingen løsning modulo et naturlig tall. {\em Hasse-prinsippet}, som vi ikke kommer til å se på i kurset, men som er svært viktig i tallteori, sier at vi noen ganger kan gå den motsatte veien: dersom en ligning har en løsning modulo et {\em hvilket som helst primtall}, kan vi i noen tilfeller konkludere at den har en heltallsløsning. \end{merknad}

\section{Pytagoreiske tripler}

\begin{merknad} Fra skolen kjenner du til Pythagoras' teorem: gitt en rettvinklet trekant slik at katetene har lengdene $a$ og $b$, og hypotenusen har lengden $c$, er \[ a^{2} + b^{2} = c^{2}. \]

\begin{figuretikz}

\begin{tikzpicture} [>=stealth,font=\small]

\draw (0,0) to node[auto,swap] {$a$} (2,0);
\draw (2,0) to node[auto,swap] {$b$} (2,2);
\draw (0,0) to node[auto] {$c$} (2,2);

\draw (1.8,0) -- (1.8,0.2) -- (2,0.2);

\end{tikzpicture}

\end{figuretikz} %
%
I denne delen av kapittelet kommer vi til å finne alle de rettvinklete trekantene slik at $a$, $b$, og $c$ er heltall. 

Utgangspunktet er Pythagoras' teorem: vi kommer til å finne alle tripler naturlige tall $(a,b,c)$ slik at \[ a^{2} + b^{2} = c^{2}. \]   

 \end{merknad} 

\begin{terminologi} La $a$, $b$, og $c$ være heltall slik at \[ a^{2} + b^{2} = c^{2}. \] Da sier vi at $(a,b,c)$ er et {\em pytagoreisk trippel}.  \end{terminologi} 

\begin{merknad} Med andre ord er $(a,b,c)$ et pytagoreisk trippel om $x=a$, $y=b$, og $z=c$ er en løsning til ligningen \[ x^{2} + y^{2} = z^{2}. \] \end{merknad}

\begin{eks} \label{Eksempel345PytagoreiskTrippel} Vi har: \[ 3^{2} + 4^{2} = 9 + 16 = 25 = 5^{2}. \] Dermed er $(3,4,5)$ et pytagoreisk trippel. \end{eks}

\begin{eks} \label{Eksempel51213PytagoreiskTrippel} Vi har: \[ 5^{2} + 12^{2} = 25 + 144 = 169 = 13^{2}. \] Dermed er $(5,12,13)$ et pytagoreisk trippel.  \end{eks}

\begin{eks} Vi har: \[ 8^{2} + 15^{2} = 64 + 225 = 289 = 17^{2}. \] Dermed er $(8,15,17)$ et pytagoreisk trippel.  \end{eks}

\begin{prpn} \label{ProposisjonKGangerPytagTrippelErEtPytagTrippel} La $(a,b,c)$ være et pytagoreisk trippel. La $k$ være et naturlig tall. Da er $(ka,kb,kc)$ et pytagoreisk trippel. \end{prpn} 

\begin{proof} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Vi har: %
%
\begin{align*} %
%
(ka)^{2} + (kb)^{2} &= k^{2}a^{2} + k^{2}b^{2} \\
                    &= k^{2} \left( a^{2} + b^{2} \right).
\end{align*} 

\item[(2)] Siden $(a,b,c)$ er et pytagoreisk trippel, er \[ a^{2} + b^{2} = c^{2}. \] Derfor er \[ k^{2} \left( a^{2} + b^{2} \right) = k^{2}c^{2}. \]

\item[(3)] Det følger fra (1) og (2) at \[ (ka)^{2} + (kb)^{2} = k^{2}c^{2} = (kc)^{2}. \]  

\end{itemize} %
%
Dermed er $(ka,kb,kc)$ et pytagoreisk trippel. 

\end{proof}

\begin{eks} Ut ifra Eksempel \ref{Eksempel345PytagoreiskTrippel}, er $(3,4,5)$ et pytagoreisk trippel. Da fastslår Proposisjon \ref{ProposisjonKGangerPytagTrippelErEtPytagTrippel} at $(3k,4k,5k)$ er et pytagoreisk trippel for et hvilket som helst naturlig tall $k$. Ved å la for eksempel $k$ være $2$, fastslår Proposisjon \ref{ProposisjonKGangerPytagTrippelErEtPytagTrippel} at $(6,8,10)$ er et pytagoreisk trippel. Siden \[ 6^{2} + 8^{2} = 36 + 64 = 100 = 10^{2}, \] er dette riktignok sant.     
\end{eks}

\begin{eks} Ut ifra Eksempel \ref{Eksempel51213PytagoreiskTrippel}, er $(5,12,13)$ et pytagoreisk trippel. Da fastslår Proposisjon \ref{ProposisjonKGangerPytagTrippelErEtPytagTrippel} at $(5k,12k,13k)$ er et pytagoreisk trippel for et hvilket som helst naturlig tall $k$. Ved å la for eksempel $k$ være $3$, fastslår Proposisjon \ref{ProposisjonKGangerPytagTrippelErEtPytagTrippel} at $(15,36,39)$ er et pytagoreisk trippel. Siden \[ 15^{2} + 36^{2} = 225 + 1296 = 1521 = 39^{2}, \] er dette riktignok sant.     
\end{eks}

\begin{defn} Et pytagoreisk trippel $(a,b,c)$ er {\em primitivt} dersom $\sfd{a,b,c} = 1$. \end{defn}

\begin{eks} Siden $\sfd{3,4,5} = 1$, er det pytagoreiske trippelet $(3,4,5)$ primitivt. 
 \end{eks}

\begin{eks} Siden $\sfd{5,12,13} = 1$, er det pytagoreiske trippelet $(5,12,13)$ primitivt. 
 \end{eks}

\begin{eks} Vi har: \[ 14^{2} + 48^{2} = 2500 = 50^{2}. \] Dermed er $(14,48,50)$ et pytagoreisk trippel. Siden $\sfd{14,48,50} = 2$, er $(14,48,50)$ ikke et primitivt pytagoreisk trippel.  \end{eks} 

\begin{eks} Vi har: \[ 100^{2} + 105^{2} = 21025 = 145^{2}. \] Dermed er $(100,105,145)$ et pytagoreisk trippel. Siden $\sfd{100,105,145} = 5$, er $(100,105,145)$ ikke et primitivt pytagoreisk trippel.  \end{eks} 

\begin{prpn} Dersom $(a,b,c)$ er et pytagoreisk trippel, finnes det et pytagoreisk trippel $(a',b',c')$ slik at: %
%
\begin{itemize}

\item[(1)] $(a',b',c')$ er primitivt;

\item[(2)] det finnes et naturlig tall $k$ slik at $a=ka'$, $b=kb'$, og $c=kc'$. 

\end{itemize}
 \end{prpn}

\begin{proof} La oss betegne $\mathsf{sfd}(a,b,c)$ som $k$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra definisjonen til $k$, har vi: $k \mid a$. Dermed finnes det et naturlig tall a' slik at $a=a'k$.

\item[(2)] Ut ifra definisjonen til $k$, har vi: $k \mid b$. Dermed finnes det et naturlig tall b' slik at $b=b'k$.

\item[(3)] Ut ifra definisjonen til $k$, har vi: $k \mid c$. Dermed finnes det et naturlig tall c' slik at $c=c'k$.

\item[(4)] Ut ifra Proposisjon \ref{}, er $\mathsf{sfd}(a',b',c') = 1$. 

\item[(5)] Vi har: %
%
\begin{align*} %
%
k^{2} \cdot \left( \left( a' \right)^{2} + \left(b'\right)^{2} \right) &= k^{2} \left( a' \right)^{2} + k^{2} \left( b' \right)^{2} \\ 
&= \left( a'k \right)^{2} + \left( b'k \right)^{2} \\
&= a^{2} + b^{2}.
\end{align*} 

\item[(6)] Siden $(a,b,c)$ er et pytagoreisk trippel, er \[ a^{2} + b^{2} = c^{2}. \] 

\item[(7)] Vi har: %
%
\begin{align*} %
%
c^{2} &= \left( c'k \right)^{2} \\
&= k^{2} \left( c' \right)^{2}.
\end{align*} 

\item[(8)] Det følger fra (5) --(7) at \[ k^{2} \cdot \left( \left( a' \right)^{2} + \left(b'\right)^{2} \right) = k^{2} \left( c' \right)^{2}. \] 

\item[(9)] Det følger fra (8) og Proposisjon \ref{ProposisjonKanDeleBeggeSideneAvEnLigningMedEtHeltall} at \[ \left( a' \right)^{2} + \left(b'\right)^{2} = \left( c' \right)^{2}. \] Dermed er $\left( a', b', c' \right)$ et pytagoreisk trippel.  

\end{itemize} %
%
Ut ifra (4) og (9), har vi: $(a',b',c')$ er et primitivt pytagoreisk trippel. 

 \end{proof}

\begin{eks} Ut ifra Eksempel \ref{}, er $(14,48,50)$ et pytagoreisk trippel. Vi har: $\sfd{14,48,50} = 2$. 
\end{eks}

\begin{merknad} %TODO: Can focus on primitive ones. 

 \end{merknad} 

\begin{prpn} La $(a,b,c)$ være et primitivt pytagoreisk trippel. Da er det ikke sant at \[ a \equiv b \pmod{2}. \] \end{prpn}

\begin{proof} Ett av følgende er sanne.  \end{proof} 

\begin{prpn} La $a$, $b$, og $c$ være naturlige tall. Anta at $a$ er et partall. Da er $(a,b,c)$ et primitivt pytagoreisk trippel om og bare om det finnes naturlige tall $m$ og $n$ slik at følgende er sanne. %
%
\begin{itemize}

\item[(A)] $a=2mn$;

\item[(B)] $b=m^{2} - n^{2}$;

\item[(C)] $c=m^{2} + n^{2}$; 

\item[(D)] $m > n$;

\item[(E)] $\sfd{m,n}=1$;

\item[(F)] det ikke er sant at \[ m \equiv n \pmod{2}. \]

\end{itemize}
 
 \end{prpn}

\begin{proof} Anta først at $(a,b,c)$ er et pytagoreisk trippel. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Siden $a$ er et partall, finnes det et naturlig tall $k$ slik at $a=2k$. I tillegg følger det fra Proposisjon \ref{} at $b$ og $c$ er oddetall. Ut ifra Lemma \ref{} er da $c-b$ og $c+b$ partall. Dermed finnes det et naturlig tall $s$ slik at $c-b=2s$, og et naturlig tall $t$ slik at $c+b=2t$. 

\item[(2)] Siden \[ a^{2} + b^{2} = c^{2}, \] er \[ a^{2} = c^{2} - b^{2}. \] Siden \[ c^{2} - b^{2} = (c-b)(c+b), \] er derfor \[ a^{2} = (c-b)(c+b). \]      

\item[(3)] Det følger fra (1) og (2) at \[ \left( 2k \right)^{2} = \left( 2s - 2t \right) \left( 2s + 2t \right) =2 \cdot (s-t) \cdot 2 \cdot (s+t) = 4 (s-t)(s+t), \] altså at \[ 4k^{2} = 4(s-t)(s+t). \] Ut ifra Proposisjon \ref{}, er da \[ k^{2} = (s-t)(s+t). \]

\item[(4)] La $d$ være et naturlig tall slik at $d \mid s$ og $d \mid t$. Da følger det fra Proposisjon \ref{} at $d \mid t-s$ og at $d \mid s+t$. 

\item[(5)] Vi har: \[ 2(t-s) = 2t -2s = \left( c+b \right) - \left( c-b \right) = 2b, \] altså \[ 2(t-s) = 2b. \] Ut ifra Proposisjon \ref{} er da \[ t-s = b. \] 

\item[(6)] Vi har: \[ 2(s+t) = 2s + 2t = \left( c - b \right) + \left( c+b \right) = 2c, \] altså \[ 2(s-t) = 2c. \] Ut ifra Proposisjon \ref{} er da \[ s+t = c. \]

\item[(7)] Det følger fra (4) -- (6) at $d \mid b$ og $d \mid c$. Siden $\sfd{b,c}=1$, følger det at $d=1$. 

\item[(8)] Således har vi bevist: dersom $d \mid s$ og $d \mid t$, er $d=1$. Vi konkluderer at $\sfd{s,t} = 1$.

\item[(9)] Ut ifra (3), (8), og Lemma \ref{}, finnes det et naturlig tall $m$ slik at $m^{2} = s-t$, og et naturlig tall $n$ slik at $n^{2} = s+t$.

\item[(10)] Ut ifra (5) og (9) er  
  
\end{itemize}
 
 \end{proof}
