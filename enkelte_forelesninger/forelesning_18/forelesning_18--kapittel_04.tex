%forelesning18A
\section{Lagranges teorem}

\begin{merknad} \label{MerkandPolynomLiktNullHarMaksNLoesninger} Fra skolen kjenner du til at en ligning \[ ax^{2} + bx + c = 0 \] har maksimum to løsninger. Se Merknad \ref{MerknadFraKvadratiskeLigningerTilKvadratiskeKongruenser} for mer om dette. Kanskje kjenner du dessuten til noe som er mer generell: en ligning \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \cdots + a_{2}x^{2} + a_{1}x + a_{0} = 0 \] har maksimum $n$ løsninger. I denne delen av kapittelet skal vi bevise at det samme er tilfellet i modulær aritmetikk: en kongruens \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \cdots + a_{2}x^{2} + a_{1}x + a_{0} = 0 \] har maksimum $n$ løsninger slik at ikke noe par av disse er kongruent til hverandre modulo $p$.   \end{merknad}

\begin{prpn} \label{ProposisjonPolynomIEenVariabelKongNullHarLoesningModMFoererTilAtDetHarLoesningMellom0OgM} La $m$ være et heltall. La $n$ være et naturlig tall. For hvert heltall $i$ slik at $0 \leq i \leq n$, la $a_{i}$ være et heltall. La $x$ være et heltall slik at \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \equiv 0 \pmod{m} .\] Ut ifra Proposisjon \ref{ProposisjonAlleHeltalleneKongruentTilEtHeltallMindreEnnN} finnes det et heltall $r$ slik at: %
%
\begin{itemize}

\item[(1)] $0 \leq r < m-1$;

\item[(2)] $x \equiv r \pmod{m}$.

\end{itemize} %
%
Vi har: \[ a_{n}r^{n} + a_{n-1}r^{n-1} + \dots + a_{2}r^{2} + a_{1}r + a_{0} \equiv 0 \pmod{m} .\]   \end{prpn}

\begin{proof} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonXKongModNImplisererXOpphoeydITKongYOpphoeydITModN} er \[ x^{i} \equiv r^{i} \pmod{m} \] for hvert naturlig tall $i$ slik at $i \leq n$.

\item[(2)] Det følger fra (1) og Korollar \ref{KorollarXKongYModNImplisererXGangerMedZKongYGangerMedZModN} at \[ a_{i} x^{i} \equiv a_{i} r^{i} \pmod{m} \] for hvert naturlig tall $i$ slik at $i \leq n$. 

\item[(3)] Det følger fra (2) og Korollar \ref{ProposisjonXKongYOgXXKongYYModNImplisererXPlussXXKongYPlussYYModN} at %
%
\begin{align*} %
%
& a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x \\ 
&\equiv a_{n}r^{n} + a_{n-1}r^{n-1} + \dots + a_{2}r^{2} + a_{1}r \pmod{m}
\end{align*}

\item[(4)] Det følger fra (3) og Korollar \ref{KorollarXKongYModNImplisererXPlussZKongYPlussZModN} at %
%
\begin{align*} %
%
& a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \\
&\equiv a_{n}r^{n} + a_{n-1}r^{n-1} + \dots + a_{2}r^{2} + a_{1}r + a_{0} \pmod{m}.
\end{align*} %
%
Ut ifra Proposisjon \ref{ProposisjonXKongYModNImplisererYKongXModN} er da %
%
\begin{align*} %
%
& a_{n}r^{n} + a_{n-1}r^{n-1} + \dots + a_{2}r^{2} + a_{1}r + a_{0} \\
&\equiv a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \pmod{m}
\end{align*} 

\end{itemize} %
%
Det følger fra (4), antakelsen at \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \equiv 0 \pmod{m} \] og Proposisjon \ref{ProposisjonXKongYOgYonZModNImplisererXKongZModN} at \[ a_{n}r^{n} + a_{n-1}r^{n-1} + \dots + a_{2}r^{2} + a_{1}r + a_{0} \equiv 0 \pmod{m}. \]

\end{proof}

\begin{eks} Det kan regnes ut at \[ 16^{2} + 3 \cdot 16 + 4 = 308 \] og at $308 = 44 \cdot 7$, altså at \[ 308 \equiv 0 \pmod{7}. \] Dermed er $x=16$ en løsning til kongruensen \[ x^{2} + 3x + 4 \equiv 0 \pmod{7}. \] Siden \[ 16 \equiv 2 \pmod{7}, \] fastslår Proposisjon \ref{ProposisjonPolynomIEenVariabelKongNullHarLoesningModMFoererTilAtDetHarLoesningMellom0OgM} at $x=2$ er også en løsning til kongruensen. Dette er riktignok sant. \end{eks} 

\begin{eks} Det kan regnes ut at \[ 9^{3} + 3 \cdot 9^{2}  -16 \cdot 9 + 2= 830 \] og at $830 = 166 \cdot 5$, altså at \[ 830 \equiv 0 \pmod{5}. \] Dermed er $x=9$ en løsning til kongruensen \[ x^{3} + 3x^{2} + -16x + 2 \equiv 0 \pmod{5}. \] Siden \[ 9 \equiv 4 \pmod{5}, \] fastslår Proposisjon \ref{ProposisjonPolynomIEenVariabelKongNullHarLoesningModMFoererTilAtDetHarLoesningMellom0OgM} at $x=4$ er også en løsning til kongruensen. Dette er riktignok sant.  \end{eks} 

\begin{lem} \label{LemmaDelerMedXMinusY} La $p$ være et primtall. La $n$ være et naturlig tall. For hvert heltall $i$ slik at $0 \leq i \leq n$, la $a_{i}$ være et heltall. La $y$ være et heltall. Da finnes det et heltall $r$ og, for hvert heltall $i$ slik at $0 \leq i \leq n-1$, et heltall $b_{i}$, slik at %
%
\begin{align*} %
%
& a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \\
&= (x-y) \left( b_{n-1}x^{n-1} + b_{n-2}x^{n-2} + \dots + b_{2}x^{2} + b_{1}x + b_{0} \right) + r
\end{align*} %
%
for hvert heltall $x$. 

\end{lem}

\begin{proof} Først sjekker vi om lemmaet er sant når $n=1$. La $b_{0}$ være $a_{1}$, og la $r$ være $a_{1}y + a_{0}$. Da er: %
%
\begin{align*} %
%
(x-y)b_{0} + r &= a_{1}(x-y) + \left( a_{1}y + a_{0} \right) \\
               &= a_{1}x - a_{1}y + a_{1}y + a_{0} \\
               &= a_{1}x + a_{0}.
\end{align*} %
%
Dermed er lemmaet sant når $n=1$. 

Anta nå at proposisjonen har blitt bevist når $n=m$, hvor $m$ er et gitt naturlig tall. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Vi har: %
%
\begin{align*} %
%
& a_{m+1}x^{m+1} + a_{m}x^{m} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \\
&= x \left( a_{m+1}x^{m} + a_{m}x^{m-1} + \dots + a_{2}x + a_{1} \right) + a_{0}.
\end{align*}  

\item[(2)] Ut ifra antakelsen at lemmaet er sant når $n=m$, finnes det et heltall $r'$ og, for hvert heltall $i$ slik at $0 \leq i \leq m-1$, et heltall $b'_{i}$, slik at %
%
\begin{align*} %
%
& a_{m+1}x^{m} + a_{m}x^{m-1} + \dots + a_{2}x + a_{1} \\
&= (x-y) \left( b'_{m-1}x^{m-1} + b'_{m-2}x^{m-2} + \dots + b'_{1}x + b'_{0} \right) + r'. 
\end{align*}

\item[(3)] La $b_{0}$ være $r'$. For hvert heltall $i$ slik at $1 \leq i \leq m$, la $b_{i}$ være $b'_{i-1}$. La $r$ være $yr' + a_{0}$. Da er %
%
\begin{align*} %
%
& (x-y) \left(b_{m}x^{m} + b_{m-1}x^{m-1} + \dots + b_{2}x^{2} + b_{1}x + b_{0} \right) + r \\
&= (x-y) \left(b_{m}x^{m} + b_{m-1}x^{m-1} + \dots + b_{2}x^{2} + b_{1}x \right) + (x-y)b_{0} + r \\ 
&= x (x-y) \left(b_{m}x^{m-1} + b_{m-1}x^{m-2} + \dots + b_{2}x + b_{1} \right) + xb_{0} - yb_{0} + r \\
&= x \Big( (x-y) \left(b_{m}x^{m-1} + b_{m-1}x^{m-2} + \dots + b_{2}x + b_{1} \right) + b_{0} \Big) -yb_{0} + r \\ 
&= x \Big( (x-y) \left(b'_{m-1}x^{m-1} + b'_{m-2}x^{m-2} + \dots + b'_{1}x + b'_{0} \right) + r' \Big) - yr' + \left( yr'+ a_{0} \right) \\ 
&=  x \Big( (x-y) \left(b'_{m-1}x^{m-1} + b'_{m-2}x^{m-2} + \dots + b'_{1}x + b'_{0} \right) + r' \Big) + a_{0}.
\end{align*}

\item[(4)] Det følger fra (2) at %
%
\begin{align*} %
%
& x \Big( (x-y) \left(b'_{m-1}x^{m-1} + b'_{m-2}x^{m-2} + \dots + b'_{1}x + b'_{0} \right) + r' \Big) + a_{0} \\
&= x \left( a_{m+1}x^{m} + a_{m}x^{m-1} + \dots + a_{2}x + a_{1} \right) + a_{0}.
\end{align*}

\end{itemize} %
%
Det følger fra (1), (3), og (4) at %
%
\begin{align*} %
%
& a_{m+1}x^{m+1} + a_{m}x^{m} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \\
&= (x-y) \left(b_{m}x^{m} + b_{m-1}x^{m-1} + \dots + b_{2}x^{2} + b_{1}x + b_{0} \right) + r.
\end{align*} %
%
Dermed er lemmaet sant når $n=m+1$.  

Ved induksjon konkluderer vi at lemmaet er sant når $n$ er et hvilket som helst naturlig tall.  

\end{proof}

\begin{eks} La $y$ være $3$. Da fastslår Lemma \ref{LemmaDelerMedXMinusY} at det finnes et heltall $r$ og et heltall $b_{0}$ slik at \[ 11x + 8 = (x-3) \cdot b_{0} + r, \] for hvert heltall $x$. Dette er riktigknok sant, ved å la $b_{0}$ være $11$, og $r$ være $41$: det stemmer at \[ 11x + 8 = (x-3) \cdot 11 + 41. \]  \end{eks}

\begin{eks} La $y$ være $-7$. Lemma \ref{LemmaDelerMedXMinusY} fastslår at det finnes et heltall $r$ og heltall $b_{0}$ og $b_{1}$ slik at \[ 5x^{2} + 2x -3 = \big( x-(-7) \big) \left( b_{1}x + b_{0} \right) + r, \] altså at \[ 5x^{2} + 2x -3 = (x+7) \left( b_{1}x + b_{0} \right) + r, \] for hvert heltall $x$. Dette er riktigknok sant, ved å la $b_{0}$ være $-33$, $b_{1}$ være $5$, og $r$ være $228$: det stemmer at \[ 5x^{2} + 2x - 3 = (x+7) \left( 5x -33x \right) + 228. \] \end{eks} 

\begin{eks} La $y$ være $6$. Lemma \ref{LemmaDelerMedXMinusY} fastslår at det finnes et heltall $r$ og heltall $b_{0}$, $b_{1}$, og $b_{2}$ slik at \[ 2x^{3} - 8x^{2} + 5x - 7 = (x-6) \left( b_{2}x^{2} + b_{1}x + b_{0} \right) + r \] for hvert heltall $x$. Dette er riktigknok sant, ved å la $b_{0}$ være $2$, $b_{1}$ være $4$, $b_{2}$ være $29$, og $r$ være $167$: det stemmer at \[  2x^{3} - 8x^{2} + 5x - 7 = (x-6) \left( 2x^{2} +4x + 29 \right) + 167. \] \end{eks} 

\begin{merknad} Utsagnet i Lemma \ref{LemmaDelerMedXMinusY} er: det finnes et heltall $r$ og, for hvert heltall $i$ slik at $0 \leq i \leq n$, et heltall $b_{i}$, slik at %
%
\begin{align*} %
%
& a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0} \\
&= (x-y) \left( b_{n-1}x^{n-1} + b_{n-2}x^{n-2} + \dots + b_{2}x^{2} + b_{1}x + b_{0} \right) + r 
\end{align*} %
%
for {\em hvert} heltall $x$. Dette er ikke det samme som å si: gitt et heltall $x$, finnes det et heltall $r$ og, for hvert heltall $i$ slik at $0 \leq i \leq n$, et heltall $b_{i}$, slik at denne ligningen stemmer. 

Den andre påstanden holder muligheten åpen for at heltallet $r$ og heltallene $b_{i}$ varierer avhengig av $x$. Heltallet $r$ og heltallene $b_{i}$ i Lemma \ref{LemmaDelerMedXMinusY} varierer ikke avhengig av $x$. 

Gitt et heltall $x$, er for eksempel \[ 2x^{2} + x  -1 = (x-1)(2x + 1) + 2x. \] Ved å la $b_{0}$ være $1$,  $b_{1}$ være $2$, og $r$ være $2x$, får vi med andre ord at \[ 2x^{2} + x  - 1  = (x-1)\left( b_{1}x + b_{0} \right) + r. \] Imidlertid varierer da $r$ avhengig av $x$. Hvis for eksempel $x=1$, er $r=2$, og vi har: \[ 2x^{2} + x  -1 = (x-1)(2x + 1) + 2. \] Hvis $x=2$, er $r=4$, og vi har: \[ 2x^{2} + x  -1 = (x-1)(2x + 1) + 4. \] 

Istedenfor kan vi la $b_{0}$ være $1$, $b_{1}$ være $2$, og $r$ være $3$: da har vi \[ 2x^{2} + x -1 = (x-1)(2x+3) + 2. \] I dette tilfellet varierer $r$ ikke avhengig av $x$: uansett hvilket heltall $x$ vi velger, er $r=3$. \end{merknad}

\begin{merknad} Lemma \ref{LemmaDelerMedXMinusY} kan generaliseres. Et uttrkk \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0}, \] hvor $a_{i}$ er et heltall for hvert heltall $i$ slik at $0 \leq i \leq n$, og $x$ er en variabel, kalles et {\em polynom}. Det finnes en divisjonsalgoritme for polynom som bygger på divisjonsalgoritmen for heltall: vi kan dele et polynom med et annet polynom, og får en kvotient som er et polynom og en rest som er et heltall. Lemma \ref{LemmaDelerMedXMinusY} følger umiddelbart fra dette.

Imidlertid kommer vi ikke til å trenge et annet sted divisjonsalgoritmen for polynom. Dessuten må begrepet « «polynom» defineres formelt, og dette er heller ikke noe vi kommer et annet sted til å trenge. Derfor skal vi nøye oss med det direkte beviset vi ga for Lemma \ref{LemmaDelerMedXMinusY}. \end{merknad}

\begin{prpn} \label{ProposisjonLagrangesTeorem} La $p$ være et primtall. La $n$ være et naturlig tall. For hvert heltall $i$ slik at $0 \leq i \leq n$, la $a_{i}$ være et heltall. Anta at det ikke er sant at \[ a_{n} \equiv 0 \pmod{p}. \] Enten har kongruensen \[ a_{n}x_{i}^{n} + a_{n-1}x_{i}^{n-1} + \dots + a_{2} x_{i}^{2} + a_{1} x_{i} + a_{0} \equiv 0 \pmod{p} \] ingen løsning, eller så er der et naturlig tall $l$ slik at $l \leq n$, og heltall $x_{1}$, $x_{2}$, $\ldots$, $x_{l}$, slik at følgende er sanne. %
%
\begin{itemize}

\item[(I)] For hvert naturlig tall $i$ slik at $i \leq l$, er \[ a_{n}x_{i}^{n} + a_{n-1}x_{i}^{n-1} + \dots + a_{2} x_{i}^{2} + a_{1} x_{i} + a_{0} \equiv 0 \pmod{p}. \] 

\item[(II)] La $z$ være et heltall slik at \[ a_{n}z^{n} + a_{n-1}z^{n-1} + \dots + a_{2} z^{2} + a_{1} z + a_{0} \equiv 0 \pmod{p}. \] Da finnes det et naturlig tall $i$ slik at $i \leq l$ og $z \equiv x_{i} \pmod{p}$.  

\end{itemize} 

\end{prpn}

\begin{proof} Først sjekker vi om proposisjonen er sann når $n=1$. La $a_{0}$ og $a_{1}$ være heltall. Siden $p$ er et primtall og det ikke er sant at \[  a_{1} \equiv 0 \pmod{p}, \] følger det fra Proposisjon \ref{ProposisjonKongruensModuloPrimtallEntydigLoesning} at det finnes et heltall $x$ slik at følgende er sanne. %
%
\begin{itemize}

\item[(1)] Vi har: $a_{1}x \equiv -a_{0} \pmod{p}$.

\item[(2)] La $y$ være et heltall slik at $a_{1}y \equiv -a_{0} \pmod{p}$. Da er $x \equiv y \pmod{p}$.

\end{itemize} %
%
Det følger fra (1) og Korollar \ref{KorollarXKongYModNImplisererXPlussZKongYPlussZModN} at \[ a_{1}x + a_{0} \equiv 0 \pmod{p}. \] Således er proposisjonen sann når $n=1$, ved å la $l=1$ og $x_{1} = x$. 

Anta nå at proposisjonen har blitt bevist når $n=m$, hvor $m$ er et gitt naturlig tall. For hvert heltall $i$ slik at $0 \leq i \leq m+1$, la $a_{i}$ være et heltall. Hvis kongruensen \[ a_{m+1}x^{m+1} + a_{m}x^{m} + \dots + a_{2} x^{2} + a_{1} x + a_{0} \equiv 0 \pmod{p} \] har ingen løsning, er proposisjonen sann. Ellers finnes det et heltall $y$ slik at \[ a_{m+1}y^{m+1} + a_{m}y^{m} + \dots + a_{2} y^{2} + a_{1} y + a_{0} \equiv 0 \pmod{p}. \] 

Ut ifra Lemma \ref{LemmaDelerMedXMinusY} finnes det et heltall $r$ og, for hvert naturlig tall $i$ slik at $0 \leq i \leq m$, et heltall $b_{i}$, slik at %
%
\begin{align*} %
%
& a_{m+1}x^{m+1} + a_{m}x^{m} + \dots + a_{2} x^{2} + a_{1} x + a_{0} \\
&= (x-y) \left( b_{m} x^{m} + b_{m-1} x^{m-1} + \dots + b_{2} x^{2} + b_{1} x + b_{0} \right) + r
\end{align*} %
%
for hvert heltall $x$. Ved å la $x=y$, får vi: \[ a_{m+1}y^{m+1} + a_{m}y^{m} + \dots + a_{2} y^{2} + a_{1} y + a_{0} = r. \] Siden \[ a_{m+1}y^{m+1} + a_{m}y^{m} + \dots + a_{2} y^{2} + a_{1} y + a_{0} \equiv 0 \pmod{p}, \] følger det at \[ r \equiv 0 \pmod{p}. \] %
%
Ut ifra Korollar \ref{KorollarXKongYModNImplisererXPlussZKongYPlussZModN} er dermed   %
%
\begin{align*} %
%
& a_{m+1}x^{m+1} + a_{m}x^{m} + \dots + a_{2} x^{2} + a_{1} x + a_{0} \\
&\equiv (x-y) \left( b_{m} x^{m} + b_{m-1} x^{m-1} + \dots + b_{2} x^{2} + b_{1} x + b_{0} \right) \pmod{p} 
\end{align*} %
%
for hvert heltall $x$.

Anta først at kongruensen \[ b_{m} x^{m} + b_{m-1} x^{m-1} + \dots + b_{2} x^{2} + b_{1} x + b_{0} \equiv 0 \pmod{p} \] har ingen løsning. Da er proposisjonen sann når $n=m+1$, ved å la $l$ være $1$ og $x_{1}$ være $y$. 

Anta istedenfor at kongruensen \[ b_{m} x^{m} + b_{m-1} x^{m-1} + \dots + b_{2} x^{2} + b_{1} x + b_{0} \equiv 0 \pmod{p} \] har minst én løsning. Siden det ikke er sant at \[ a_{m+1} \equiv 0 \pmod{p}, \] er det ikke sant at \[ b_{m} \equiv 0 \pmod{p}. \] Ut ifra antakelsen at proposisjonen er sann når $n=m$, finnes det derfor et naturlig tall $l'$ og, for hvert naturlig tall $i$ slik at $i \leq l'$, et heltall $y_{i}$, slik at følgende er sanne.  %
%
\begin{itemize}

\item[(A)] For hvert naturlig tall $i$ slik at $i \leq l'$, er \[ b_{m}y_{i}^{m} + b_{m-1}y_{i}^{m-1} + \dots + b_{2} y_{i}^{2} + b_{1} y_{i} + b_{0} \equiv 0 \pmod{p}. \] 

\item[(B)] La $z$ være et heltall slik at \[ b_{m}z^{m} + b_{m-1}z^{m-1} + \dots + b_{2} z^{2} + b_{1} z + b_{0} \equiv 0 \pmod{p}. \]  Da finnes det et naturlig tall $i$ slik at $i \leq l'$ og $z \equiv y_{i} \pmod{p}$.  

\end{itemize} %
%
La da $l$ være $l'+1$. For hvert naturlig tall $i$ slik at $i \leq l'$, la $x_{i}$ være $y_{i}$. La $x_{l}$ være $y$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Det følger fra (A) og Korollar \ref{KorollarXKongYModNImplisererXGangerMedZKongYGangerMedZModN} at, for hvert naturlig tall $i$ slik at $i \leq l'$, er \[ (y_{i}-y) \left( b_{m} y_{i}^{m} + b_{m-1} y_{i}^{m-1} + \dots + b_{2} y_{i}^{2} + b_{1} y_{i} + b_{0} \right) \equiv (x-y) \cdot 0 \pmod{p}, \] altså \[ (y_{i}-y) \left( b_{m} y_{i}^{m} + b_{m-1} y_{i}^{m-1} + \dots + b_{2} y_{i}^{2} + b_{1} y_{i} + b_{0} \right) \equiv 0 \pmod{p}. \] %
%
Dermed er %
%
\[ (x_{i} - y) \left( b_{m} x_{i}^{m} + b_{m-1} x_{i}^{m-1} + \dots + b_{2} x_{i}^{2} + b_{1} x_{i} + b_{0} \right) \equiv 0 \pmod{p} \] %
%
for hvert naturlig tall $i$ slik at $i \leq l-1$. Siden %
%
\begin{align*} %
%
& a_{m+1}x_{i}^{m+1} + a_{m}x_{i}^{m} + \dots + a_{2} x_{i}^{2} + a_{1} x_{i} + a_{0} \\
&\equiv (x_{i} - y) \left( b_{m} x_{i}^{m} + b_{m-1} x_{i}^{m-1} + \dots + b_{2} x_{i}^{2} + b_{1} x_{i} + b_{0} \right) \pmod{p}, 
\end{align*} %
%
følger det fra Korollar \ref{ProposisjonXKongYOgYonZModNImplisererXKongZModN} at \[ a_{m+1}x_{i}^{m+1} + a_{m}x_{i}^{m} + \dots + a_{2} x_{i}^{2} + a_{1} x_{i} + a_{0} \equiv 0 \pmod{p} \] for hvert naturlig tall $i$ slik at $i \leq l-1$.  

\item[(2)] Siden \[ a_{m+1}y^{m+1} + a_{m}y^{m} + \dots + a_{2} y^{2} + a_{1}y + a_{0} \equiv 0 \pmod{p}, \] er \[ a_{m+1}x_{l}^{m+1} + a_{m}x_{l}^{m} + \dots + a_{2} x_{l}^{2} + a_{1} x_{l} + a_{0} \equiv 0 \pmod{p}. \] 

\end{itemize} %
%
For hvert naturlig tall $i$ slik at $i \leq l$, er således \[ a_{m+1}x_{i}^{m+1} + a_{m}x_{i}^{m} + \dots + a_{2} x_{i}^{2} + a_{1} x_{i} + a_{0} \equiv 0 \pmod{p}. \] Dermed er (I) sant.

La nå $z$ være et heltall slik at \[ a_{m+1}z^{m+1} + a_{m}z^{m} + \dots + a_{2} z^{2} + a_{1} x + a_{0} \equiv 0 \pmod{p}. \] Siden %
%
\begin{align*} %
%
& a_{m+1}z^{m+1} + a_{m}z^{m} + \dots + a_{2} z^{2} + a_{1} x + a_{0} \\
&= (z-y) \left( b_{m} z^{m} + b_{m-1} z^{m-1} + \dots + b_{2} z^{2} + b_{1} z + b_{0} \right) \\
\end{align*} %
%
er da \[ (z-y) \left( b_{m} z^{m} + b_{m-1} z^{m-1} + \dots + b_{2} z^{2} + b_{1} z + b_{0} \right) \equiv 0 \pmod{p}. \] Anta at det ikke er sant at \[ z \equiv y \pmod{p}. \] Ut ifra Korollar \ref{KorollarXKongYModNImplisererXPlussZKongYPlussZModN} er det da ikke sant at \[ z -y \equiv 0 \pmod{p}. \] Det følger da fra Proposisjon \ref{ProposisjonKanDeleModuloEtPrimtall} at \[ b_{m} z^{m} + b_{m-1} z^{m-1} + \dots + b_{2} z^{2} + b_{1} z + b_{0} \equiv 0 \pmod{p}. \] %
% 
Fra denne kongruensen og (B) deduserer vi at det finnes et naturlig tall $i$ slik at $i \leq l'$ og \[ z \equiv y_{i} \pmod{p}, \] altså slik at $i \leq l-1$ og \[ z \equiv x_{i} \pmod{p}. \] 

Vi har således bevist: dersom \[ a_{m+1}z^{m+1} + a_{m}z^{m} + \dots + a_{2} z^{2} + a_{1} x + a_{0} \equiv 0 \pmod{p}, \] er enten \[ z \equiv y \pmod{p}, \] altså \[ z \equiv x_{l} \pmod{p}, \] eller så finnes det et naturlig tall $i$ slik at $i \leq l-1$ og \[ z \equiv x_{i} \pmod{p}. \] Dermed er (II) er sant. 

Således er proposisjonen sann når $n=m+1$. Ved induksjon konkluderer vi at proposisjonen er sann for et hvilket som helst naturlig tall $n$.

  \end{proof}

\begin{terminologi} Proposisjon \ref{ProposisjonLagrangesTeorem} kalles {\em Lagranges teorem}. \end{terminologi}  

\begin{merknad} Med andre ord fastslår Proposisjon \ref{ProposisjonLagrangesTeorem} at, dersom det ikke er sant at \[ a_{n} \equiv 0 \pmod{p}, \] finnes det maksimum $n$ løsninger til kongruensen \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2} x^{2} + a_{1} x + a_{0} \equiv 0 \pmod{p} \] slik at ikke noe par av disse løsningene er kongruent til hverandre modulo $p$, og slik at enhver annen løsning er kongruent modulo $p$ til én av disse løsningene. \end{merknad}

\begin{terminologi} Anta at kongruensen \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2} x^{2} + a_{1} x + a_{0} \equiv 0 \pmod{p} \] har $m$ løsninger, hvor $m$ er et heltall slik at $0 \leq m \leq n$, slik at ikke noe par av disse $m$ løsningene er kongruent til hverandre modulo $p$, og slik at enhver annen løsning er kongruent modulo $p$ til én av disse $m$ løsningene. Da sier vi at kongruensen  \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2} x^{2} + a_{1} x + a_{0} \equiv 0 \pmod{p} \] har {\em $m$ løsninger modulo $p$}.  
 \end{terminologi}

\begin{merknad} Ved å benytte denne terminologien, fastslår Proposisjon \ref{ProposisjonLagrangesTeorem} at, dersom det ikke er sant at \[ a_{n} \equiv 0 \pmod{p}, \] finnes det maksimum $n$ løsninger modulo $p$ til kongruensen \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2} x^{2} + a_{1} x + a_{0} \equiv 0 \pmod{p}. \]  \end{merknad}  

\begin{eks} Proposisjon \ref{ProposisjonLagrangesTeorem} fastslår at kongruensen \[ -3x^{2} + 7x -17 \equiv 0 \pmod{5} \] har maksimum to løsninger $p$. For å vise om dette er sant, er det, ut ifra Proposisjon \ref{ProposisjonPolynomIEenVariabelKongNullHarLoesningModMFoererTilAtDetHarLoesningMellom0OgM}, nok å sjekke hvilke av heltallene $1$, $2$, $\ldots$, $4$ er løsninger. 

Vi har følgende. Alle kongruensene er modulo $5$.

\begin{center}
\begin{tabular}{lll}
\toprule
$x$ & $-3x^{2} + 7x - 17$ & Løsning modulo $5$?  \\
\midrule
$1$ & $-13 \equiv 2$ & \cross \\
$2$ & $-15 \equiv 0$ & \tick \\
$3$ & $-23 \equiv 2$ & \cross \\
$4$ & $-37 \equiv 3$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Således har kongruensen \[ -3x^{2} + 7x -17 \equiv 0 \pmod{5} \] én løsning modulo $5$. 
       
\end{eks}

\begin{eks} Proposisjon \ref{ProposisjonLagrangesTeorem} fastslår at kongruensen \[ 2x^{2} + 3x + 5 \equiv 0 \pmod{7} \] har maksimum to løsninger. For å vise om dette er sant, er det, ut ifra Proposisjon \ref{ProposisjonPolynomIEenVariabelKongNullHarLoesningModMFoererTilAtDetHarLoesningMellom0OgM}, nok å sjekke hvilke av heltallene $1$, $2$, $\ldots$, $6$ er løsninger. Vi har følgende. Alle kongruensene er modulo $7$.

\begin{center}
\begin{tabular}{lll}
\toprule
$x$ & $2x^{2} + 3x + 5$ & Løsning modulo $7$?  \\
\midrule
$1$ & $10 \equiv 3$ & \cross \\
$2$ & $19 \equiv 5$ & \cross \\
$3$ & $32 \equiv 4$ & \cross \\
$4$ & $49 \equiv 0$ & \tick \\
$5$ & $70 \equiv 0$ & \tick \\
$6$ & $95 \equiv 4$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Således har kongruensen \[ 2x^{2} + 3x + 5  \equiv 0 \pmod{7} \] to løsninger modulo $7$.
       
\end{eks} 

\begin{eks} Proposisjon \ref{ProposisjonLagrangesTeorem} fastslår at kongruensen \[ 5x^{2} + 7x + 6 \equiv 0 \pmod{13} \] har maksimum to løsninger. For å vise om dette er sant, er det, ut ifra Proposisjon \ref{ProposisjonPolynomIEenVariabelKongNullHarLoesningModMFoererTilAtDetHarLoesningMellom0OgM}, nok å sjekke hvilke av heltallene $1$, $2$, $\ldots$, $12$ er løsninger. 

Vi har følgende. Alle kongruensene er modulo $13$.

\begin{center}
\begin{tabular}{lll}
\toprule
$x$ & $5x^{2} + 7x + 6$ & Løsning modulo $13$?  \\
\midrule
$1$ & $18 \equiv 5$ & \cross \\
$2$ & $40 \equiv 1$ & \cross \\
$3$ & $72 \equiv 7$ & \cross \\
$4$ & $114 \equiv 10$ & \cross \\
$5$ & $166 \equiv 10$ & \cross \\
$6$ & $228 \equiv 7$ & \cross \\
$7$ & $300 \equiv 1$ & \cross \\
$8$ & $382 \equiv 5$ & \cross \\
$9$ & $474 \equiv 6$ & \cross \\
$10$ & $576 \equiv 4$ & \cross \\
$11$ & $688 \equiv 12$ & \cross \\
$12$ & $810 \equiv 4$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Således har kongruensen \[ 5x^{2} + 7x + 6  \equiv 0 \pmod{13} \] ingen løsning modulo $13$.
\end{eks} 

\begin{eks} Proposisjon \ref{ProposisjonLagrangesTeorem} fastslår at kongruensen \[ x^{3} - x^{2} + x + 1 \equiv 0 \pmod{11} \] har maksimum tre løsninger. For å vise om dette er sant, er det, ut ifra Proposisjon \ref{ProposisjonPolynomIEenVariabelKongNullHarLoesningModMFoererTilAtDetHarLoesningMellom0OgM}, nok å sjekke hvilke av heltallene $1$, $2$, $\ldots$, $10$ er løsninger. 

Vi har følgende. Alle kongruensene er modulo $11$.

\begin{center}
\begin{tabular}{lll}
\toprule
$x$ & $x^{3} - x^{2} + x +1$ & Løsning modulo $11$?  \\
\midrule
$1$ & $2$ & \cross \\
$2$ & $7$ & \cross \\
$3$ & $22 \equiv 0$ & \tick \\
$4$ & $53 \equiv 9$ & \cross \\
$5$ & $106 \equiv 7$ & \cross \\
$6$ & $187 \equiv 0$ & \tick \\
$7$ & $302 \equiv 5$ & \cross \\
$8$ & $457 \equiv 6$ & \cross \\
$9$ & $658 \equiv 9$ & \cross \\
$10$ & $911 \equiv 7$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Således har kongruensen \[ x^{3} - x^{2} + x + 1 \equiv 0 \pmod{11}  \] to løsninger modulo $11$.
\end{eks} 

\begin{merknad} Hvis vi ikke jobber modulo $p$, og se istedenfor på ligningen \[ a_{n}x^{n} + a_{n-1}x^{n-1} + \dots + a_{2}x^{2} + a_{1}x + a_{0} = 0, \] fører akkurat det samme argumentet som i beviset for Proposisjon \ref{ProposisjonLagrangesTeorem} til et bevis for faktumet nevnt i Merknad \ref{MerkandPolynomLiktNullHarMaksNLoesninger}: at denne ligningen har maksimum $n$ løsninger. \end{merknad} 

\begin{merknad} Proposisjon \ref{ProposisjonLagrangesTeorem} er ikke nødvendigvis sann om vi ikke antar at $p$ er et primtall. La oss se for eksempel på kongruensen \[ x^{2} +x - 2 \equiv 0 \pmod{10}. \] Vi har følgende. Alle kongruensene er modulo $10$. 

\begin{center}
\begin{tabular}{lll}
\toprule
$x$ & $x^{2} + x - 2$ & Løsning modulo $10$?  \\
\midrule
$1$ & $0$ & \tick \\
$2$ & $4$ & \cross \\
$3$ & $10 \equiv 0$ & \tick \\
$4$ & $18 \equiv 8$ & \cross \\
$5$ & $28 \equiv 8$ & \cross \\
$6$ & $40 \equiv 0$ & \tick \\
$7$ & $54 \equiv 4$ & \cross \\
$8$ & $70 \equiv 0$ & \tick \\
$9$ & $88 \equiv 8$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Således har kongruensen \[ x^{2} + x -2 \equiv 0 \pmod{10}  \] fire løsninger modulo $10$.

\end{merknad} 

\section{Wilsons teorem}

\begin{merknad} Kanskje ser Lagranges teorem temmelig unøyaktig ut. Det sier ikke hvor mange løsninger kongruensen \[ a_{n}x_{i}^{n} + a_{n-1}x_{i}^{n-1} + \dots + a_{2} x_{i}^{2} + a_{1} x_{i} + a_{0} \equiv 0 \pmod{p} \] har, og sier ikke hvordan eventuelle løsninger kan finnes. 

Derfor er det lett å tro at Lagranges teorem derfor ikke er så nyttig. Imidlertid kommer vi nå til å se at Lagranges teorem kan benyttes for å gi et bevis for Proposisjon \ref{ProposisjonWilsonsTeorem}, som er både konkret og eksakt. Beviset for Proposisjon \ref{ProposisjonWilsonsTeorem} benytter altså, på en interessant måte, et overslag vi får ved å benytte Lagranges teorem som et steg mot å fastslå at den nøyaktige kongruensen i proposisjonen stemmer. 

Først må vi gjøre noen forberedelser. 
 \end{merknad}

\begin{lem} \label{LemmaProduktNMinus1NMinus2TilNMinusNMinus1} La $n$ være et naturlig tall slik at $n \geq 2$. La $x$ være et heltall. Det finnes heltall $a_{0}$, $a_{1}$, $\ldots$, $a_{n-2}$ slik at: %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x-(n-1) \big) \\
&= x^{n-1} + a_{n-2} x^{n-2} + a_{n-3} x^{n-3} + \cdots + a_{2} x^{2} + a_{1} x + a_{0}. 
\end{align*}
 
\end{lem}

\begin{proof} Først sjekker vi om lemmaet er sant når $n=2$. I dette tilfellet er utsagnet at det finnes et heltall $a_{0}$ slik at \[ x-1 = x - a_{0}. \] Ved å la $a_{0}$ være $1$, ser vi at dette riktignok er sant. 

Anta nå at lemmaet har blitt bevist når $n=m$, hvor $m$ er et gitt naturlig tall slik at $m \geq 2$. Således har det blitt bevist at det finnes heltall $b_{0}$, $b_{1}$, $\ldots$, $b_{m-2}$ slik at: %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x-(m-1) \big) \\
&= x^{m-1} + b_{m-2} x^{m-2} + b_{m-3} x^{m-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0}.
\end{align*} %
%
Da er %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots ( x-m ) \\
&= \Big( (x-1)(x-2) \cdots \big(x - (m-1) \big) \Big) \cdot (x-m) \\
&= \left( x^{m-1} + b_{m-2} x^{m-2} + b_{m-3} x^{m-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0} \right) \cdot (x-m). 
\end{align*} %
%
Produktet \[ \left( x^{m-1} + b_{m-2} x^{m-2} + b_{m-3} x^{m-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0} \right) \cdot (x-m) \] er likt summen av \[ x \left( x^{m-1} + b_{m-2} x^{m-2} + b_{m-3} x^{m-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0} \right) \] og \[ - m \left( x^{m-1} + b_{m-2} x^{m-2} + b_{m-3} x^{m-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0} \right), \] %
%
altså summen av  \[ \left( x^{m} + b_{m-2} x^{m-1} + b_{m-3} x^{m-2} + \cdots + b_{2} x^{3} + b_{1} x^{2} + b_{0}x \right) \] og \[ - \left( mx^{m-1} + mb_{m-2} x^{m-2} + mb_{m-3} x^{m-3} + \cdots + mb_{2} x^{2} + mb_{1} x + mb_{0} \right). \] %
%
Denne summen er lik \[ x^{m} + \left(b_{m-2} + m \right)x^{m-1} + \left( b_{m-3} + mb_{m-2} \right) x^{m-2}  + \cdots + \left( b_{1} + mb_{2} \right)x^{2} + \left( b_{0} + mb_{1} \right) x + mb_{0}.  \] %
%
Dermed har vi vist at %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots ( x-m ) \\ 
&= x^{m} + \left(b_{m-2} + m \right)x^{m-1} + \left( b_{m-3} + mb_{m-2} \right) x^{m-2}  + \cdots + \left( b_{1} + mb_{2} \right)x^{2} + \left( b_{0} + mb_{1} \right) x + mb_{0}.
\end{align*} %
%
La $a_{0}$ være $mb_{0}$. For hvert naturlig tall $i$ slik at $i \leq m-2$, la $a_{i}$ være $b_{i-1} + mb_{i}$. La $a_{m-1}$ være $b_{m-2} + m$. Da er %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots ( x-m ) \\ 
&= x^{m} + a_{m-1} x^{m-1} + a_{m-2} x^{m-2} + \cdots + a_{2} x^{2} + a_{1} x + a_{0}. 
\end{align*} %
%
Dermed er lemmaet sant når $n=m+1$. 

Ved induksjon konkluderer vi at lemmaet er sant for alle de naturlige tallene $n$ slik at $n \geq 2$.  
      
\end{proof}

\begin{eks} Lemma \ref{LemmaProduktNMinus1NMinus2TilNMinusNMinus1} fastslår at det finnes heltall $a_{0}$ og $a_{1}$ slik at \[ (x-1)(x-2) = x^{2} + a_{1}x + a_{0}. \] Dette er riktignok sant: \[ (x-1)(x-2) = x^{2} - 3x + 2, \] altså kan vi la $a_{0}$ være $2$ og $a_{1}$ være $-3$. \end{eks} 

\begin{eks} Lemma \ref{LemmaProduktNMinus1NMinus2TilNMinusNMinus1} fastslår at det finnes heltall $a_{0}$, $a_{1}$, $a_{2}$ slik at \[ (x-1)(x-2)(x-3) = x^{3} + a_{2}x^{2} + a_{1}x + a_{0}. \] Dette er riktignok sant: \[ (x-1)(x-2)(x-3) = x^{3} - 6x^{2} + 11x - 6, \] altså kan vi la $a_{0}$ være $-6$, $a_{1}$ være $11$, og $a_{2}$ være $-6$. \end{eks} 

\begin{kor} \label{KorollarProduktNMinus1NMinus2TilNMinusNMinus1} La $n$ være et naturlig tall slik at $n \geq 2$. La $x$ være et heltall. Det finnes heltall $a_{0}$, $a_{1}$, $\ldots$, $a_{n-2}$ slik at: %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x-(n-1) \big) - \left( x^{n-1} - 1 \right) \\
&= a_{n-2} x^{n-2} + a_{n-3} x^{n-3} + \cdots + a_{2} x^{2} + a_{1} x + a_{0}. 
\end{align*}
 
\end{kor} 

\begin{proof} Ut ifra Lemma \ref{LemmaProduktNMinus1NMinus2TilNMinusNMinus1} finnes det heltall $b_{0}$, $b_{1}$, $\ldots$, $b_{n-1}$ slik at %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x-(n-1) \big) \\
&= x^{n-1} + b_{n-2} x^{n-2} + b_{n-3} x^{n-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0}. 
\end{align*} 
%
Da er %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x-(n-1) \big) - \left( x^{n-1} - 1 \right) \\
&= \left( x^{n-1} + b_{n-2} x^{n-2} + b_{n-3} x^{n-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0} \right) - x^{n-1} + 1 \\
&= b_{n-2} x^{n-2} + b_{n-3} x^{n-3} + \cdots + b_{2} x^{2} + b_{1} x + b_{0} + 1. 
\end{align*} %
%
La $a_{0} = b_{0} + 1$. For hvert naturlig tall $i$ slik at $i \leq n-2$, la $a_{i} = b_{i}$. Da er %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x-(n-1) \big) - \left( x^{n-1} - 1 \right) \\
&= a_{n-2} x^{n-2} + a_{n-3} x^{n-3} + \cdots + a_{2} x^{2} + a_{1} x + a_{0}. 
\end{align*}

\end{proof}

\begin{eks} Korollar \ref{KorollarProduktNMinus1NMinus2TilNMinusNMinus1} fastslår at det finnes heltall $a_{0}$ og $a_{1}$ slik at \[ (x-1)(x-2) - \left(x^{2} - 1 \right) = a_{1}x + a_{0}. \] Dette er riktignok sant: \[ (x-1)(x-2) - \left( x^{2} - 1 \right) = -3x + 3, \] altså kan vi la $a_{0}$ være $-3$ og $a_{1}$ være $3$. \end{eks} 

\begin{eks} Korollar \ref{KorollarProduktNMinus1NMinus2TilNMinusNMinus1} fastslår at det finnes heltall $a_{0}$, $a_{1}$, $a_{2}$ slik at \[ (x-1)(x-2)(x-3) - \left( x^{3} - 1 \right) = a_{2}x^{2} + a_{1}x + a_{0}. \] Dette er riktignok sant: \[ (x-1)(x-2)(x-3) = - 6x^{2} + 11x - 5, \] altså kan vi la $a_{0}$ være $-6$, $a_{1}$ være $11$, og $a_{2}$ være $-5$. \end{eks} 

\begin{prpn} \label{ProposisjonWilsonsTeorem} La $p$ være et primtall. Da er \[ (p-1)! \equiv -1 \pmod{p}. \] \end{prpn}

\begin{proof} Anta først at $p=2$. Vi har: \[ (2-1)! - (-1) = 1! - (-1) = 1 + 1 = 2. \] Siden $2 \mid 2$, deduserer vi at \[ (2-1)! \equiv -1 \pmod{2}. \] Dermed er proposisjonen sann i dette tilfellet.

Anta nå at $p > 2$. La $x$ være et heltall. Ut ifra Korollar \ref{KorollarProduktNMinus1NMinus2TilNMinusNMinus1} finnes det heltall  $a_{0}$, $a_{1}$, $\ldots$, $a_{p-2}$ slik at %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x- (p-1) \big) - \left( x^{p-1} - 1 \right) \\
&= a_{p-2} x^{p-2} + a_{p-3} x^{p-3} + \cdots + a_{2} x^{2} + a_{1} x + a_{0}.
\end{align*} %
%
Anta at det ikke er sant at \[ a_{i} \equiv 0 \pmod{p} \] for alle heltallene $i$ slik at $0 \leq i \leq p-2$. La da $m$ være det største heltallet slik at: %
%
\begin{itemize}

\item[(i)] $0 \leq m \leq p-2$;

\item[(ii)] det ikke er sant at \[ a_{m} \equiv 0 \pmod{p}. \] 

\end{itemize} %
%
Da er %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x- (p-1) \big) - \left( x^{p-1} - 1 \right) \\
&= a_{m} x^{m} + a_{m-1} x^{m-1} + \cdots + a_{2} x^{2} + a_{1} x + a_{0}. 
\end{align*} 

For hvert naturlig tall $r$ slik at $r \leq p-1$ er følgende sanne. %
%
\begin{itemize}

\item[(1)] Siden $(r-r) = 0$, er \[ (x-1)(x-2) \cdots \big( x-(p-1) \big) = 0. \]

\item[(2)] Ut ifra Korollar \ref{KorollarFermatsLilleTeoremPMinus1} er \[ r^{p-1} \equiv 1 \pmod{p}. \] Dermed er \[ r^{p-1} -1 \equiv 1 -1 \pmod{p}, \] altså \[ r^{p-1} -1 \equiv 0 \pmod{p}. \] 

\item[(3)] Det følger fra (1) og (2) at \[ (r-1)(r-2) \cdots \big( r-(p-1) \big) - \left( r^{p-1} - 1 \right) \equiv 0 \pmod{p}. \] 

\end{itemize} %
%
For hvert naturlig tall $r$ slik at $r \leq p-1$, er dermed $x=r$ en løsning til kongruensen \[ (x-1)(x-2) \cdots \big( x- (p-1) \big) - \left( x^{p-1} - 1 \right) \equiv 0 \pmod{p}, \] altså til kongruensen \[ a_{m} x^{m} + a_{m-1} x^{m-1} + \cdots + a_{2} x^{2} + a_{1} x + a_{0} \pmod{p}. \] Således har kongruensen \[ a_{m} x^{m} + a_{m-1} x^{m-1} + \cdots + a_{2} x^{2} + a_{1} x + a_{0} \pmod{p} \] minst $p-1$ løsninger. 

Siden det ikke er sant at \[ a_{m} \equiv 0 \pmod{p}, \] følger det på en annen side fra Proposisjon \ref{ProposisjonLagrangesTeorem} at kongruensen \[ a_{m} x^{m} + a_{m-1} x^{m-1} + \cdots + a_{2} x^{2} + a_{1} x + a_{0} \pmod{p} \] har maksimum $m$ løsninger. Vi har: $m \leq p-2$. Dermed har vi en motsigelse: kongruensen  \[ a_{m} x^{m} + a_{m-1} x^{m-1} + \cdots + a_{2} x^{2} + a_{1} x + a_{0} \pmod{p} \] kan ikke ha både minst $p-1$ løsninger og maksimum $p-2$ løsninger. 
 
Vi har således bevist at antakelsen at det ikke er sant at \[ a_{i} \equiv 0 \pmod{p} \] for alle heltallene $i$ slik at $0 \leq i \leq p-2$ fører til en motsigelse. Derfor er \[ a_{i} \equiv 0 \pmod{p} \] for alle heltallene $i$ slik at $0 \leq i \leq p-2$.

Det følger fra Korollar \ref{KorollarXKongYModNImplisererXGangerMedZKongYGangerMedZModN} og Proposisjon \ref{ProposisjonXKongYOgXXKongYYModNImplisererXPlussXXKongYPlussYYModN} at, for et hvilket som helst heltall $x$, er da %
%
\begin{align*} %
%
& (x-1)(x-2) \cdots \big( x- (p-1) \big) - \left( x^{p-1} - 1 \right) \\
&\equiv 0 \cdot x^{p-2} + 0 \cdot x^{p-3} + \cdots + 0 \cdot x^{2} + 0 \cdot x + 0 \pmod{p},
\end{align*} %
%
altså \[  (x-1)(x-2) \cdots \big( x- (p-1) \big) - \left( x^{p-1} - 1 \right) \equiv 0 \pmod{p}. \] 

La $x=0$. Ut ifra den foregående proposisjonen er \[  (0-1)(0-2) \cdots \big( 0 - (p-1) \big) - \left( ^{p-1} - 1 \right) \equiv 0 \pmod{p}, \]  altså \[  (-1)^{p-1} \big (1 \cdot 2 \cdots (p-1) \big) +1 \equiv 0 \pmod{p}. \] Ut ifra Korollar \ref{KorollarXKongYModNImplisererXPlussZKongYPlussZModN} er dermed \[ (-1)^{p-1}(p-1)! \equiv -1 \pmod{p}. \]

Ut ifra Proposisjon \ref{ProposisjonPMinus1DeleligMed2OmPPrimtallStoerreEnn2}, finnes det et naturlig tall $k$ slik at $p-1=2k$. Derfor er \[ (-1)^{p-1} = (-1)^{2k} = \left( (-1)^{2} \right)^{k} = 1^{k} = 1. \] Vi konkluderer at \[ (p-1)! \equiv -1 \pmod{p}. \] 

\end{proof} 

\begin{terminologi} Proposisjon \ref{ProposisjonWilsonsTeorem} kalles {\em Wilsons teorem}. \end{terminologi}

\begin{eks} Siden $3$ er et primtall og $3 > 2$, fastslår Proposisjon \ref{ProposisjonWilsonsTeorem} at \[ (3-1)! \equiv -1 \pmod{3}. \] Siden $(3-1)! = 2! = 2$ og \[ 2 \equiv -1 \pmod{3}, \] er dette riktignok sant.   \end{eks}

\begin{eks} Siden $5$ er et primtall og $5 > 2$, fastslår Proposisjon \ref{ProposisjonWilsonsTeorem} at \[ (5-1)! \equiv -1 \pmod{5}. \] Siden $(5-1)! = 4! = 24$ og \[ 24 \equiv -1 \pmod{5}, \] er dette riktignok sant.   \end{eks}

\begin{eks} Siden $7$ er et primtall og $7 > 2$, fastslår Proposisjon \ref{ProposisjonWilsonsTeorem} at \[ (7-1)! \equiv -1 \pmod{7}. \] Siden $(7-1)! = 6! = 720$ og \[ 720 \equiv -1 \pmod{7}, \] er dette riktignok sant.   \end{eks}

\begin{prpn} \label{Proposisjon2Ganger26FakultetPluss1DeleligMed29} Det naturlige tallet \[ 2 \cdot (26!) + 1 \] er delelig med $29$.  \end{prpn}

\begin{proof} Vi gjør følgende observasjoner.  %
%
\begin{itemize}

\item[(1)] Vi har: $2 = (-1) \cdot (-2)$. Derfor er \[ 2 \cdot (26!) = (-1) \cdot (-2) \cdot (26!). \]

\item[(2)] Vi har: \[ -1 \equiv 28 \pmod{29} \] og \[ -2 \equiv 27 \pmod{29}. \]

\item[(3)] Det følger fra (2) og Proposisjon \ref{ProposisjonXKongYOgXXKongYYModNImplisererXGangerMedXXKongYGangerMedYYModN} at \[ (-1) \cdot (-2) \equiv 28 \cdot 27 \pmod{29}. \] 

\item[(4)] Det følger fra (3) og Korollar \ref{KorollarXKongYModNImplisererXGangerMedZKongYGangerMedZModN} at \[ (-1) \cdot (-2) \cdot (26!) \equiv 28 \cdot 27 \cdot 26! \pmod{29}. \] Siden $28 \cdot 27 \cdot (26!) = 28!$, er dermed  \[ (-1) \cdot (-2) \cdot (26!) \equiv 28! \pmod{29}. \] 

\item[(5)] Siden $29$ er et primtall, følger det fra Proposisjon \ref{ProposisjonWilsonsTeorem} at \[ 28! \equiv -1 \pmod{29}. \]           

\item[(6)] Det følger fra (4), (5), og Proposisjon \ref{ProposisjonXKongYOgYonZModNImplisererXKongZModN} at \[ (-1) \cdot (-2) \cdot (26!) \equiv -1 \pmod{29}. \] 

\end{itemize} %
%
Det følger fra (1) og (6) at \[ 2 \cdot (26!) \equiv -1 \pmod{29}. \] Ut ifra Korollar \ref{KorollarXKongYModNImplisererXPlussZKongYPlussZModN} er da \[ 2 \cdot (26!) + 1 \equiv -1 + 1 \pmod{29}, \] altså \[ 2 \cdot (26!) + 1 \equiv 0 \pmod{29}. \] Vi konkluderer at $29 \mid 2 \cdot(26!) + 1$.   \end{proof}

\begin{merknad} Det er naturlig å se først på Wilsons teorem som er artig, men ikke så viktig fra et teoretisk synspunkt. Imidlertid kommer til å benytte Wilsons teorem i løpet av vårt bevis for det dypeste teoremet i kurset, Teorem \ref{TeoremKvadratiskGjensidighet}! \end{merknad}
%forelesning18A
