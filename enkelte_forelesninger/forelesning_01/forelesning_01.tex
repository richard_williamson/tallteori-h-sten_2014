%forelesning01
\section{Naturlige tall og heltall} \label{DelNaturligeTallOgHeltall}

\begin{defn} \label{DefinisjonNaturligeTall} Et {\em naturlig tall} er et av tallene: $1$, $2$, $\ldots$. \end{defn}
 
\begin{merknad} Legg spesielt merke til at i dette kurset teller vi ikke $0$ iblant de naturlige tallene. Allikevel er det noen som ser på $0$ som et naturlig tall. Å inkludere det eller ikke er bare en konvensjon, og ikke noe å bekymre seg for. Noen ganger inkluderer jeg selv det, og noen ganger ikke! \end{merknad}

\begin{defn} \label{DefinisjonHeltall} Et {\em heltall} er et av tallene: $\dots, -2, -1, 0, 1, 2, \dots$. \end{defn}
 
\begin{merknad} Alle naturlige tall er heltall. Men ikke alle heltall er naturlige tall: de negative heltallene er ikke naturlige tall. Ifølge Definisjon \ref{DefinisjonNaturligeTall} er $0$ heller ikke et naturlig tall. \end{merknad}

\begin{notn} La $m$ og $n$ være heltall. Vi skriver $m$ ganger $n$ som $mn$, $m \cdot n$, eller $m \times n$. \end{notn}

\section{Bevis}

\begin{merknad} Matematikk er som et gigantisk byggverk i murstein. Det bygges opp på følgende måte.
 
\begin{itemize}

\item[(1)] Matematiske påstander formuleres. Disse er mursteinene til byggverket.

\item[(2)] Disse påstandene bevises, ved hjelp av matematiske påstander som allerede har blitt bevist. Bevisene er sementen som binder mursteinene sammen.

\end{itemize}  

\end{merknad}

\begin{terminologi} Å {\em bevise} at en matematisk påstand er sann betyr å vise, ved å benytte gitte logiske prinsipper, at den er sann fra påstander som allerede har blitt bevist, og fra definisjonene av tingene påstanden handler om. Typisk blir et bevis bygd opp steg for steg i en rekke deduksjoner. \end{terminologi}    

\begin{eks} La $n$ være et naturlig tall. Et eksempel på en matematisk påstand er: \[ n+4> n+3. \] At denne påstanden er sann følger logisk fra de følgende to påstandene: 

\begin{itemize}

\item[(1)] $4>3$.

\item[(2)] For hvilke som helst naturlige tall $n$, $k$, og $l$ slik at $k > l$, er $n +k > n+ l$.

\end{itemize} %
%
Logikken er som følger: siden $4 > 3$ kan vi ta $k$ som $4$ og $l$ som $3$ i Påstand (2), og da får vi at $n+4 > n+3$, som vi ønsket å bevise. \end{eks}

\begin{merknad} Hele kurset består av matematiske påstander og deres beviser, så vi ikke skal gi flere eksempler nå. De logiske prinsippene som står bak dem er stort sett så velkjente at vi ikke pleier å nevne dem. Imidlertid skal vi i dette kapittelet introdusere et logisk prinsipp som er svært viktig i matematikk, og som du sannsynligvis ikke kjenner til: «induksjon». \end{merknad} 

\begin{merknad} Å bevise at en matematisk påstand er gal betyr helt enkelt å gi et eksempel hvor det ikke er sant. For eksempel se på påstanden: dersom $n$ er et naturlig tall, er $2n > n +1$. Denne påstanden er gal: når $n=1$, er påstanden at $2 > 2$, noe som er galt. 

Imidlertid er følgende påstand sann: dersom $n$ er et naturlig tall slik at $n \geq 2$, er $2n > n+1$. Den kan bevises ved hjelp av induksjon. Dette illustrerer hvor viktig det er at en matematisk påstand uttrykkes nøyaktig. \end{merknad}

\begin{terminologi} Et eksempel som beviser at en matematisk påstand er gal kalles noen ganger et {\em moteksempel}. Det tilsvarende engelske ordet er: «counterexample». \end{terminologi}

\section{Teoremer, proposisjoner, lemmaer, og korollarer}

\begin{terminologi} Et matematisk utsagn som har blitt bevist kalles et {\em teorem}, en {\em proposisjon}, et {\em korollar}, eller et {\em lemma}. Forskjellige matematikere bruker disse betegnelsene på forskjellige måter, og noen bruker i tillegg andre betegnelser. Likevel finnes det noen hovedtrekk som går igjen.

\begin{itemize}

\item[(1)] Et lemma betegner typisk et steg mot et teorem eller en proposisjon som i seg selv ikke er spesielt viktig. Ofte kan et lemma bevises ganske lett, men ikke alltid!

\item[(2)] Et teorem eller en proposisjon er et utsagn som er betydningsfult i seg selv. Et teorem er viktigere enn en proposisjon. Personlig bruker jeg «teorem» bare for de aller viktigste utsagnene. 

\item[(3)] Et korollar betegner typisk et utsagn som er lett å dedusere fra et allerede bevist teorem, proposisjon, eller lemma.

\end{itemize}

\end{terminologi}

\section{Induksjon}

\begin{merknad} \label{MerknadSummenAvDeFoersteNNaturligeTallene} La $n$ være et naturlig tall. Se på påstanden \[ 1 + 2 + \dots + n = \frac{n(n+1)}{2}. \] Hvordan kan vi bevise at dette er sant? 

Vi kan sjekke om det er sant for gitte verdier av $n$. La for eksempel $n=1$. Siden \[ \frac{1 \cdot (1+1)}{2} = \frac{1 \cdot 2}{2} = \frac{2}{2} = 1 \] er utsagnet sant i dette tilfellet. 

La $n=2$ istedenfor. Siden $1+2=3$ og \[ \frac{2 \cdot (2+1)}{2} = \frac{2 \cdot 3}{2} = \frac{6}{2} = 3, \] er det sant at \[ 1+2 = \frac{2 \cdot (2+1)}{2}. \] Dermed er utsagnet sant i dette tilfellet også. Vi kan på en lignende måte sjekke om utsagnet er sant når $n=3$, når $n=4$, og så videre. 

Likevel kan vi ikke sjekke om proposisjonen er sann for alle naturlig tall selv om vi brukte hele livet på kun det! Derfor regnes ikke å sjekke om det er sant i for enkelte verdier av $n$ som et matematisk bevis.  

Istedenfor benytter vi en type resonnement som kalles induksjon.

\end{merknad}

\begin{terminologi} \label{TerminologiInduksjon} Anta at vi har et gitt matematisk utsagn for hvert heltall større enn eller likt et gitt heltall $r$. Anta dessuten at vi ønsker å bevise utsagnet for hvert av disse heltallene. {\em Induksjon} sier at vi kan gjøre det på følgende måte: %
%
\begin{itemize}

\item[(1)] Sjekk om utsagnet er sant for heltallet $r$.

\item[(2)] Hvis det antas at utsagnet har blitt bevist for et gitt heltall $m$ som er større enn eller likt $r$, bevis at utsagnet er sant for heltallet $m+1$.

\end{itemize}

\end{terminologi}

\begin{merknad} \label{MerknadInduksjonIde} Id{\'e}en bak induksjon er at Steg (1) og Steg (2) gir oss en algoritme for å konstruere et bevis for utsagnet for et hvilket som helst heltall $m$ større enn eller likt $r$: %
%
\begin{itemize}

\item[(i)] Steg (1) i Terminologi \ref{TerminologiInduksjon} fastslår at vi kan bevise utsagnet når $m=r$;
 
\item[(ii)] Steg (2) i Terminologi \ref{TerminologiInduksjon} fastslår at vi da kan bevise utsagnet når $m=r+1$; 

\item[(iii)] Steg (2) i Terminologi \ref{TerminologiInduksjon} fastslår at vi {\em da} kan bevise utsagnet når $m=r+2$; 

\item[(iv)] Steg (2) i Terminologi \ref{TerminologiInduksjon} fastslår at vi {\em da} kan bevise utsagnet når $m=r+3$; 

\item[(v)] Slik fortsetter vi til vi når heltallet vi er interessert i.

\end{itemize} 

\end{merknad}

\begin{merknad} Det er svært viktig å fremstille et bevis ved induksjon på en klar måte: % 
%
\begin{itemize}

\item[(1)] Skriv tydelig at vi sjekker utsagnet for et gitt heltall $r$, for å gjennomføre Steg (1) i Terminologi \ref{TerminologiInduksjon}.

\item[(2)] Skriv tydelig at vi antar at utsagnet har blitt bevist for et gitt heltall $m$ større enn $r$. Skriv så et bevis for utsagnet for heltallet $m+1$, og redegjør for hvor du benytter antagelsen at utsagnet stemmer for heltallet $m$. Dermed har Steg (2) i Terminologi \ref{TerminologiInduksjon} blitt fullført.

\item[(3)] Avslutt fremstillingen ved å nevne at utsagnet stemmer for alle heltall større enn $r$ ved induksjon. Det er også greit å begynne med å skrive at utsagnet skal bevises ved induksjon. Det viktigste er å nevne dette et eller annet sted.

\end{itemize} %
%
Vi skal se på mange bevis ved induksjon i løpet av dette kurset, og du kommer sikkert til å bli fortrolig med det. La oss begynne med en gang ved å uttrykke formelt påstanden som vi tok for oss i Merknad \ref{MerknadSummenAvDeFoersteNNaturligeTallene}, og å fremstille et bevis for det ved induksjon.

\end{merknad}

\begin{prpn} \label{ProposisjonSummenAvDeFoersteNNaturligeTallene} La $n$ være et naturlig tall. Da er \[ 1 + 2 + \dots + n = \frac{n(n+1)}{2}. \] \end{prpn}

\begin{proof} Først sjekker vi om proposisjonen er sann når $n=1$. Dette gjorde vi i Merknad \ref{MerknadSummenAvDeFoersteNNaturligeTallene}. 

Anta nå at proposisjonen har blitt bevist for et gitt naturlig tall $m$ større enn eller likt $1$. Således har det blitt bevist at \[ 1 + 2 + \dots + m = \frac{m(m+1)}{2}. \] Da er \begin{align*} 1 + 2 + \dots + m + (m+1) &= \frac{m(m+1)}{2} + (m+1) \\ &= \frac{m(m+1) + 2(m+1)}{2} \\ &= \frac{(m+2)(m+1)}{2} \\ &= \frac{(m+1)(m+2)}{2}. \end{align*} Dermed er proposisjonen sann for det naturlige tallet $m+1$. 

Ved induksjon konkluderer vi at proposisjonen er sann for alle naturlige tall. 
\end{proof}

\begin{eks} Når $n=2$, fastslår Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene} at \[ 1 + 2 = \frac{2 \cdot 3}{2} = \frac{6}{2} = 3. \]  \end{eks}

\begin{eks} Når $n=3$, fastslår Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene} at \[ 1 + 2 + 3 = \frac{3 \cdot 4}{2} = \frac{12}{2} = 6. \] \end{eks}

\begin{eks} Når $n=57$, fastslår Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene} at \[ 1 + 2 + \dots + 57 = \frac{57 \cdot 58}{2} = \frac{3306}{2} = 1653. \] \end{eks}

\begin{eks} Når $n=100$, fastslår Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene} at \[ 1 + 2 + \dots + 100 = \frac{100 \cdot 101}{2} = \frac{10100}{2} = 5050. \] \end{eks}

\begin{merknad} \label{MerknadViktigsteDelenBevisetSummenAvDeFoersteNNaturligeTallene} Den viktigste delen av beviset for Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene} er ligningen \[ 1 + 2 + \dots + m + (m+1) = \frac{m(m+1)}{2} + (m+1). \] Det er her vi benytter antakelsen at \[ 1 + 2 + \dots + m = \frac{m(m+1)}{2}. \] De andre linjene er bare algebraiske manipulasjoner. \end{merknad}

\begin{merknad} \label{MerknadAlgoritmenSummenAvDeFoersteNNaturligeTallene} La oss se hvordan algoritmen i Merknad \ref{MerknadInduksjonIde} ser ut for Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene}. Vi begynner med å sjekke om \[ 1 = \frac{1 \cdot 2}{2}. \] 

Så argumenterer vi som i beviset for Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene}, ved å erstatte $m$ med $1$: %
%
\begin{align*} %
%
1 + 2 &= \frac{1 \cdot 2}{2} + 2 \\ 
      &= \frac{1 \cdot 2 + 2 \cdot 2}{2} \\ 
      &= \frac{(1+2) \cdot 2}{2} \\ 
      &= \frac{2 \cdot (1+2)}{2} \\ 
      &= \frac{2 \cdot 3}{2}.
\end{align*} %
%
Dermed er \[ 1+2 = \frac{2 \cdot 3}{2}. \] Således har vi bevist at proposisjonen er sann når $n=2$.

Så argumenterer vi som i beviset for Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene}, ved å erstatte $m$ med $2$: %
%
\begin{align*} %
%
1 + 2 + 3 &= \frac{2 \cdot 3}{2} + 3 \\ 
          &= \frac{2 \cdot 3 + 2 \cdot 3}{2} \\ 
          &= \frac{(2+2) \cdot 3}{2} \\  
          &= \frac{3 \cdot (2+2)}{2} \\ 
          &= \frac{3 \cdot 4}{2}.
\end{align*} 

Dermed er \[ 1+2+3 = \frac{3 \cdot 4}{2}. \] Således har vi bevist at proposisjonen er sann når $n=3$.

Slik fortsetter vi til vi når heltallet vi er interessert i.
\end{merknad}

\begin{merknad} \label{MerknadAlleBevisLikeVerdifulle} Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene} kan bevises på andre måter. Matematiske utsagn generelt kan typisk bevises på flere måter, og alle bevisene er like verdifulle. Ofte gir hvert bevis ny innsikt. 

Likevel skal vi ikke her se på andre bevis for Proposisjon \ref{ProposisjonSummenAvDeFoersteNNaturligeTallene}. Istedenfor skal vi øve oss litt mer på induksjon.
\end{merknad}
%forelesning01
