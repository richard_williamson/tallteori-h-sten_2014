%forelesning03
\begin{merknad} For å bevise en proposisjon om heltall som involverer to eller flere variabler, er det typisk mye lettere å benytte induksjon på en av variablene enn induksjon på noen av de andre. Det er for eksempel ikke lett å bevise Proposisjon \ref{ProposisjonInduksjonToVariabler} ved induksjon på $k$, altså med tilnærmingsmetoden beskrevet i Merknad \ref{MerknadIPrinisippetKanBytteOmRolleneInduksjonToVariabler}.  \end{merknad}

\section{Fakultet}

\begin{defn} \label{DefinisjonFakultet} La $n$ være et naturlig tall. Da er {\em $n$ fakultet} produktet \[ 1 \times 2 \times \dots \times (n-1) \times n. \] I tillegg definerer vi {\em $0$ fakultet} til å være $1$. \end{defn}

\begin{notn} La $n$ være et heltall slik at $n \geq 0$. Vi betegner $n$ fakultet som «$n!$». \end{notn}

\begin{eks} Vi har: $1! = 1$. \end{eks}

\begin{eks} Siden $1 \times 2 =2$, er $2!=2$. \end{eks}

\begin{eks} Siden $1 \times 2 \times 3 = 6$, er $3! = 6$. \end{eks}

\begin{eks} Siden $1 \times 2 \times 3 \times 4 = 24$, er $4! = 24$. \end{eks}

\section{Binomialkoeffisienter og binomialteoremet}

\begin{merknad} Fra skolen kjenner du til ligningen \[ (x+y)^{2} = x^{2} +2xy + y^{2}. \] Nå skal vi se på en tilsvarende ligning for $(x+y)^{n}$, hvor $n$ er et hvilket som helst naturlig tall. Først må vi gjøre noen forberedelser. \end{merknad}

\begin{defn} \label{DefinisjonBinomialkoeffisient} La $n$ være et naturlig tall, og la $k$ være et heltall slik at $0 \leq k \leq n$. Da er {\em binomialkoeffisienten av $n$ og $k$} brøken \[ \frac{n!}{k! \cdot (n-k)!}. \] \end{defn}

\begin{notn} Vi betegner binomialkoeffisienten av $n$ og $k$ som \[ \binom{n}{k}. \] \end{notn}

\begin{merknad} Symbolet $\binom{n}{k}$ leses (temmelig ugrammatisk!) som «$n$ velg $k$». Dette kommer av at det kan bevises at $\binom{n}{k}$ er antall muligheter for å velge ut $k$ ting fra $n$ ting. På grunn av denne tolkningen blir binomialkoeffisientene brukt mye i et område innen matematikken som kalles {\em kombinatorikk}. \end{merknad}

\begin{eks} \label{Eksempel4Velg2} Vi har: \begin{align*} \binom{4}{2} &= \frac{4!}{2! \cdot (4-2)!} \\ &= \frac{4!}{2! \cdot 2!} \\ &= \frac{24}{2 \cdot 2} \\ &= \frac{24}{4} \\  &= 6. \end{align*} \end{eks}

\begin{eks} \label{Eksempel5Velg3} Vi har: \begin{align*} \binom{5}{3} &= \frac{5!}{3! \cdot (5-3)!} \\ &= \frac{5!}{3! \cdot 2!} \\ &= \frac{120}{6 \cdot 2} \\ &= \frac{120}{12} \\ &= 10. \end{align*} \end{eks}

\begin{merknad} Bevisene av de følgende proposisjonene er enkle utregniner, og induksjon behøves ikke. \end{merknad}

\begin{prpn} \label{ProposisjonBinomialkoeffisientAvNOgNull} La $n$ være et naturlig tall. Da er $\binom{n}{0} = 1$. \end{prpn}

\begin{proof} Vi regner som følger: \begin{align*} \binom{n}{0} &= \frac{n!}{0! \cdot (n-0)!} \\ &= \frac{n!}{0! \cdot n!} \\ &= \frac{n!}{1 \cdot n!} \\ &= \frac{n!}{n!} \\ &= 1. \end{align*} %
%
\end{proof}

\begin{prpn} \label{ProposisjonBinomialkoeffisientAvNOgEn}  La $n$ være et naturlig tall. Da er $\binom{n}{1} = n$. \end{prpn}

\begin{proof} Vi regner som følger: \begin{align*} \binom{n}{1} &= \frac{n!}{1! \cdot (n-1)!} \\ &= \frac{n!}{1 \cdot (n-1)!} \\ &= \frac{n!}{(n-1)!} \\ &= \frac{1 \times 2 \times \dots \times (n-1) \times n}{1 \times 2 \times \dots \times (n-2) \times (n-1)} \\ &= n. \end{align*} \end{proof}

\begin{prpn} \label{ProposisjonBinomialkoeffisientSymmetri} La $n$ være et naturlig tall, og la $k$ være et heltall slik at $0 \leq k \leq n$. Da er $\binom{n}{k} = \binom{n}{n-k}$. \end{prpn}

\begin{proof} Vi regner som følger: \begin{align*} \binom{n}{k} &= \frac{n!}{k! \cdot (n-k)!} \\ &= \frac{n!}{(n-k)! \cdot k!} \\ &= \frac{n!}{(n-k)! \cdot \big( n - (n-k) \big)! } \\ &= \binom{n}{n-k}. \end{align*} \end{proof} 

\begin{kor} \label{KorollarBinomialkoeffisientAvNOgN} La $n$ være et naturlig tall. Da er $\binom{n}{n} = 1$. \end{kor}

\begin{proof} På grunn av Proposisjon \ref{ProposisjonBinomialkoeffisientSymmetri} er $\binom{n}{n} = \binom{n}{0}$. På grunn av Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{n}{0}=1$. Således konkluderer vi at $\binom{n}{n} = 1$.  \end{proof}

\begin{kor} \label{KorollarBinomialkoeffisientAvNOgNMinusEn} La $n$ være et naturlig tall. Da er $\binom{n}{n-1} = n$. \end{kor}

\begin{proof} Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientSymmetri} er $\binom{n}{n-1} = \binom{n}{1}$. Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgEn}, er $\binom{n}{1}=n$. Således konkluderer vi at $\binom{n}{n-1} = n$.  \end{proof}

\begin{eks} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{2}{0}=1$.

\item[(2)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgEn} er $\binom{2}{1}=2$. 

\item[(3)] Ut ifra Korollar \ref{KorollarBinomialkoeffisientAvNOgN} er $\binom{2}{2}=1$.

\end{itemize} %
%
Dermed har vi regnet ut $\binom{2}{k}$ for alle mulige verdier av $k$. \end{eks}

\begin{eks} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{3}{0}=1$. 

\item[(2)] Ut ifra Korollar \ref{KorollarBinomialkoeffisientAvNOgN} er $\binom{3}{3}=1$. 

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgEn} er $\binom{3}{1}=3$.

\item[(4)] Fra (3) og Korollar \ref{KorollarBinomialkoeffisientAvNOgNMinusEn}, følger det at $\binom{3}{2}=3$. 

\end{itemize} %
%
Dermed har vi regnet ut $\binom{3}{k}$ for alle mulige verdier av $k$. \end{eks}

\begin{eks} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{4}{0}=1$.

\item[(2)] Ut ifra Korollar \ref{KorollarBinomialkoeffisientAvNOgN} er $\binom{4}{4}=1$.

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgEn} er $\binom{4}{1}=4$. 

\item[(4)] Fra (3) og Korollar \ref{KorollarBinomialkoeffisientAvNOgNMinusEn}, følger det at $\binom{4}{3}=4$. 

\item[(5)] Fra Eksempel \ref{Eksempel4Velg2} har vi: $\binom{4}{2} = 6$. 

\end{itemize} %
%
Dermed har vi regnet ut $\binom{4}{k}$ for alle mulige verdier av $k$. \end{eks}

\begin{eks}  Vi gjør følgende observasjoner. %
%
\begin{itemize} 

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{5}{0}=1$.

\item[(2)] Ut ifra Korollar \ref{KorollarBinomialkoeffisientAvNOgN} er $\binom{5}{5}=1$. 

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgEn} er $\binom{5}{1}=5$.

\item[(4)] Fra (3) og Korollar \ref{KorollarBinomialkoeffisientAvNOgNMinusEn}, følger det at $\binom{5}{4}=5$. 

\item[(5)] Fra Eksempel \ref{Eksempel5Velg3} har vi: $\binom{5}{3} = 10$.

\item[(6)] Fra (5) og Proposisjon \ref{ProposisjonBinomialkoeffisientSymmetri}, følger det at $\binom{5}{2}=10$. 

\end{itemize} %
%
Dermed har vi regnet ut $\binom{5}{k}$ for alle mulige verdier av $k$. \end{eks}

\begin{merknad} I alle eksemplene vi har tatt for oss så langt, var $\binom{n}{k}$ er et naturlig tall. Vi skal snart bevise at dette er tilfelle for hvilke som helst $n$ og $k$. \end{merknad}

\begin{prpn} \label{ProposisjonBinomialkoeffisienterPascal} La $n$ og $k$ være naturlige tall. Da er \[ \binom{n}{k} + \binom{n}{k-1} = \binom{n+1}{k}. \] \end{prpn}

\begin{proof} Vi gjør følgende observasjoner. 

\begin{itemize}

\item[(1)] Ut ifra definisjonen av $\binom{n}{k}$ og $\binom{n}{k-1}$ i Definisjon \ref{DefinisjonBinomialkoeffisient}, er \[ \binom{n}{k} + \binom{n}{k-1} = \frac{n!}{k! \cdot (n-k)!} + \frac{n!}{(k-1)! \cdot (n-k+1)!}. \]

\item[(2)] Siden $k! = (k-1)! \cdot k$ og $(n-k+1)! = (n-k)! \cdot (n-k+1)$, er

\begin{align*} %
%
\frac{n!}{k! \cdot (n-k)!} + \frac{n!}{(k-1)! \cdot (n-k+1)!} &= \frac{(n-k+1) \cdot n! + k \cdot n!}{k! \cdot (n-k+1)!} \\
                                                              &= \frac{ n! \cdot \left( n-k+1 +k \right)}{k! \cdot (n+1-k)!} \\
                                                              &= \frac{ n! \cdot (n+1)}{k! \cdot (n+1-k)!}. \end{align*} %
%

\item[(3)] Siden $(n+1)! = n! \cdot (n+1)$, er \[ \frac{ n! \cdot (n+1)}{k! \cdot (n+1-k)!} = \frac{(n+1)!}{k! \cdot (n+1-k)!}. \] 

\item[(4)] Ut ifra definisjonen av $\binom{n+1}{k}$ i Definisjon \ref{DefinisjonBinomialkoeffisient}, er \[ \binom{n+1}{k} = \frac{(n+1)!}{k! \cdot (n+1-k)!}. \]

\end{itemize} %
% 
Fra (1) -- (4) konkluderer vi at \[ \binom{n}{k} + \binom{n}{k-1} = \binom{n+1}{k}. \] 
\end{proof}

\begin{eks} Når $n=3$ og $k=1$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{3}{1} + \binom{3}{0} = \binom{4}{1}, \] altså at \[ 3 + 1 = 4. \] \end{eks}

\begin{eks} Når $n=3$ og $k=2$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{3}{2} + \binom{3}{1} = \binom{4}{2}, \] altså at \[ 3 + 3 = 6. \] \end{eks}

\begin{eks} Når $n=3$ og $k=3$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{3}{3} + \binom{3}{2} = \binom{4}{3}, \] altså at \[ 1 + 3 = 4. \] \end{eks}

\begin{eks} Når $n=5$ og $k=1$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{5}{1} + \binom{5}{0} = \binom{6}{1}, \] altså at \[ 5 + 1 = \binom{6}{1}. \] Vi deduserer at $\binom{6}{1} = 6$.  \end{eks}

\begin{eks} Når $n=5$ og $k=2$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{5}{2} + \binom{5}{1} = \binom{6}{2}, \] altså at \[ 10 + 5 = \binom{6}{2}. \] Vi deduserer at $\binom{6}{2} = 15$.  \end{eks}

\begin{eks} Når $n=5$ og $k=3$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{5}{3} + \binom{5}{2} = \binom{6}{3}, \] altså at \[ 10 + 10 = \binom{6}{3}. \] Vi deduserer at $\binom{6}{3} = 20$.  \end{eks}

\begin{eks} Når $n=5$ og $k=4$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{5}{4} + \binom{5}{3} = \binom{6}{4}, \] altså at \[ 5 + 10 = \binom{6}{4}. \] Vi deduserer at $\binom{6}{4} = 15$.  \end{eks}

\begin{eks} Når $n=5$ og $k=5$, fastslår Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} at \[ \binom{5}{5} + \binom{5}{4} = \binom{6}{5}, \] altså at \[ 1 + 5 = \binom{6}{5}. \] Vi deduserer at $\binom{6}{5} = 6$.  \end{eks}

\begin{merknad} \label{MerknadPascalsTrekant} La oss sette opp binomialkoeffisientene på følgende måte. Det $k$-te tallet fra venstre, ved å telle $k$ fra $0$ til $n$, i den $n$-te raden fra toppen, ved å telle $n$ fra $1$,  er binomialkoeffisienten $\binom{n}{k}$. For eksempel er det andre tallet fra venstre (ved å telle fra $0$) i den fjerde raden fra toppen $6$, som er binomialkoeffisienten $\binom{4}{2}$. %
%
\[
\begin{array}{*{20}c}
  &   &   &   &   & 1  &   & 1  &   &   &   &   \\
  &   &   &   & 1 &    & 2 &    & 1 &   &   &   \\
  &   &   & 1 &   & 3  &   & 3  &   & 1 &   &   \\
  &   & 1 &   & 4 &    & 6 &    & 4 &   & 1 &   \\
  & 1 &   & 5 &   & 10 &   & 10 &   & 5 &   & 1 \\
\end{array}
\] %
%
Proposisjonen \ref{ProposisjonBinomialkoeffisienterPascal} sier at når vi legger sammen to tall i en rad, får vi tallet mellom dem i den neste raden. For eksempel når vi legger sammen tallene $4$ og $6$ i den fjerde raden, får vi $10$, som står mellom $4$ og $6$ i den femte raden. 
\end{merknad}

\begin{terminologi} Oppsettet av tallene i Merknad \ref{MerknadPascalsTrekant} kalles for {\em Pascals trekant}. \end{terminologi}

\begin{prpn} \label{ProposisjonBinomialkoeffisienteneErNaturligeTall} La $n$ være et naturlig tall, og la $k$ være et heltall slik at $0 \leq k \leq n$. Da er $\binom{n}{k}$ et naturlig tall.  \end{prpn}

\begin{proof} Først sjekker vi om proposisjonen er sann når $n=1$. I dette tilfellet er utsagnet at $\binom{1}{k}$ er et naturlig tall for hvert heltall $k$ slik at $0 \leq k \leq 1$, altså når $k=0$ og når $k=1$. Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er det sant at $\binom{1}{0}$ er et naturlig tall, og ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgEn} er det sant at $\binom{1}{1}$ er et naturlig tall.

Anta nå at proposisjonen har blitt bevist når $n$ er et gitt naturlig tall $m$. Således har det blitt bevist at $\binom{m}{k}$ er et naturlig tall for alle heltallene $k$ slik at $0 \leq k \leq m$. Vi gjør følgende observasjoner.

\begin{itemize}

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{m+1}{0}$ et naturlig tall.

\item[(2)] La $k$ være et naturlig tall. Fra antakelsen at $\binom{m}{k}$ er et naturlig tall for alle heltallene $k$ slik at $0 \leq k \leq m$, er $\binom{m}{k}$ og $\binom{m}{k-1}$ naturlige tall. Derfor er \[ \binom{m}{k} + \binom{m}{k-1} \] et naturlig tall. Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} vet vi dessuten at \[ \binom{m+1}{k} = \binom{m}{k} + \binom{m}{k-1}. \] Vi deduserer at $\binom{m+1}{k}$ er et naturlig tall.

\item[(3)] Ut ifra Korollar \ref{KorollarBinomialkoeffisientAvNOgN} er $\binom{m+1}{m+1}$ et naturlig tall.    
\end{itemize} %
%
Dermed er $\binom{m+1}{k}$ et naturlig tall for alle naturlige tall $k$ slik at $0 \leq k \leq m+1$. Således er proposisjonen sann når $n$ er det naturlige tallet $m+1$.

Ved induksjon konkluderer vi at proposisjonen er sann når $n$ er et hvilket som helst naturlig tall.

\end{proof}

\begin{prpn} \label{ProposisjonBinomialteoremet} La $x$ og $y$ være tall. La $n$ være et heltall slik at $n \geq 0$. Da er \[ (x + y)^{n} = \sum_{i=0}^{n} \binom{n}{i} x^{n-i}y^{i}. \] \end{prpn}

\begin{proof} Først sjekker vi om proposisjonen er sann når $n=0$. I dette tilfellet er utsagnet at \[ (x+y)^{0} = \sum_{i=0}^{0} \binom{0}{i} x^{0-i}y^{i}. \] Siden $(x+y)^{0} = 1$ og \[ \sum_{i=0}^{0} \binom{0}{i} x^{0-i}y^{i} = \binom{0}{0} x^{0-0}y^{0} = 1 \cdot x^{0}y^{0} = 1, \] er dette sant.

Anta nå at proposisjonen har blitt bevist når $n$ er et gitt naturlig tall $m$. Således har det blitt bevist at \[ (x + y)^{m} = \sum_{i=0}^{m} \binom{m}{i} a^{m-i}b^{i}. \] Vi gjør følgende observasjoner.

\begin{itemize}

\item[(1)] Vi har: %
%
\begin{align*} %
%
(x+y)^{m+1} &= (x+y)^{m} \cdot (x+y) \\ 
            &= \left( \sum_{i=0}^{m} \binom{m}{i} x^{m-i}y^{i} \right) \cdot (x + y) \\ 
            &= \left( \sum_{i=0}^{m} \binom{m}{i} x^{m-i}y^{i} \right) \cdot x + \left( \sum_{i=0}^{m} \binom{m}{i} x^{m-i}y^{i} \right) \cdot y \\ 
            &= \left( \sum_{i=0}^{m} \binom{m}{i} x^{m+1-i}y^{i} \right) + \left( \sum_{i=0}^{m} \binom{m}{i} x^{m-i}y^{i+1} \right)
\end{align*}

\item[(2)] Vi har: \[ \sum_{i=0}^{m} \binom{m}{i} x^{m+1-i}y^{i} = \binom{m}{0}x^{m+1-0}y^{0} + \left( \sum_{i=1}^{m} \binom{m}{i} x^{m+1-i}y^{i} \right). \] 

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{m}{0} = 1$. Derfor er \[ \binom{m}{0}x^{m+1-0}y^{0} + \left( \sum_{i=1}^{m} \binom{m}{i} x^{m+1-i}y^{i} \right) = x^{m+1} + \left( \sum_{i=1}^{m} \binom{m}{i} x^{m+1-i}y^{i} \right). \] 

\item[(4)] Vi har: %
%
\begin{align*} %
%           
\sum_{i=0}^{m} \binom{m}{i} x^{m-i}y^{i+1} &= \sum_{i=1}^{m+1} \binom{m}{i-1} x^{m-(i-1)}y^{(i-1)+1} \\ 
&= \sum_{i=1}^{m+1} \binom{m}{i-1} x^{m+1-i)}y^{i} \\
&= \left( \sum_{i=1}^{m} \binom{m}{i-1} x^{m+1-i)}y^{i} \right) + \binom{m}{(m+1)-1}x^{m+1-(m+1)}y^{m+1} \\
&= \left( \sum_{i=1}^{m} \binom{m}{i-1} x^{m+1-i)}y^{i} \right) + \binom{m}{m}x^{0}y^{m+1}.
\end{align*} 

\item[(5)] Ut ifra Korollar \ref{KorollarBinomialkoeffisientAvNOgN} er $\binom{m}{m} = 1$. Derfor er: %
%
\[ \left( \sum_{i=1}^{m} \binom{m}{i-1} x^{m+1-i)}y^{i} \right) + \binom{m}{m}x^{0}y^{m+1} = \left( \sum_{i=1}^{m} \binom{m}{i-1} x^{m+1-i)}y^{i} \right) + y^{m+1}. \] 

\item[(6)] Vi har: %
%
\begin{align*} %
%       
& x^{m+1} + \left( \sum_{i=1}^{m} \binom{m}{i} x^{m+1-i}y^{i} \right) + \left( \sum_{i=1}^{m} \binom{m}{i-1} x^{m+1-i)}y^{i} \right) + y^{m+1} \\
&= x^{m+1} + \left( \sum_{i=1}^{m} \left( \binom{m}{i} + \binom{m}{i-1} \right) x^{m+1-y}y^{i} \right) + y^{m+1} 
\end{align*}

\item[(7)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisienterPascal} er \[ \binom{m+1}{i} = \binom{m}{i} + \binom{m}{i-1} \] for alle heltall $i$ slik at $1 \leq i \leq m$. Vi deduserer at %
%
\begin{align*} %
%
& x^{m+1} + \left( \sum_{i=1}^{m} \left( \binom{m}{i} + \binom{m}{i-1} \right) x^{m+1-y}y^{i} \right) + y^{m+1} \\
&= x^{m+1} + \left( \sum_{i=1}^{m} \left( \binom{m}{i} + \binom{m}{i-1} \right) x^{m+1-y}y^{i} \right) y^{m+1} \\ 
&= x^{m+1} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + y^{m+1}.
\end{align*} 

\end{itemize} %
%
Vi deduserer fra (1) -- (7) at \[ (x+y)^{m+1} =  x^{m+1} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + y^{m+1}. \] Nå gjør vi følgende observasjoner.

\begin{itemize}

\item[(1)] Vi har: %
%
\begin{align*} %
%
& \sum_{i=0}^{m+1} \binom{m+1}{i} x^{m+1-i}y^{i} \\
&= \binom{m+1}{0} x^{m+1-0} y^{0} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + \binom{m+1}{m+1} x^{m+1-(m+1)}y^{m+1} \\ 
&= \binom{m+1}{0} x^{m+1} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + \binom{m+1}{m+1} y^{m+1}   
\end{align*}

\item[(2)] Ut ifra Proposisjon \ref{ProposisjonBinomialkoeffisientAvNOgNull} er $\binom{m+1}{0} = 1$. Ut ifra Korollar \ref{KorollarBinomialkoeffisientAvNOgN} er $\binom{m+1}{m+1} = 1$. Derfor er %
%
\begin{align*} %
%
& \binom{m+1}{0} x^{m+1} + \left( \sum_{i=1}^{m}  \binom{m+1}{i} x^{m+1-i}y^{i} \right) + \binom{m+1}{m+1} y^{m+1} \\
&= x^{m+1} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + y^{m+1}. 
\end{align*}

\end{itemize} %
%
Vi deduserer fra (1) -- (2) at \[ \sum_{i=0}^{m+1} \binom{m+1}{i} x^{m+1-i}y^{i} = x^{m+1} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + y^{m+1}. \] %
%
For å oppsummere beviset så langt, har vi fastslått at \[ (x+y)^{m+1} =  x^{m+1} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + y^{m+1} \] og at \[ x^{m+1} + \left( \sum_{i=1}^{m} \binom{m+1}{i} x^{m+1-i}y^{i} \right) + y^{m+1} = \sum_{i=0}^{m+1} \binom{m+1}{i} x^{m+1-i}y^{i}. \] %
%
Vi deduserer at \[ (x+y)^{m+1} = \sum_{i=0}^{m+1}   \binom{m+1}{i} x^{m+1-i}y^{i}. \] %
%
Dermed er proposisjonen sann når $n$ er det naturlige tallet $m+1$.

Ved induksjon konkluderer vi at proposisjonen er sann når $n$ er et hvilket som helst naturlig tall. 

\end{proof}

\begin{eks} Når $n=2$, fastslår Proposisjon \ref{ProposisjonBinomialteoremet} at \[ (x+y)^{2} = x^{2} + 2xy + y^{2}, \] som forventet. \end{eks}

\begin{eks} Når $n=3$, fastslår Proposisjon \ref{ProposisjonBinomialteoremet} at \[ (x+y)^{3} = x^{3} + 3x^{2}y + 3xy^{2} + y^{3}. \] \end{eks}

\begin{eks} Når $n=4$, fastslår Proposisjon \ref{ProposisjonBinomialteoremet} at \[ (x+y)^{4} = x^{4} + 4x^{3}y + 6x^{2}y^{2} + 4xy^{3} + y^{4}. \] \end{eks}

\begin{merknad} Proposisjon \ref{ProposisjonBinomialteoremet} kalles noen ganger {\em binomialteoremet}. \end{merknad}

\begin{merknad} Den viktigste delen av beviset for Proposisjon \ref{ProposisjonBinomialteoremet} er ligningen \[ (x+y)^{m} \cdot (x+y) = \left( \sum_{i=0}^{m} \binom{m}{i} x^{m-i}y^{i} \right) \cdot (x + y). \] Det er her vi benytter antakelsen at \[ (x + y)^{m} = \sum_{i=0}^{m} \binom{m}{i} a^{m-i}b^{i}. \] \end{merknad} 

\begin{merknad} \label{MerknadBinomialteoremetUtenSummetegn} Til å begynne med kan manipulasjoner med summetegn som i beviset for Proposisjon \ref{ProposisjonBinomialteoremet} se litt forvirrende ut. I så fall skriv alle summene uten å bruke summetegnet. Skriv for eksempel \[ \sum_{i=0}^{n} \binom{n}{i} x^{n-i}y^{i} \] som \[ \binom{n}{0}x^{n}y^{0} + \binom{n}{1}x^{n-1}y^{1} + \dots + \binom{n}{n-1}x^{1}y^{n-1} + \binom{n}{n}x^{0}y^{n}. \] \end{merknad}
%forelesning03
