%forelesning17
\section{Orden modulo et primtall}

\begin{defn} \label{DefinisjonOrden} La $p$ være et primtall. La $x$ være et heltall slik at det ikke er sant at \[ x \equiv 0 \pmod{p}. \] Et naturlig tall $t$ er {\em ordenen} til $a$ modulo $p$ dersom $t$ er det minste naturlige tallet slik at: %
%
\begin{itemize}

\item[(1)] $x^{t} \equiv 1 \pmod{p}$;

\item[(2)] $0 \leq t < p$.

\end{itemize} 

\end{defn} 

\begin{merknad} La $x$ være et heltall slik at det ikke er sant at \[ x \equiv 0 \pmod{p}. \] Ut ifra Korollar \ref{KorollarFermatsLilleTeoremPMinus1} er \[ x^{p-1} \equiv 1 \pmod{p}. \] Derfor har $x$ en orden, og denne ordenen er mindre enn eller likt $p-1$.  \end{merknad} 

\begin{merknad} For å finne ordenen til et heltall $x$ modulo et primtall $p$, kan vi gå gjennom heltallene $x$, $x^{2}$, $x^{3}$, $\ldots$, $x^{p-1}$. Den første potensen $i$ slik at \[ x^{i} \equiv 1 \pmod{p} \] er ordenen til $x$ modulo $p$.  \end{merknad} 

\begin{notn} La $p$ være et primtall. La $x$ være et heltall slik at det ikke er sant at $x \equiv 0 \pmod{p}$. Vi betegner ordenen til $x$ modulo $p$ som $\orden{x,p}$.  \end{notn}

\begin{eks} Siden $1^{1} = 1$, er ordenen til $1$ lik $1$ for et hvilket som helst primtall $p$. \end{eks}

\begin{eks} \label{EksempelOrdeneneModulo3} For å finne ordenen til $2$ modulo $3$, gjør vi følgende. Kongruensen i den andre raden er modulo $3$.

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $2^{i}$ \\
\midrule
$1$ & $2$  \\
$2$ & $4 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $2$ modulo $3$ lik $2$.

Således har vi følgende ordener modulo $3$.

\begin{center}
\begin{tabular}{ll}
\toprule
$x$ & Ordenen til $x$ modulo $3$ \\
\midrule
$1$ & $1$ \\
$2$ & $2$ \\
\bottomrule
\end{tabular}
\end{center} 
\end{eks}

\begin{eks} \label{EksempelOrdeneneModulo5} Alle kongruenser i dette eksempelet er modulo $5$. For å finne ordenen til $2$ modulo $5$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $2^{i}$ \\
\midrule
$1$ & $2$  \\
$2$ & $4$ \\
$3$ & $8 \equiv 3$ \\
$4$ & $2^{4} = 2^{2} \cdot 2^{2} \equiv 4 \cdot 4 = 16 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $2$ modulo $5$ lik $4$.

For å finne ordenen til $3$ modulo $5$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $3^{i}$ \\
\midrule
$1$ & $3$  \\
$2$ & $9 \equiv 4$ \\
$3$ & $3^{3} = 3^{2} \cdot 3^{1} \equiv 4 \cdot 3 = 12 \equiv 2$ \\
$4$ & $3^{4} = 3^{3} \cdot 3^{1} \equiv 2 \cdot 3 = 6 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $3$ modulo $5$ lik $4$.

For å finne ordenen til $4$ modulo $5$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $4^{i}$ \\
\midrule
$1$ & $4$  \\
$2$ & $16 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $4$ modulo $5$ lik $2$.

Således har vi følgende ordener modulo $5$.

\begin{center}
\begin{tabular}{ll}
\toprule
$x$ & Ordenen til $x$ modulo $5$ \\
\midrule
$1$ & $1$ \\
$2$ & $4$ \\
$3$ & $4$ \\
$4$ & $2$ \\
\bottomrule
\end{tabular}
\end{center} 

\end{eks}

\begin{merknad} Utregningene i Eksempel \ref{EksempelOrdeneneModulo5} er ikke de eneste mulige. For å vise at \[ 2^{4} \equiv 1 \pmod{5}, \] kan vi også for eksempel regne som følger: \[ 2^{4} = 2^{3} \cdot 2^{1} \equiv 3 \cdot 2 = 6 \equiv 1 \pmod{5}. \] Alternativt følger det fra Korollar \ref{KorollarFermatsLilleTeoremPMinus1}. 

Det samme gjelder i neste eksempel. 
\end{merknad} 

\begin{eks} \label{EksempelOrdeneneModulo7} Alle kongruenser i dette eksempelet er modulo $7$. For å finne ordenen til $2$ modulo $7$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $2^{i}$ \\
\midrule
$1$ & $2$  \\
$2$ & $4$ \\
$3$ & $8 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $2$ modulo $7$ lik $3$.

For å finne ordenen til $3$ modulo $7$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $3^{i}$ \\
\midrule
$1$ & $3$  \\
$2$ & $9 \equiv 2$ \\
$3$ & $3^{3} = 3^{2} \cdot 3^{1} \equiv 2 \cdot 3 = 6$ \\
$4$ & $3^{4} = 3^{2} \cdot 3^{2} \equiv 2 \cdot 2 = 4$ \\
$5$ & $3^{5} = 3^{3} \cdot 3^{2} \equiv 6 \cdot 2 = 12 \equiv 5$ \\
$6$ & $3^{6} = 3^{4} \cdot 3^{2} \equiv 4 \cdot 2 = 8 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $4$ modulo $7$ lik $6$.

For å finne ordenen til $4$ modulo $7$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $4^{i}$ \\
\midrule
$1$ & $4$  \\
$2$ & $16 \equiv 2$ \\
$3$ & $4^{3} = 4^{2} \cdot 4^{1} \equiv 2 \cdot 4 = 8 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $4$ modulo $7$ lik $3$.

For å finne ordenen til $5$ modulo $7$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $5^{i}$ \\
\midrule
$1$ & $5$  \\
$2$ & $25 \equiv 4$ \\
$3$ & $5^{3} = 5^{2} \cdot 5^{1} \equiv 4 \cdot 5 = 20 \equiv -1$ \\
$4$ & $5^{4} = 5^{3} \cdot 5^{1} \equiv (-1) \cdot 5 = -5 \equiv 2$ \\
$5$ & $5^{5} = 5^{3} \cdot 5^{2} \equiv (-1) \cdot 4 = -4 \equiv 3$ \\
$6$ & $5^{6} = 5^{3} \cdot 5^{3} \equiv (-1) \cdot (-1) = 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $5$ modulo $7$ lik $6$.

For å finne ordenen til $6$ modulo $7$, gjør vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$i$ & $6^{i}$ \\
\midrule
$1$ & $6$  \\
$2$ & $36 \equiv 1$ \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed er ordenen til $6$ modulo $7$ lik $2$.

Således har vi følgende ordener modulo $7$.

\begin{center}
\begin{tabular}{ll}
\toprule
$x$ & Ordenen til $x$ modulo $7$ \\
\midrule
$1$ & $1$ \\
$2$ & $3$ \\
$3$ & $6$ \\
$4$ & $3$ \\
$5$ & $6$ \\
$6$ & $2$ \\
\bottomrule
\end{tabular}
\end{center} 

\end{eks}

\begin{prpn} \label{ProposisjonOrdenenTilDelerEnHvilkenSomHelstPotensSomGir1ModuloP} La $p$ være et primtall. La $x$ være et heltall slik at $x$ det ikke er sant at \[ x \equiv 0 \pmod{p}. \] La $s$ være ordenen til $x$. La $t$ være et naturlig tall. Da er \[ x^{t} \equiv 1 \pmod{p} \] hvis og bare hvis $s \mid t$. \end{prpn}

\begin{proof} Anta først at $x^{t} \equiv 1 \pmod{p}$. Ut ifra Proposisjon \ref{ProposisjonDivisjonsalgoritme} finnes det naturlige tall $k$ og $r$ slik at $t = ks + r$. Da er: %
%
\begin{align*} %
%
x^{t} &= x^{ks+r} \\
      &= x^{ks}x^{r} \\
      &= \left(x^{s} \right)^{k} x^{r}.
\end{align*} %
%
Ut ifra definisjonen til $s$ er \[ x^{s} \equiv 1 \pmod{p}. \] Dermed er \[ x^{t} \equiv 1^{k} \cdot x^{r}, \] alts \[ x^{t} \equiv x^{r} \pmod{p}. \] Ut ifra antakelsen at \[ x^{t} \equiv 1 \pmod{p} \] og Proposisjon \ref{ProposisjonXKongYModNImplisererYKongXModN}, er da \[ x^{r} \equiv 1 \pmod{p}. \] Ut ifra definisjonen til $s$, er $s$ det minste naturlige tallet slik at $x^{s} \equiv 1 \pmod{p}$. Siden $0 \leq r < s$ og \[ x^{r} \equiv 1 \pmod{p}, \] følger det at $r=0$.  Dermed er $t=ks$. Vi konkluderer at $s \mid t$.

Anta istedenfor at $s \mid t$. Da finnes det et naturlig tall $k$ slik at $t=ks$. Ut ifra definisjonen til $s$, er $x^{s} \equiv 1 \pmod{p}$. Derfor er \[ \left(x^{s}\right)^{k} \equiv 1^{k} \pmod{p}, \] altså er \[ x^{sk} \equiv 1 \pmod{p}. \] Siden \[ sk=ks=t, \] konkluderer vi at \[ x^{t} \equiv 1 \pmod{p}. \]    
\end{proof} 

\begin{eks} Siden $2^{6} = 64$ og \[ 64 \equiv 1 \pmod{7}, \] fastslår Proposisjon \ref{ProposisjonOrdenenTilDelerEnHvilkenSomHelstPotensSomGir1ModuloP} at ordenen til $2$ modulo $7$ deler $6$. Ut ifra Eksempel \ref{EksempelOrdeneneModulo7} er ordenen til $2$ modulo $7$ lik $3$. Det er riktignok sant at $3 \mid 6$. \end{eks}

\begin{eks} Siden $3^{8} = 6561$ og \[ 6561 \equiv 1 \pmod{5}, \] fastslår Proposisjon \ref{ProposisjonOrdenenTilDelerEnHvilkenSomHelstPotensSomGir1ModuloP} at ordenen til $3$ modulo $4$ deler $8$. Ut ifra Eksempel \ref{EksempelOrdeneneModulo5} er ordenen til $3$ modulo $5$ lik $4$. Det er riktignok sant at $4 \mid 8$. \end{eks}

\begin{comment}

\begin{prpn} La $p$ være et primtall.  La $x$ og $y$ være heltall slik at det ikke er sant at \[ x \equiv 0 \pmod{p} \] og det ikke er sant at \[ y \equiv 0 \pmod{p}. \] Anta at $\sfd{\orden{x,p},\orden{y,p}}=1$. La $t$ være et heltall slik at \[ (xy)^{t} \equiv 1 \pmod{p}. \] Da har vi: $\orden{x,p} \mid t$. \end{prpn}

\begin{proof} Vi gjør følgende observasjoner. %
%
\bgein{itemize}

\item[(1)] Siden \[ (xy)^{t} \equiv 1 \pmod{p}, \] er \[ x^{t} y^{t} \equiv 1 \pmod{p}.\] Da følger det fra Korollar \ref{} at \[ \left( x^{t}y^{t} \right) \cdot \left( y^{-1} \right)^{t} \equiv \left( y^{-1} \right)^{t} \pmod{p}. \] 

\item[(2)] Ut ifra Proposisjon \ref{}, er det et heltall $y^{-1}$ slik at \[ y \cdot y^{-1} \equiv 1 \pmod{p}. \] Ut ifra Proposisjon \ref{}, er \[ y^{t} \cdot \left( y^{-1} \right)^{t} \equiv 1 \pmod{p}. \] Da følger det fra Korollar \ref{} at \[ x^{t} y^{t} \cdot \left( y^{-1} \right)^{t} \equiv x^{t} \pmod{p}. \] Ut ifra Proposisjon \ref{} er da \[ x^{t} \equiv x^{t} y^{t} \cdot \left( y^{-1} \right)^{t} \pmod{p}. \]  

\item[(3)] Det følger fra (1), (2), og Proposisjon \ref{}  at \[ x^{t} \equiv \left(y^{-1}\right)^{t}  \pmod{p}. \]

\item[(4)] Det følger fra Korollar \ref{} at \[ \orden{x^{t},p} \mid \orden{x,p}. \]

\item[(5)] Ut ifra Proposisjon \ref{} er \[ \orden{y^{-1},p} = \orden{y,p}. \] Det følger fra Korollar \ref{} at \[ \orden{\left(y^{-1} \right)^{t},p} \mid \orden{y^{-1},p}. \] Dermed har vi: \[ \orden{\left(y^{-1} \right)^{t},p} \mid \orden{y,p}. \]

\item[(6)] Det følger fra (3) og (5) at \[ \orden{x^{t},p} \mid \orden{y,p}. \] 

\end{itemize} %
%
Siden $\sfd{\orden{x,p},\orden{y,p}} = 1$, følger det fra (4) og (6) at $\orden{x^{t},p} = 1$. Dermed er \[ \left( x^{t} \right)^{1} \equiv 1 \pmod{p}, \] altså \[ x^{t} \equiv 1 \pmod{p}. \] Det følger fra Proposisjon \ref{} at $\orden{x,p} \mid t$.
        
 \end{proof} 

%TODO: Examples.

\begin{kor} La $p$ være et primtall.  La $x$ og $y$ være heltall slik at det ikke er sant at \[ x \equiv 0 \pmod{p} \] og det ikke er sant at \[ y \equiv 0 \pmod{p}. \] Anta at $\sfd{\orden{x,p},\orden{y,p}}=1$. La $t$ være et heltall slik at \[ (xy)^{t} \equiv 1 \pmod{p}. \] Da har vi: $\orden{y,p} \mid t$. \end{kor}

\begin{proof} Siden $(xy)^{t} = (yx)^{t}$, er \[ (yx)^{t} \equiv 1 \pmod{p}. \] Da følger det umiddelbart fra Proposisjon \ref{} at $\orden{y,p} \mid t$. \end{proof}

%TODO: Examples.

\begin{prpn} La $p$ være et primtall. La $x$ og $y$ være heltall slik at det ikke er sant at \[ x \equiv 0 \pmod{p} \] og det ikke er sant at \[ y \equiv 0 \pmod{p}. \] Anta at $\sfd{\orden{x,p},\orden{y,p}}=1$. Da er \[ \orden{xy,p} = \orden{x,p} \cdot \orden{y,p}. \]  \end{prpn}

\begin{proof} Vi gjør følgende observasjoner. %
%
\begin{itemize} 

\item[(1)] Det følger fra Proposisjon \ref{} at $\orden{x,p} \mid \orden{xy,p}$. 

\item[(2)] Det følger fra Korollar \ref{} at $\orden{y,p} \mid \orden{xy,p}$.

\end{itemize} %
%
Siden $\sfd{\orden{x,p},\orden{y,p}}=1$, følger det fra Proposisjon \ref{} at \[ \orden{x,p} \cdot \orden{y,} \mid \orden{xy,p}. \] 
 
   \end{proof}

%TODO: Examples. Not true if not relatively prime. Other examples of this for the previous couple of proposisions. 

\end{comment}

\section{Primitive røtter modulo et primtall}

\begin{defn} \label{DefinisjonPrimitivRot} La $p$ være et primtall. La $x$ være et heltall slik at det ikke er sant at \[ x \equiv 0 \pmod{p}. \] Da er $x$ en {\em primitiv rot} modulo $p$ dersom ordenen til $x$ modulo $p$ er $p-1$. \end{defn}

\begin{eks} \label{EksempelPrimitivRoetteneModulo2} Siden ordenen til $1$ er $2-1=1$, er $1$ en primitiv rot modulo $2$. \end{eks}

\begin{eks} \label{EksempelPrimitivRoetteneModulo3}  Ut ifra tabellen på slutten av Eksempel \ref{EksempelOrdeneneModulo3} har vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$x$ & Primitiv rot modulo $3$?\\
\midrule
$1$ & \cross \\
$2$ & \tick \\
\bottomrule
\end{tabular}
\end{center} 

\end{eks}

\begin{eks} \label{EksempelPrimitivRoetteneModulo5}  Ut ifra tabellen på slutten av Eksempel \ref{EksempelOrdeneneModulo5} har vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$x$ & Primitiv rot modulo $5$?\\
\midrule
$1$ & \cross \\
$2$ & \tick \\
$3$ & \tick \\
$4$ & \cross \\
\bottomrule
\end{tabular}
\end{center} 

\end{eks}

\begin{eks} \label{EksempelPrimitivRoetteneModulo7}  Ut ifra tabellen på slutten av Eksempel \ref{EksempelOrdeneneModulo7} har vi følgende. 

\begin{center}
\begin{tabular}{ll}
\toprule
$x$ & Primitiv rot modulo $7$?\\
\midrule
$1$ & \cross \\
$2$ & \cross \\
$3$ & \tick \\
$4$ & \cross \\
$5$ & \tick \\
$6$ & \cross \\
\bottomrule
\end{tabular}
\end{center} 

\end{eks}

\begin{prpn} \label{ProposisjonHvertHeltallKongPrimitivRotOpphoeydINoe} La $p$ være et primtall. La $x$ være en primitiv rot modulo $p$. La $a$ være et heltall. Da finnes det et heltall $r$ slik at $0 \leq r < p$ og \[ x^{r} \equiv a \pmod{p}. \] \end{prpn} 

\begin{proof} Kommer snart!  \end{proof} 

\begin{merknad} Proposisjon \ref{ProposisjonHvertHeltallKongPrimitivRotOpphoeydINoe} er grunnen for at primitive røtter er viktige. Å kunne uttrykke et hvilket som helst heltall modulo $p$ som en potens av ett heltall er noe er spesielt med aritmetikk modulo $p$, og svært viktig fra et teoretisk synspunkt. Det er langt fra tilfellet at det finnes et heltall $x$ slik at hvert naturlig tall er {\em likt} $x$ opphøyd i noe. Når $x = 2$, får vi for eksempel heltallene $2$, $4$, $8$, $16$, $\ldots$, men får vi ikke de negative heltallene, og heller ikke de naturlige tallene $1$, $3$, $5$, $6$, $7$, $9$, $\ldots$.  \end{merknad}
%forelesning17
