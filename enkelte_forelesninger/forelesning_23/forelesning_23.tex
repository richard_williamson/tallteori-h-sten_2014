%forelesning23
\section{Totienten}

\begin{merknad} La $p$ være et primtall. Fermats lille teorem, altså Korollar \ref{KorollarFermatsLilleTeoremPMinus1}, fastslår at, dersom det ikke er sant at \[ x \equiv 0 \pmod{p}, \] er \[ x^{p-1} \equiv 1 \pmod{p}. \] Vi har sett at dette resultatet er svært nyttig. 

Hva om vi erstatter $p$ med et hvilket som helst naturlig tall? Er et lignende utsagn sant? Det er visselig ikke nødvendigvis sant at \[ x^{n-1} \equiv 1 \pmod{n} \] når $n$ ikke er et primtall. For eksempel er \[ 27 \equiv 3 \pmod{4}, \] altså \[ 3^{4-1} \equiv 3 \pmod{4}, \] og det er ikke sant at \[ 3 \equiv 1 \pmod{4}. \]       

Likevel kan Fermats lille teorem generalises, ved å ertsatte potensen $p-1$ med noe som kalles totienten til $n$. Nå kommer vi til å se på dette resultatet, som kalles Eulers teorem, og etterpå til å utforske hvordan det benyttes i kryptografi.   \end{merknad}

\begin{defn} \label{DefinisjonTotient} La $n$ være et naturlig tall. Da er {\em totienten} til $n$ antall naturlige tall $x$ slik at $x \leq n$ og $\sfd{x,n}=1$. \end{defn} 

\begin{notn} \label{NotasjonTotient} La $n$ være et naturlig tall. Vi betegner totienten til $n$ som $\phi(n)$. \end{notn}

\begin{eks} \label{EksempelTotientenTil1} Det eneste naturlige tallet $x$ slik at $x \leq 1$ er $1$. Det er sant at $\sfd{1,1}=1$. Dermed er $\phi(1) = 1$. \end{eks} 

\begin{eks} \label{EksempelTotientenTil2}  De eneste naturlige tallene $x$ slik at $x \leq 2$ er $1$ og $2$. Det er sant at $\sfd{1,2}=1$, men $\sfd{2,2}=2$. Dermed er $1$ det eneste naturlige tallet $x$ slik at $x \leq 2$ og $\sfd{x,2}=1$. Således er $\phi(2) = 1$. \end{eks} 

\begin{eks} \label{EksempelTotientenTil3} Tabellen nedenfor viser informasjonen som behøves for å regne ut $\phi(3)$.

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,3}$ & Bidrar til $\phi(3)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $1$ & \tick \\
$3$ & $3$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed finnes det to naturlige tall $x$ slik at $x \leq 3$ og $\sfd{x,3}=1$. Således er $\phi(3) = 2$. \end{eks} 

\begin{eks} \label{EksempelTotientenTil4}  Tabellen nedenfor viser informasjonen som behøves for å regne ut $\phi(4)$.

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,4}$ & Bidrar til $\phi(4)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $2$ & \cross \\
$3$ & $1$ & \tick \\
$4$ & $4$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed finnes det to naturlige tall $x$ slik at $x \leq 4$ og $\sfd{x,4}=1$. Således er $\phi(4) = 2$. \end{eks} 

\begin{eks} \label{EksempelTotientenTil5} Tabellen nedenfor viser informasjonen som behøves for å regne ut $\phi(5)$.

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,5}$ & Bidrar til $\phi(5)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $1$ & \tick \\
$3$ & $1$ & \tick \\
$4$ & $1$ & \tick \\
$5$ & $5$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed finnes det fire naturlige tall $x$ slik at $x \leq 5$ og $\sfd{x,5}=1$. Således er $\phi(5) = 4$. \end{eks} 

\begin{eks} Tabellen nedenfor viser informasjonen som behøves for å regne ut $\phi(6)$.

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,6}$ & Bidrar til $\phi(6)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $2$ & \cross \\
$3$ & $3$ & \cross \\
$4$ & $2$ & \cross \\
$5$ & $1$ & \tick \\
$6$ & $6$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed finnes det to naturlige tall $x$ slik at $x \leq 6$ og $\sfd{x,6}=1$. Således er $\phi(6) = 2$. \end{eks} 

\begin{eks} Tabellen nedenfor viser informasjonen som behøves for å regne ut $\phi(10)$.

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,10}$ & Bidrar til $\phi(10)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $2$ & \cross \\
$3$ & $1$ & \tick \\
$4$ & $2$ & \cross \\
$5$ & $5$ & \cross \\
$6$ & $2$ & \cross \\
$7$ & $1$ & \tick \\
$8$ & $2$ & \cross \\
$9$ & $1$ & \tick \\
$10$ & $10$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed finnes det fire naturlige tall $x$ slik at $x \leq 10$ og $\sfd{x,10}=1$. Således er $\phi(10) = 4$. \end{eks} 

\begin{eks} \label{EksempelTotientenTil12} Tabellen nedenfor viser informasjonen som behøves for å regne ut $\phi(12)$.

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,12}$ & Bidrar til $\phi(12)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $2$ & \cross \\
$3$ & $3$ & \cross \\
$4$ & $4$ & \cross \\
$5$ & $1$ & \tick \\
$6$ & $3$ & \cross \\
$7$ & $1$ & \tick \\
$8$ & $4$ & \cross \\
$9$ & $3$ & \cross \\
$10$ & $2$ & \cross \\
$11$ & $1$ & \tick \\
$12$ & $12$ & \cross \\
\bottomrule
\end{tabular}
\end{center} %
%
Dermed finnes det fire naturlige tall $x$ slik at $x \leq 12$ og $\sfd{x,12}=1$. Således er $\phi(12) = 4$. \end{eks} 

\begin{prpn} \label{ProposisjonTotientenTilNLikNMinus1OmOgBareOmNPrimtall} La $n$ være et naturlig tall. Da er $\phi(n) = n-1$ om og bare om $n$ er et primtall. \end{prpn}

\begin{proof} Anta først at $n$ er et primtall. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra Korollar \ref{KorollarSFDPrimtallHeltall1OmHeltalletIkkeDeleligMedPrimtallet}, er da $\sfd{x,n} = 1$ for et hvilket som helst naturlig tall $x$ slik at $x \leq n-1$. 

\item[(2)] Vi har: $\sfd{n,n} = n$. Siden $n$ er et primtall, er $n > 1$. Dermed er $\sfd{n,n} \not= 1$. 

\end{itemize} %
%
Vi konkluderer at $\phi(n)=n-1$.

Anta istedenfor at $\phi(n) = n-1$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Vi har: $\sfd{n,n} = n$. Siden $\phi(1) = 1$, er det ikke sant at $n=1$. Derfor er $n \geq 2$. Dermed er $\sfd{n,n} \not= 1$.

\item[(2)] Det følger fra (1) at $\phi(n)$ antall naturlige tall $x$ slik at $x \leq n-1$ og $\sfd{x,n}=1$. Siden $\phi(n)=n-1$, følger det at $\sfd{x,n}=1$ for alle de naturlige tallene $x$ slik at $x \leq n-1$.

\item[(3)] La $x$ være et naturlig tall slik at $x \mid n$. Da er $\sfd{x,n}=x$.  

\end{itemize} %
%
Det følger fra (2) og (3) at, dersom $x$ er et naturlig tall slik at $x \mid n$ og $x \not= n$, er $x=1$. Derfor er $n$ et primtall.  

\end{proof} 

\begin{eks} Proposisjon \ref{ProposisjonTotientenTilNLikNMinus1OmOgBareOmNPrimtall} fastslår at $\phi(3) = 2$. Ut ifra Eksempel \ref{EksempelTotientenTil3} er dette riktignok sant. \end{eks} 

\begin{eks} Ut ifra Eksempel \ref{EksempelTotientenTil5} er  $\phi(5) = 4$. Da fastslår Proposisjon \ref{ProposisjonTotientenTilNLikNMinus1OmOgBareOmNPrimtall} at $5$ er et primtall. Dette er riktignok sant. \end{eks} 

\begin{lem} \label{LemmaNDelerPrimtallOpphoeydINFoererTilAtPrimtallDelerN} La $p$ være et primtall. La $n$ være et naturlig tall. La $y$ være et naturlig tall slik at $y \mid p^{n}$ og $y > 1$. Da har vi: $p \mid y$. \end{lem}

\begin{proof} Ut ifra Korollar \ref{KorollarHvertNaturligTallDeleligMedEtPrimtall} finnes det et primtall $q$ slik at $q \mid y$. Ut ifra Proposisjon \ref{ProposisjonMDeleligMedLOgNDeleligMedMImplisererNDeleligMedLProposisjonMDeleligMedLOgNDeleligMedMImplisererNDeleligMedL} har vi da: $q \mid p^{n}$. Det følger fra Korollar \ref{KorollarPrimtallDelerProduktPrimtallFoererTilDetErLikEttAvLeddene} at $q=p$. Siden $q \mid y$, konkluderer vi at $p \mid y$. \end{proof} 

\begin{eks} Vi har: $9 \mid 27$, altså $9 \mid 3^{3}$. Siden $3$ er et primtall, fastlår Lemma \ref{LemmaNDelerPrimtallOpphoeydINFoererTilAtPrimtallDelerN} at $3 \mid 9$. Dette er rikignok sant. \end{eks} 

\begin{eks} Vi har: $16 \mid 64$, altså $16 \mid 2^{6}$. Siden $2$ er et primtall, fastlår Lemma \ref{LemmaNDelerPrimtallOpphoeydINFoererTilAtPrimtallDelerN} at $2 \mid 16$. Dette er riktignok sant. \end{eks} 

\begin{prpn} \label{ProposisjonTotientenTilPrimtallOpphoeydINoe} La $p$ være et primtall. La $n$ være et naturlig tall. Da er $\phi\left( p^{n} \right) = p^{n} - p^{n-1}$.  \end{prpn}

\begin{proof} La $x$ være et naturlig tall slik at $x \leq p^{n}$ og $\sfd{x,p^{n}} \not= 1$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Da finnes det et naturlig tall $y$ slik at $y \mid x$ og $y \mid p^{n}$, og slik at $y > 1$. Siden $y \mid p^{n}$, følger det fra Lemma \ref{LemmaNDelerPrimtallOpphoeydINFoererTilAtPrimtallDelerN} at $p \mid y$. Dermed er $y=kp$, hvor $k$ er et naturlig tall. 

\item[(2)] Siden $y \mid x$, finnes det et naturlig tall $l$ slik at $x = ly$. Dermed er $x=l(kp)$, altså $x=(kl)p$. La oss betegne det naturlige tallet $kl$ som $m$.

\item[(3)] Siden $x \leq p^{n}$, altså $mp \leq p^{n}$, er $m \leq p^{n-1}$.    

\end{itemize} 

La nå $m$ være et hvilket som helst naturlig tall slik at $m \leq p^{n-1}$. Da har vi: $p \mid mp$ og $p \mid p^{n-1}$. Derfor er $\sfd{mp,p^{n}} \geq p$, altså $\sfd{mp,p^{n}} > 1$.  

Således har vi bevist: %
%
\begin{itemize}

\item[(A)] dersom $x \leq p^{n}$ og $\sfd{x,p^{n}} \not= 1$, finnes det et naturlig tall $m$ slik at $m \leq p^{n-1}$ og $x=mp$;

\item[(B)] dersom $m$ er et naturlig tall slik at $m \leq p^{n-1}$, er $\sfd{mp,p^{n}} \not= 1$. 

\end{itemize} %
%
Det følger at de naturlige tallene $x$ slik at $x \leq p^{n}$ og $\sfd{x,p^{n}} \not= 1$ er akkurat de naturlige tallene $p$, $2p$, $3p$, \ldots, $\left( p^{n-1} \right)p$. Denne lista består av akkurat $p^{n-1}$ ulike naturlige tall. Siden antall naturlige tall $x$ slik at $x \leq p^{n}$ er $p^{n}$, konkluderer vi at antall naturlige tall slik at $x \leq p^{n}$ og $\sfd{x,p^{n}} = 1$ er $p^{n} - p^{n-1}$, altså at $\phi\left( p^{n} \right) = p^{n} - p^{n-1}$. 
 
 \end{proof}

\begin{eks} Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe} fastslår at $\phi(2^{2}) = 2^{2} - 2^{1}$, altså at $\phi(4) = 2$. Ut ifra Eksempel \ref{EksempelTotientenTil4} er dette riktignok sant.   \end{eks} 

\begin{eks} \label{EksempelTotientenTil9}   Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe} fastslår at $\phi(3^{2}) = 3^{2} - 3^{1}$, altså at $\phi(9) = 6$. Følgende tabell viser at dette riktignok er sant. 

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,9}$ & Bidrar til $\phi(9)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $1$ & \tick \\
$3$ & $3$ & \cross \\
$4$ & $1$ & \tick \\
$5$ & $1$ & \tick \\
$6$ & $3$ & \cross \\
$7$ & $1$ & \tick \\
$8$ & $1$ & \tick \\
$9$ & $9$ & \cross \\
\bottomrule
\end{tabular}
\end{center} 

Som fastslått av beviset for Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe}, er det de naturlige tallene $3$, $6$, og $9$, altså $3$, $2 \cdot 3$, og $3 \cdot 3$, som ikke bidrar til $\phi(9)$.
 \end{eks} 

\begin{eks} \label{EksempelTotientenTil8} Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe} fastslår at $\phi(2^{3}) = 2^{3} - 2^{2}$, altså at $\phi(8) = 4$. Følgende tabell viser at dette riktignok er sant. 

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,8}$ & Bidrar til $\phi(8)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $2$ & \cross \\
$3$ & $1$ & \tick \\
$4$ & $4$ & \cross \\
$5$ & $1$ & \tick \\
$6$ & $2$ & \cross \\
$7$ & $1$ & \tick \\
$8$ & $8$ & \cross \\
\bottomrule
\end{tabular}
\end{center} 

Som fastslått av beviset for Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe}, er det de naturlige tallene $2$, $4$, $6$, og $8$, altså $2$, $2 \cdot 2$, $3 \cdot 2$, og $4 \cdot 2$, som ikke bidrar til $\phi(8)$.
 \end{eks} 

\section{Eulers teorem}

\begin{merknad} Vi kommer til å bygge på følgende proposisjon, som er viktig i seg selv, for å gi et bevis for Eulers teorem. \end{merknad}

\begin{prpn} \label{ProposisjonTotientenHomomorfisme} La $m$ og $n$ være naturlige tall slik at $\sfd{m,n}=1$. Da er $\phi(mn) = \phi(m) \cdot \phi(n)$. \end{prpn}

\begin{proof} Ut ifra Eksempel \ref{EksempelTotientenTil1} er $\phi(1) = 1$. Det følger umiddelbart at utsagnet er sant når $m=1$ eller når $n=1$. 

Anta at $m > 1$ og at $n > 1$. Ut ifra definisjonen til $\phi(m)$, finnes det $\phi(m)$ naturlige tall $x$ slik at $\sfd{x,m}=1$. La oss betegne disse naturlige tallene som $x_{1}$, $x_{2}$, $\ldots$, $x_{\phi(m)}$. 

Ut ifra definisjonen til $\phi(n)$, finnes det $\phi(n)$ naturlige tall $y$ slik at $\sfd{y,n}=1$. La oss betegne disse naturlige tallene som $y_{1}$, $y_{2}$, $\ldots$, $y_{\phi(n)}$.  

Ut ifra Proposisjon \ref{ProposisjonAlleHeltalleneKongruentTilEtHeltallMindreEnnN} finnes det, for hvert naturlig tall $i$ slik at $i \leq \phi(m)$ og hvert naturlig tall $j$ slik at $j \leq \phi(n)$, et naturlig tall $r_{i,j}$ slik at \[ nx_{i} + my_{j} \equiv r_{i,j} \pmod{mn} \] og $0 \leq r_{i,j} < mn$.  

Anta at følgende utsagn har blitt bevist. %
%
\begin{itemize}

\item[(A)] For hvert naturlig tall $i$ slik at $i \leq \phi(m)$, og hvert naturlig tall $j$ slik at $j \leq \phi(n)$, er $\sfd{r_{i,j},mn}=1$.

\item[(B)] La nå $i$ og $i'$ være naturlige tall slik at $i \leq \phi(m)$ og $i' \leq \phi(m)$. La $j$ og $j'$ være naturlige tall slik at $j \leq \phi(n)$ og $j' \leq \phi(n)$. Da er $r_{i,j} = r_{i',j'}$ om og bare om $x_{i}=x_{i'}$ og $y_{j}=y_{j'}$.   

\item[(C)] Dersom $z$ er et naturlig tall slik at $z < mn$ og $\sfd{z,mn}=1$, finnes det et naturlig tall $i$ og et naturlig tall $j$ slik at $z = r_{i,j}$.

\end{itemize} %
%
Det følger fra (A) og (C) at $\phi(mn)$ er antall ulike naturlige tall blant de naturlige tallene $r_{i,j}$, hvor $i$ er et naturlig tall slik at $i \leq \phi(m)$, og $j$ er et naturlig tall slik at $j \leq \phi(n)$. Siden alle de naturlige tallene $x_{1}$, $x_{2}$, \ldots, $x_{\phi(m)}$ er ulike, og siden alle de naturlige tallene $y_{1}$, $y_{2}$, \ldots, $y_{\phi(n)}$ er ulike, følger det fra (B) at alle de naturlige tallene $r_{i,j}$ er ulike, hvor $i \leq \phi(m)$ og $j \leq \phi(n)$, altså at det er akkurat $\phi(m) \cdot \phi(n)$ av dem. Vi konkluderer at \[ \phi(mn) = \phi(m) \cdot \phi(n). \]  

La oss nå bevise at (A) -- (C) er sanne. La $i$ være et naturlig tall slik at $i \leq \phi(m)$. La $j$ være et naturlig tall slik at $j \leq \phi(n)$. La $z$ være et naturlig tall slik at $z \mid r_{i,j}$ og $z \mid mn$. Vi gjør følgende observasjoner. %

%
\begin{itemize}

\item[(1)] Siden \[ nx_{i} + my_{j} \equiv r_{i,j} \pmod{mn}, \] følger det da fra Proposisjon \ref{} og antakelsen $z \mid r_{i,j}$ at \[ nx_{i} + my_{j} \equiv 0 \pmod{z}, \] altså at \[ z \mid nx_{i} + my_{j}. \]

\item[(2)] Dersom $z > 1$, følger det fra Korollar \ref{KorollarHvertNaturligTallDeleligMedEtPrimtall} at det finnes et primtall $p$ slik at $p \mid z$. Da følger det fra (1) og Proposisjon \ref{ProposisjonMDeleligMedLOgNDeleligMedMImplisererNDeleligMedL} at $p \mid nx_{i} + my_{j}$. 

\item[(3)] Siden $p \mid z$ og $z \mid mn$, følger det fra Proposisjon \ref{ProposisjonMDeleligMedLOgNDeleligMedMImplisererNDeleligMedL} at $p \mid mn$. Siden $p$ er et primtall, følger det da fra Proposisjon \ref{ProposisjonPrimtallDelerProduktParHeltallFoererTilDetDelerEttAvLeddene} at enten $p \mid m$ eller $p \mid n$.

\item[(4)] Anta først at $p \mid m$. Siden $\sfd{m,n} = 1$, er det da ikke sant at $p \mid n$. 

\item[(5)] Siden $p \mid m$, følger det fra Korollar \ref{KorollarNOgMDeleligeMedLImplisererMNDeleligMedL} at $p \mid -my_{j}$. 

\item[(6)] Det følger fra (3), (5), og Proposisjon \ref{ProposisjonNOgMDeleligeMedLImplisererMPlussNDeleligMedL} at \[ p \mid \left( nx_{i} + my_{j} \right) - my_{j}, \] altså at $p \mid nx_{i}$. 

\item[(7)] Siden det ikke er sant, ut ifra (4), at $p \mid n$, følger det fra (6) og Proposisjon \ref{ProposisjonPrimtallDelerProduktParHeltallFoererTilDetDelerEttAvLeddene} at $p \mid x_{i}$. Siden vi har antatt at $p \mid m$, er da $\sfd{x_{i},m} \geq p$. 

\item[(8)] Ut ifra definisjonen til $x_{i}$, er imidlertid $\sfd{x_{i},m} = 1$. Siden antakelsen at $p \mid m$ fører til denne motsigelsen, konkluderer vi at det ikke er sant at $p \mid m$.

\item[(9)] Anta istedenfor at $p \mid n$. Et lignende argument som i (4) -- (7) fastslår at det da finnes et primtall $q$ slik at $\sfd{y_{j},n} \geq q$. Ut ifra definisjonen til $y_{j}$, er imidlertid $\sfd{y_{j},n} = 1$. Siden antakelsen at $p \mid n$ fører til denne motsigelsen, konkluderer vi at det ikke er sant at $p \mid n$.

\item[(10)] Dermed har vi en motsigelse: (2) fastslår at enten $p \mid m$ eller $p \mid n$, mens (8) og (9) fastslår at verken $p \mid m$ eller $p \mid n$. Siden antakelsen at $z > 1$ fører til denne motsigelsen, konkluderer vi at $z = 1$. 

\end{itemize} %
%
Således har vi bevist at, dersom $z \mid r_{i,j}$ og $z \mid mn$, er $z=1$. Vi konkluderer at $\sfd{r_{i,j},mn} = 1$, altså at (A) er sant.  

La nå $i$ og $i'$ være naturlige tall slik at $i \leq \phi(m)$ og $i' \leq \phi(m)$. La $j$ og $j'$ være naturlige tall slik at $j \leq \phi(n)$ og $j' \leq \phi(n')$. Anta at $r_{i,j} = r_{i',j'}$. Da er \[ ns_{i} + my_{j} \equiv nx_{i'} + my_{j'} \pmod{mn}. \] Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Det følger at \[ n(x_{i} -x_{i'}) + m(y_{j} -y_{j'}) \equiv 0 \pmod{mn}. \] Derfor har vi: \[ mn \mid n(x_{i} -x_{i'}) + m(y_{j} -y_{j'}). \] 

\item[(2)] Dermed finnes det et heltall $k$ slik at \[ n(x_{i} -x_{i'}) + m(y_{j} -y_{j'}) = k(mn), \] altså slik at \[ n(x_{i} -x_{i'}) = \big( y_{j'} - y_{j} + kn \big) m. \] Således har vi: $m \mid n(x_{i} - x_{i'})$.

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonEuklidsLemma} har vi da: enten $m \mid n$ eller $m \mid x_{i} - x_{i'}$.

\item[(4)] Dersom $m \mid n$, følger det fra Proposisjon \ref{ProposisjonSFDNDeleligMedL} at $\sfd{m,n}=m$. Imidlertid har vi antatt at $\sfd{m,n} = 1$. Siden $m > 1$, har vi da en motsigelse. Siden antakelsen at $m \mid n$ fører til denne motsigelsen, konkluderer vi at det ikke er sant at $m \mid n$.  

\item[(5)] Dersom $m \mid x_{i} - x_{i'}$, er \[ x_{i} \equiv x_{i'} \pmod{m}. \] Siden $x_{i} < m$ og $x_{i'} < m$, følger det fra Proposisjon \ref{ProposisjonResterEquivModNHvisOgBareHvisLik} at $x_{i} = x_{i'}$.  

\item[(6)] Et lignende argument som i (1) -- (5) fastslår at  $n \mid m(y_{j'} - y_{j})$, og deretter at $y_{j'} = y_{j}$.

\end{itemize} %
%
Således har vi bevist at, dersom $r_{i,j} = r_{i',j'}$, er $x_{i} = x_{i'}$ og $y_{j} = y_{j'}$. Dermed er (B) sant.  

La nå $z$ være et naturlig tall slik at $z < mn$ og $\sfd{z,mn} = 1$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonSFDXOgYZLik1OmOgBareOmSFDXYLik1OgSFDXZLik1} er da $\sfd{m,z} = 1$. 

\item[(2)] Ut ifra Proposisjon \ref{ProposisjonDivisjonsalgoritme}, finnes et naturlig tall $k$ og et naturlig tall $r$ slik at $0 \leq r < m-1$ og \[ z = km + r. \] 

\item[(3)] Ut ifra Lemma \ref{LemmaSFDDivisjonsalgoritme} er $\sfd{m,r} = \sfd{z,m}$. 

\item[(4)] Det følger fra (1) og (3) at $\sfd{m,r} = 1$. Ut ifra definisjonen til de naturlige tallene $x_{1}$, $x_{2}$, $\ldots$, $x_{\phi(m)}$, finnes det derfor et naturlig tall $i$ slik at $i \leq \phi(m)$ og $r = x_{i}$. Dermed er \[ km + r \equiv 0 + x_{i} \pmod{m}, \] altså er \[ z \equiv x_{i} \pmod{m}. \] 

\item[(5)] Ut ifra Proposisjon \ref{ProposisjonSFDXOgYZLik1OmOgBareOmSFDXYLik1OgSFDXZLik1} er $\sfd{n,z} = 1$. Et lignende argument som i (2) -- (4) fastslår da at det finnes et naturlig tall $j$ slik at $j \leq \phi(n)$ og \[ z \equiv y_{j} \pmod{n}. \]

\item[(6)] Ut ifra (4) og (5) er $x=z$ en løsning både til kongruensen \[ x \equiv x_{i} \pmod{m} \] og til kongruensen \[ x \equiv y_{j} \pmod{n}. \] Siden $\sfd{m,n}=1$, følger det fra Proposisjon \ref{ProposisjonDetKinesiskeRestteoremet} (I) at $x=nx_{i} + my_{j}$ også er en løsning til begge kongruensene. 

\item[(7)] Det følger fra (6) og Proposisjon \ref{ProposisjonDetKinesiskeRestteoremet} (II) at \[ z \equiv nx_{i} + my_{j} \pmod{mn}, \] altså at \[ z \equiv r_{i,j} \pmod{mn}. \] Siden $z < mn$ og $r_{i,j} < mn$, følger det fra Proposisjon \ref{ProposisjonResterEquivModNHvisOgBareHvisLik} at $z=r_{i,j}$.

\end{itemize} %
%
Således har vi bevist at, dersom $z < mn$ og $\sfd{z,mn} =1$, finnes det et naturlig tall $i$ og et naturlig tall $j$ slik at $z=r_{i,j}$, hvor $i \leq \phi(m)$ og $j \leq \phi(n)$. Dermed er (C) sant.
\end{proof} 

\begin{eks} Ut ifra Eksempel \ref{EksempelTotientenTil3} er $\phi(3) = 2$. Ut ifra Eksempel \ref{EksempelTotientenTil4} er $\phi(4) = 2$. Da fastslår Proposisjon \ref{ProposisjonTotientenHomomorfisme} at $\phi(3 \cdot 4) = \phi(3) \cdot \phi(4)$, altså at $\phi(12) = 2 \cdot 2 = 4$. Ut ifra Eksempel \ref{EksempelTotientenTil12} er dette riktignok sant.   

Ut ifra Eksempel \ref{EksempelTotientenTil3} er $1$ og $2$ de to naturlige tallene $x$ slik at $x \leq 3$ og $\sfd{x,3}=1$. Ut ifra Eksempel \ref{EksempelTotientenTil4} er $1$ og $3$ de to naturlige tallene $x$ slik at $x \leq 4$ og $\sfd{x,4}=1$. Da fastslår beviset for Proposisjon \ref{ProposisjonTotientenHomomorfisme} at de naturlige tallene $x$ slik at $x \leq 12$ og $\sfd{x,12}=1$ er kongruent modulo $12$ til $4 \cdot 1 + 3 \cdot 1$, $4 \cdot 1 + 3 \cdot 3$, $4 \cdot 2 + 3 \cdot 1$, og $4 \cdot 2 + 3 \cdot 3$, altså til $7$, $13$, $11$, og $17$. De naturlige tallene $x$ slik at $x \leq 12$ som er kongruent modulo 12 til disse er: $7$, $1$, $11$, og $5$. Ut ifra Eksempel \ref{EksempelTotientenTil12} er det riktignok disse fire naturlige tallene som bidrar til $\phi(12)$.  

\end{eks}

\begin{eks} Ut ifra Eksempel \ref{EksempelTotientenTil5} er $\phi(5) = 4$. Ut ifra Eksempel \ref{EksempelTotientenTil6} er $\phi(6) = 2$. Da fastslår Proposisjon \ref{ProposisjonTotientenHomomorfisme} at $\phi(5 \cdot 6) = \phi(5) \cdot \phi(6)$, altså at $\phi(30) = 4 \cdot 2 = 8$.   

Ut ifra Eksempel \ref{EksempelTotientenTil5} er $1$, $2$, $3$, og $4$ de fire naturlige tallene $x$ slik at $x \leq 5$ og $\sfd{x,5}=1$. Ut ifra Eksempel \ref{EksempelTotientenTil6} er $1$ og $5$ de to naturlige tallene $x$ slik at $x \leq 6$ og $\sfd{x,6}=1$. Da fastslår beviset for Proposisjon \ref{ProposisjonTotientenHomomorfisme} at de naturlige tallene $x$ slik at $x \leq 30$ og $\sfd{x,30}=1$ er kongruent modulo $30$ til $6 \cdot 1 + 5 \cdot 1$, $6 \cdot 1 + 5 \cdot 5$, $6 \cdot 2 + 5 \cdot 1$, $6 \cdot 2 + 5 \cdot 5$, $6 \cdot 3 + 5 \cdot 1$, $6 \cdot 3 + 5 \cdot 5$, $6 \cdot 4 + 5 \cdot 1$, og $6 \cdot 4 + 5 \cdot 5$, altså til $11$, $31$, $17$, $37$, $23$, $43$, $29$, og $49$. De naturlige tallene $x$ slik at $x \leq 30$ som er kongruent modulo 30 til disse er: $11$, $1$, $17$, $7$, $23$, $13$, $29$, og $19$. 

\end{eks}

\begin{merknad} \label{MerknadTotientenIkkeHomomorfismeUtenAntakelsenAtHeltalleneErRelativtPrimiske} Proposisjon \ref{ProposisjonTotientenHomomorfisme} er ikke nødvendigvis sant om vi ikke antar at $\sfd{m,n}=1$. Ut ifra Eksempel \ref{EksempelTotientenTil2} er $\phi(2)=1$. Derfor er $\phi(2) \cdot \phi(2) = 1 \cdot 1 = 1$.  Ut ifra Eksempel \ref{EksempelTotientenTil4} er imidlertid $\phi(2 \cdot 2) = \phi(4) = 2$. 

For et annet eksempel, er, ut ifra Eksempel \ref{EksempelTotientenTil4}, $\phi(4) = 2$. Ut ifra Eksempel \ref{EksempelTotientenTil4}, er $\phi(6) = 2$. Imidlertid viser følgende tabell at $\phi(24) = 8$.  

\begin{center}
\begin{tabular}{llc}
\toprule
$x$ & $\sfd{x,24}$ & Bidrar til $\phi(24)$? \\
\midrule
$1$ & $1$ & \tick \\
$2$ & $2$ & \cross \\
$3$ & $3$ & \cross \\
$4$ & $4$ & \cross \\
$5$ & $1$ & \tick \\
$6$ & $6$ & \cross \\
$7$ & $1$ & \tick \\
$8$ & $8$ & \cross \\
$9$ & $1$ & \tick \\
$10$ & $2$ & \cross \\
$11$ & $1$ & \tick \\
$12$ & $12$ & \cross \\
$13$ & $1$ & \tick \\
$14$ & $2$ & \cross \\
$15$ & $3$ & \cross \\
$16$ & $8$ & \cross \\
$17$ & $1$ & \tick \\
$18$ & $6$ & \cross \\
$19$ & $1$ & \tick \\
$20$ & $4$ & \cross \\
$21$ & $3$ & \cross \\
$22$ & $2$ & \cross \\
$23$ & $1$ & \tick \\
$24$ & $24$ & \cross \\
\bottomrule
\end{tabular}
\end{center} 

\end{merknad}

\begin{merknad} At Proposisjon \ref{ProposisjonTotientenHomomorfisme} er sann gir oss muligheten til å benytte oss av en kraftig og begrepsmessig tilnærmingsmetode for å bevise en proposisjon om totienten til et hvilket som helst naturlig tall $n$: %
%
\begin{itemize}

\item[(1)] Observer at, ut ifra Korollar \ref{KorollarAritmetikkensFundamentalteoremUtenRepetisjoner}, finnes det en primtallsfaktorisering \[ n = p_{1}^{k^{1}} p_{2}^{k_{2}} \cdots p_{t}^{k_{t}} \] til $n$ slik at $p_{i} \not= p_{j}$ dersom $i \not= j$.

\item[(2)] Siden $p_{i} \not= p_{j}$ dersom $i \not= j$, følger det fra Korollar \ref{KorollarSFDToPrimtallOpphoeydeINoeLik1} at $\sfd{p_{i}^{k_{i}},p_{j}^{k_{j}}} = 1$ dersom $i \not= j$. Observer at, ut ifra Proposisjon \ref{ProposisjonTotientenHomomorfisme}, er da \[ \phi(n)  = \phi \left( p_{1}^{k_{1}} \right)\phi\left( p_{2}^{k_{2}} \right) \cdots \phi\left( p_{t}^{k_{t}} \right). \]

\item[(3)] Bevis at proposisjonen er sann når $n=q$, hvor $q$ er et primtall.

\item[(4)] Benytt (2) og (3) for å gi et bevis for proposisjonen når $n$ er et hvilket som helst naturlig tall.

\end{itemize} %
%
Vi kommer nå til å benytte oss av denne tilnærmingsmetoden for å gi et bevis for Eulers teorem. 
    \end{merknad} 

\begin{prpn} \label{ProposisjonEulersTeoremPrimtallOpphoeydINoe} La $p$ være et primtall. La $n$ være et naturlig tall. La $x$ være et heltall slik at det ikke er sant at \[ x \equiv 0 \pmod{p}. \] Da er \[ x^{\phi\left( p^{n} \right)} \equiv 1 \pmod{p^{n}}. \] \end{prpn}

\begin{proof} Først sjekker vi om proposisjonen er sann når $n=1$. I dette tilfellet er utsagnet at \[ x^{\phi(p)} \equiv 1 \pmod{p}. \] Ut ifra Proposisjon \ref{ProposisjonTotientenTilNLikNMinus1OmOgBareOmNPrimtall} er $\phi(p) = p-1$. Derfor er utsagnet at \[ x^{p-1} \equiv 1 \pmod{p}. \] Ut ifra Korollar \ref{KorollarFermatsLilleTeoremPMinus1} er dette sant. 

Anta nå at proposisjonen har blitt bevist når $n=m$, hvor $m$ et et gitt naturlig tall. Således har det blitt bevist at \[ x^{\phi\left(p^{m}\right)} \equiv 1 \pmod{p^{m}}. \] Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Da har vi: $p^{m} \mid x^{\phi\left(p^{m} \right)} -1$. Dermed finnes det et naturlig tall $k$ slik at \[ x^{\phi\left(p^{m} \right)} -1 =kp^{m}, \] altså slik at \[ x^{\phi\left(p^{m} \right)} = 1 + kp^{m}. \] 

\item[(2)] Ut ifra Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe} er \[ \phi\left(p^{m+1}\right) = p^{m+1} - p^{m} = p\left( p^{m} - p^{m-1} \right). \] Det følger også fra Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe} at \[ \phi\left( p^{m} \right) = p^{m} - p^{m-1}. \] Dermed er \[ \phi\left(p^{m+1}\right) = p\phi\left( p^{m} \right). \] 

\item[(3)] Det følger fra (1) og (2) at %
%
\begin{align*} %
%
x^{\phi\left( p^{m+1} \right)} &= x^{p \phi\left( p^{m} \right)} \\
                               &= \left( x^{\phi\left(p^{m}\right)} \right)^{p} \\
                               &= \left(  1 + kp^{m} \right)^{p} 
\end{align*} 

\item[(4)] Ut ifra Proposisjon \ref{ProposisjonBinomialteoremet} er %
%
\begin{align*} %
%
\left(  1 + kp^{m} \right)^{p} &= \sum_{i=0}^{p} \binom{p}{i} 1^{p-i}\left( kp^{m} \right)^{i} \\
&= 1 + \binom{p}{1} \cdot \left( kp^{m} \right)^{1} + \cdots + \binom{p}{p-1} \left(kp^{m}\right)^{p-1} + \left( kp^{m} \right)^{p}.
\end{align*}

\item[(5)] For hvert naturlig tall $i$ slik at $i \geq 2$, er \[ im \geq 2m \geq m+1. \] Derfor er \[ im - \left(m+1 \right) \geq 0. \] Siden \[ p^{im} = p^{im - \left(m+1\right)} p^{m+1}, \] har vi da: $p^{m+1} \mid p^{im}$. Det følger fra Korollar \ref{KorollarNOgMDeleligeMedLImplisererMNDeleligMedL} at $p^{m+1} \mid kp^{im}$, altså at $p^{m+1} \mid \left( kp \right)^{i}$. Således er \[ \left( kp \right)^{i} \equiv 0 \pmod{p^{m+1}}. \]

\item[(6)] Siden $\binom{p}{1} = p$, er \[ \binom{p}{1}kp^{m} = kp{m+1}. \] Siden $p^{m+1} \mid kp^{m+1}$, har vi da: \[ p^{m+1} \mid \binom{p}{1}kp^{m}. \] Derfor er \[ \binom{p}{1}kp^{m} \equiv 0 \pmod{p^{m+1}}. \] 

\item[(7)] Ut ifra (5) og (6) er %
%
\begin{align*} %
%\
& 1 + \binom{p}{1} \cdot \left( kp^{m} \right)^{1} + \cdots + \binom{p}{p-1} \left(kp^{m}\right)^{p-1} + \left( kp^{m} \right)^{p} \\
&\equiv 1 + 0 + \cdots + 0 + 0 \pmod{p^{m+1}},
\end{align*} %
%
altså \[ 1 + \binom{p}{1} \cdot \left( kp^{m} \right)^{1} + \cdots + \binom{p}{p-1} \left(kp^{m}\right)^{p-1} + \left( kp^{m} \right)^{p} \equiv 1 \pmod{p^{m+1}}. \]
 
\end{itemize} %
%
Det følger fra (3), (4), og (7) at \[ x^{\phi\left( p^{m+1} \right)} \equiv 1 \pmod{p^{m+1}} . \] Således er proposisjonen sann når $n=m+1$. 

Ved induksjon konkluderer vi at proposisjonen er sann når $n$ er et hvilket som helst naturlig tall.

\end{proof} 

\begin{eks} Ut ifra Eksempel \ref{EksempelTotientenTil8} er $\phi\left( 8 \right) = 4$, altså $\phi\left( 2^{3} \right) = 4$. Da fastslår Proposisjon \ref{ProposisjonEulersTeoremPrimtallOpphoeydINoe} at \[ x^{4} \equiv 1  \pmod{8} \] for et hvilket som helst heltall $x$ slik at det ikke er sant at \[ x \equiv 0 \pmod{2}. \] For eksempel: \[ 5^{4} \equiv 1 \pmod{8}. \] Riktignok har vi: \[ 5^{4} = \left( 5^{2} \right)^{2} = 25^{2} \equiv 1^{2} = 1 \pmod{8}. \] \end{eks} 

\begin{eks} Ut ifra Eksempel \ref{EksempelTotientenTil9} er $\phi\left( 9 \right) = 6$, altså $\phi\left( 3^{2} \right) = 6$. Da fastslår Proposisjon \ref{ProposisjonEulersTeoremPrimtallOpphoeydINoe} at \[ x^{6} \equiv 1  \pmod{9} \] for et hvilket som helst heltall $x$ slik at det ikke er sant at \[ x \equiv 0 \pmod{3}. \] For eksempel: \[ 4^{6} \equiv 1 \pmod{9}. \] Riktignok har vi: \[ 4^{6} = \left( 4^{3} \right)^{2} = 64^{2} \equiv 1^{2} = 1 \pmod{9}. \] \end{eks} 

\begin{prpn} \label{ProposisjonEulersTeorem} La $n$ være et naturlig tall. La $x$ være et heltall slik at $\sfd{x,n} = 1$. Da er \[ x^{\phi(n)} \equiv 1 \pmod{n}. \] \end{prpn}

\begin{proof} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra Korollar \ref{KorollarAritmetikkensFundamentalteoremUtenRepetisjoner}, finnes det et naturlig tall $t$ og primtall $p_{1}$, $p_{2}$, \ldots, $p_{t}$ slik at \[ n = p_{1}^{k^{1}} p_{2}^{k_{2}} \cdots p_{t}^{k_{t}}, \] og $p_{i} \not= p_{j}$ dersom $i \not= j$. 

\item[(2)] Siden $p_{i} \not= p_{j}$ dersom $i \not= j$, følger det fra Korollar \ref{KorollarSFDToPrimtallOpphoeydeINoeLik1} at $\sfd{p_{i}^{k_{i}},p_{j}^{k_{j}}} = 1$ dersom $i \not= j$. Ved å benytte Korollar \ref{} gjentatte ganger, følger det at \[ \sfd{p_{1}^{k_{1}} \cdots p_{i-1}^{k_{t-1}},p_{i}^{k_{i}}} = 1 \] for et hvilket som helst naturlig tall $i$ slik at $2 \leq i \leq t$. 

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonTotientenHomomorfisme}, er da \[ \phi(n)  = \phi \left( p_{1}^{k_{1}} \right)\phi\left( p_{2}^{k_{2}} \right) \cdots \phi\left( p_{t}^{k_{t}} \right). \]

\item[(4)] La $i$ være et naturlig tall slik at $i \leq t$. La $m_{i}$ være \[  \phi \left( p_{1}^{k_{1}} \right) \cdots \phi\left( p_{i-1}^{k_{i-1}} \right) \phi\left( p_{i+1}^{k_{i+1}} \right) \cdots \phi \left( p_{t}^{k_{t}} \right). \] Ut ifra (3), er $\phi(n) = m_{i} \phi\left( p_{i}^{k_{i}} \right)$.

\item[(5)] Dermed er \[ x^{\phi(n)} = x^{m_{i} \phi\left( p_{i}^{k_{i}} \right)} = \left( x^{\phi\left( p_{i}^{k_{i}}\right)} \right)^{m_{i}}.  \]      

\item[(6)] Siden $\sfd{x,n} = 1$, og siden $p_{i} \mid n$, er det ikke sant at $p_{i} \mid x$. Ut ifra Proposisjon \ref{ProposisjonEulersTeoremPrimtallOpphoeydINoe} er da, for hvert naturlig tall $i$ slik at $i \leq t$, \[ x^{\phi\left( p_{i}^{k_{i}} \right)} \equiv 1 \pmod{p_{i}^{k_{i}}}. \]

\item[(7)] Det følger fra (5) og (6) at \[ x^{\phi(n)} \equiv 1^{m_{i}} = 1 \pmod{p_{i}^{k_{i}}}. \]  

\end{itemize} %
%
Således har vi bevist at, for hvert naturlig tall $i$ slik at $i \leq t$, er \[ x^{\phi(n)} \equiv 1 \pmod{p_{i}^{k_{i}}}. \] Siden  \[ \sfd{p_{1}^{k_{1}} \cdots p_{i-1}^{k_{t-1}},p_{i}^{k_{i}}} = 1 \] for et hvilket som helst naturlig tall $i$ slik at $2 \leq i \leq t$, følger det fra Korollar \ref{KorollarKongCModEtParRelPrimHeltallFoererTilKongCModProduktet} at \[ x^{\phi(n)} \equiv 1 \pmod{p_{1}^{k_{1}} p_{2}^{k_{2}} \cdots p_{t}^{k_{t}}}, \] altså at \[ x^{\phi(n)} \equiv 1 \pmod{n}. \]

\end{proof}

\begin{terminologi} Proposisjon \ref{ProposisjonEulersTeorem} kalles {\em Eulers teorem}. \end{terminologi} 

\begin{eks} Ut ifra Eksempel \ref{EksempelTotientenTil8} er $\phi(8) = 4$. Da fastslår Proposisjon \ref{ProposisjonEulersTeorem} at, for et hvilket som helst heltall $x$ slik at $\sfd{x,8} = 1$, er \[ x^{4} \equiv 1 \pmod{8}. \] For eksempel: \[ 3^{4} \equiv 1 \pmod{8}. \] Riktignok har vi: \[ 3^{4} = \left( 3^{2} \right)^{2} \equiv 1^{2} = 1 \pmod{8}. \]  \end{eks}  

\begin{eks} Ut ifra Merknad \ref{MerknadTotientenIkkeHomomorfismeUtenAntakelsenAtHeltalleneErRelativtPrimiske} er $\phi(24) = 8$. Da fastslår Proposisjon \ref{ProposisjonEulersTeorem} at, for et hvilket som helst heltall $x$ slik at $\sfd{x,24} = 1$, er \[ x^{8} \equiv 1 \pmod{24}. \] For eksempel: \[ 7^{8} \equiv 1 \pmod{24}. \] Riktignok har vi: \[ 7^{8} = \left( 7^{2} \right)^{4} \equiv 1^{4} = 1 \pmod{24}. \]  \end{eks}  

\begin{merknad} Følgende korollar er kjernen til RSA-algoritmen, som vi kommer til å se på i den neste delen av kapittelet. \end{merknad} 

\begin{kor} \label{KorollarEulersTeorem} La $n$ være et naturlig tall. La $a$ være et heltall slik at $\sfd{a,\phi(n)}=1$. Ut ifra Korollar \ref{KorollarSFDAOgNLik1FoererTilKongruensenHarAkkuratEenLoesning}, finnes det da et heltall $b$ slik at \[ ab \equiv 1 \pmod{\phi(n)}. \] La $x$ være et heltall slik at $\sfd{x,n} =1$. Da er \[ \left( x^{a} \right)^{b} \equiv x \pmod{n}. \] \end{kor} 

\begin{proof} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Siden \[ ab \equiv 1 \pmod{\phi(n)}, \] har vi: $\phi(n) \mid ab -1$. Dermed finnes det et naturlig tall $k$ slik at $ab-1=k\phi(n)$, altså slik at $ab=1+k\phi(n)$. 

\item[(2)] Da er %
%
\begin{align*} %
%
\left( x^{a} \right)^{b} &= x^{ab} \\
&= x^{1+k\phi(n)} \\
& =x^{1} \cdot x^{k\phi(n)} \\
&= x \cdot \left( x^{\phi(n)} \right)^{k}.
\end{align*}

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonEulersTeorem} er \[ x^{\phi(n)} \equiv 1 \pmod{n}. \] Dermed er \[ x \cdot \left( x^{\phi(n)} \right)^{k} \equiv x \cdot 1^{k} = x \pmod{n}. \]

\item[(4)] Det følger fra (2) og (3) at \[ \left( x^{a} \right)^{b} \equiv x \pmod{n}. \]

\end{itemize}

\end{proof}

\begin{eks} Vi har: %
%
\begin{itemize}

\item[(1)] $\phi(11) = 10$;

\item[(2)] $\sfd{3,10} = 1$;

\item[(3)] $3 \cdot 7 = 21 \equiv 1 \pmod{10}$. 

\end{itemize} %
%
Da fastslår Korollar \ref{KorollarEulersTeorem} at, for et hvilket som helst heltall $x$ slik at $\sfd{x,11} = 1$, er \[ \left( x^{3} \right)^{7} \equiv x \pmod{11}. \] For eksempel: \[ \left( 6^{3} \right)^{7} \equiv 6 \pmod{11}, \] altså \[ 6^{21} \equiv 6 \pmod{11}. \] Én måte å vise at dette riktignok er sant er å følge beviset for \ref{KorollarEulersTeorem}: ut ifra Korollar \ref{KorollarFermatsLilleTeoremPMinus} er \[ 6^{10} \equiv 1 \pmod{11}, \] og deretter er \[ 6^{21} = \left( 6^{10} \right)^{2} \cdot 6 \equiv 1^{2} \cdot 6 = 6 \pmod{11}. \]         

 \end{eks}

\begin{eks} Vi har: %
%
\begin{itemize}

\item[(1)] $\phi(34) = \phi(17) \cdot \phi(2) = 16 \cdot 1 = 16$;

\item[(2)] $\sfd{5,16} = 1$;

\item[(3)] $5 \cdot 13 = 65 \equiv 1 \pmod{16}$. 

\end{itemize} %
%
Da fastslår Korollar \ref{KorollarEulersTeorem} at, for et hvilket som helst heltall $x$ slik at $\sfd{x,34} = 1$, er \[ \left( x^{5} \right)^{13} \equiv x \pmod{34}. \] For eksempel: \[ \left( 9^{5} \right)^{13} \equiv 9 \pmod{34}, \] altså \[ 9^{65} \equiv 9 \pmod{34}. \] Én måte å vise at dette riktignok er sant er å følge beviset for \ref{KorollarEulersTeorem}: ut ifra Proposisjon \ref{ProposisjonEulersTeorem} er \[ 9^{16} \equiv 1 \pmod{34}, \] og deretter er \[ 9^{65} = \left( 9^{16} \right)^{4} \cdot 9 \equiv 1^{4} \cdot 9 = 9 \pmod{34}. \]         

 \end{eks}

\section{Et eksempel på et bevis hvor Eulers teorem benyttes}

\begin{merknad} Proposisjon \ref{ProposisjonEulersTeorem} kan benyttes på en lignende måte som Korollar \ref{KorollarFermatsLilleTeoremPMinus1} ble benyttet i bevisene for Proposisjon \ref{Proposisjon7OpphoeydI104Pluss1DeleligMed17} og Proposisjon \ref{ProposisjonXOpphoeydI4Pluss59DeleligMed60}. La oss se på et eksempel. \end{merknad} 

\begin{prpn} \label{Proposisjon3OpphoyedI37639Minus2187DeleligMed87808} Det naturlige tallet $3^{37639} - 2187$ er delelig med $87808$. \end{prpn} 

\begin{proof} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] En primtallsfaktorisering til $87808$ er \[ 2^{8} \cdot 7^{3}. \]

\item[(2)] Det følger fra (1) og Proposisjon \ref{ProposisjonTotientenHomomorfisme} at $\phi(87808) = \phi\left( 2^{8} \right) \cdot \phi \left( 7^{3} \right)$.

\item[(3)] Ut ifra Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe} er \[ \phi\left( 2^{8} \right) = 2^{8} - 2^{7} = 128. \] 

\item[(4)] Ut ifra Proposisjon \ref{ProposisjonTotientenTilPrimtallOpphoeydINoe} er \[ \phi\left( 7^{3} \right) = 7^{3} - 7^{2} = 294. \] 

\item[(5)] Det følger fra (2) -- (4) at \[ \phi(87808) = 128 \cdot 294 = 37632. \] 

\item[(6)] Ut ifra Proposisjon \ref{ProposisjonEulersTeorem} er \[ 3^{\phi(87808)} \equiv 1 \pmod{87808}. \] 
\item[(7)] Det følger fra (5) og (6) at \[ 3^{37632} \equiv 1 \pmod{87808}. \]

\item[(8)] Det følger fra (7) at \[ 3^{37639} - 2187 = 3^{37632} \cdot 3^{7} - 2187 \equiv 1 \cdot 3^{7} -2187 = 3^{7} - 2187 = 0 \pmod{87808},  \] altså \[ 3^{37639} - 2187 \equiv 0 \pmod{87808}. \] Da har vi: \[ 87808 \mid 3^{37639} - 2187. \]
  
\end{itemize}
\end{proof} 
%forelesning23
