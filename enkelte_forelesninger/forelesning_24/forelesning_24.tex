%forelesning24
\section{RSA-algoritmen} \label{DelRSAAlgoritmen}

\begin{merknad} Én av de meste berømte anveldesene av tallteori er i kryptografi. Alle former for sikre elektroniske overføringer er avhengige av tallteoriske algoritmer som ligner på algoritmen vi kommer til å se på i dette kapittelet: RSA-algoritmen. Noen av algoritmene som brukes i dag benytter mer avansert tallteori: teorien for elliptiske kurver for eksempel, og andre deler av {\em aritmetiske geometri}, en del av dagens forskning i tallteori. Likevel er de fleste algoritmene overraskende enkle. RSA-algoritmen brukes fortsatt veldig mye.  \end{merknad}

\begin{merknad} \label{MerknadTabellForAAOversetteTilHeltall} Kryptografi handler om hvordan meldinger kan krypteres. For å benytte tallteori for å gjøre dette, må vi oversette meldinger til og fra heltall. En mulighten vises i Tabell \ref{TabellMulighetSymbolerTilHeltall}.      

\begin{table}
\centering
\begin{tabular}{ll}
\toprule
Symbol & Tilsvarende heltall \\
\midrule
        & $0$ \\
A       & $1$ \\
B       & $2$ \\
C       & $3$ \\
D       & $4$ \\
E       & $5$ \\
F       & $6$ \\
G       & $7$ \\
H       & $8$ \\
I       & $9$ \\
J       & $10$ \\
K       & $11$ \\
L       & $12$ \\
M       & $13$ \\
N       & $14$ \\
O       & $15$ \\
P       & $16$ \\
Q       & $17$ \\
R       & $18$ \\
S       & $19$ \\
T       & $20$ \\
U       & $21$ \\
V       & $22$ \\
W       & $23$ \\
X       & $24$ \\
Y       & $25$ \\
Z       & $26$ \\
Æ       & $27$ \\
Ø       & $28$ \\
Å       & $29$ \\
0       & $30$ \\
1       & $31$ \\
2       & $32$ \\
3       & $33$ \\
4       & $34$ \\
5       & $35$ \\
6       & $36$ \\
7       & $37$ \\
8       & $38$ \\
9       & $39$ \\
.       & $40$ \\
,       & $41$ \\
!       & $42$ \\
:       & $43$ \\
$-$     & $44$ \\
?       & $45$ \\
\bottomrule
\end{tabular}
\caption{Hvordan oversette meldinger fra symboler til heltall}
\label{TabellMulighetSymbolerTilHeltall} 
\end{table} 

Symbolet i den første raden er et mellomrom. Et hvilket som helst heltall kan velges for å oversette et gitt symbol. Det eneste som er viktig er at ulike symboler tilsvarer til ulike heltall. Ved behøv kan flere symboler tas med.

\end{merknad}

\begin{terminologi} \label{TerminologiOffentligNoekkel} La $p$ og $q$ være primtall slik at $p \not= q$. La $n$ være et heltall slik at \[ 1 < n < (p-1)(q-1) \] og \[ \sfd{n,(p-1)(q-1)} = 1. \] I forbinelse med RSA-algoritmen, sier vi at paret $\left(pq, n \right)$ er en {\em offentlig nøkkel}. Vi sier at paret $(p,q)$ er en {\em privat nøkkel}. \end{terminologi}

\begin{merknad} \label{MerknadDelerProduktetOgIkkeDeIndividuellePrimtallene} Det er avgjørende at det er produktet $pq$ og ikke primtallene $p$ og $q$ hvert for seg som er en del av den offentlige nøkkelen. Grunnen for at RSA-algoritmen er sikker er at vi ikke kjenner til en effektiv algoritme for å finne primtallsfaktoriseringen til et naturlig tall, altså for å finne $p$ og $q$ gitt $pq$.  \end{merknad} 

\begin{merknad} I forbinelse med RSA-algoritmen, velger alle personene som ønsker å få meldinger kryptert av denne algoritmen en offentlig nøkkel. Den øffentlig nøkkelen til en person kan sjekkes opp, som med en telefonkatalog.

Sikkerheten av en melding som har blitt kryptert ved å benytte RSA-algoritmen er imidlertid avhengig av at den private nøkkelen til en person ikke kan sjekkes opp. Det er kun personen selv, og eventuelt andre personer han eller hun stoler på, som bør vite hans eller huns private nøkkel. 
  \end{merknad}

\begin{notn} La $p$ og $q$ være primtall slik at $p \not= q$. La $n$ være et heltall slik at \[ 1 < n < (p-1)(q-1) \] og \[ \sfd{n,(p-1)(q-1)} = 1. \] Ut ifra Korollar \ref{KorollarSFDAOgNLik1FoererTilKongruensenHarAkkuratEenLoesning}, finnes det da et heltall $m$ slik at \[ nm \equiv 1 \pmod{(p-1)(q-1)}. \] Ut ifra Proposisjon \ref{ProposisjonAlleHeltalleneKongruentTilEtHeltallMindreEnnN}, finnes det et naturlig tall $m'$ slik at \[ m \equiv m' \pmod{(p-1)(q-1)} \] og $m' \leq (p-1)(q-1)$. Til tross for at $(p-1)(q-1)$ ikke er et primtall, betegner vi $m'$ som $n^{-1}$ i denne delen av kapittelet.  \end{notn}

\begin{defn} Anta at person A ønsker å sende en meldig til person B. Anta at person B har valgt en offentlig nøkkel, og at person A vet denne nøkkelen. Å kryptere denne meldingen ved å benytte {\em RSA-algoritmen}, er å gjøre følgende. %
%
\begin{itemize}

\item[(1)] Oversett hvert symbol i meldingen til et heltall, ved å benytte for eksempel tabellen i Merknad \ref{MerknadTabellForAAOversetteTilHeltall}.   
        
\item[(2)] La $g_{1}$, $g_{2}$, $\ldots$, $g_{t}$ være disse heltallene. For hvert naturlig tall $i$ slik at $i \leq t$, finn heltallet $r_{i}$ slik at \[ g_{i}^{n} \equiv r_{i} \pmod{pq} \] og $0 \leq r_{i} < pq$. 

\end{itemize} %
%
Å dekryptere en melding $(r_{1},\ldots,r_{t})$ som har blitt kryptert ved å benytte RSA-algoritmen, er å gjøre følgende. %
%
\begin{itemize}

\item[(1)] Ut ifra Proposisjon \ref{ProposisjonAlleHeltalleneKongruentTilEtHeltallMindreEnnN}, finnes det, for hvert naturlig tall $i$ slik at $i \leq t$, et heltall $s_{i}$ slik at \[ r_{i}^{n^{-1}} \equiv s_{i} \pmod{pq}. \] Finn disse heltallene $s_{1}$, \ldots, $s_{t}$.

\item[(2)] Oversett heltallene $s_{1}$, \ldots, $s_{t}$ til symboler ved å benytte for eksempel tabellen i Merknad \ref{MerknadTabellForAAOversetteTilHeltall}.   

\end{itemize}

\end{defn}

\begin{merknad} \label{MerknadHvorforRSAAlgoritmenVirker} Vi har: \[ \left( r_{i} \right)^{n^{-1}} \equiv \left( g_{i}^{n} \right)^{n^{-1}} \pmod{pq}. \]  Ut ifra Proposisjon \ref{ProposisjonTotientenHomomorfisme} og Proposisjon \ref{ProposisjonTotientenTilNLikNMinus1OmOgBareOmNPrimtall} er \[ \phi(pq)=\phi(p)\phi(q)=(p-1)(q-1). \] Ut ifra Korollar \ref{KorollarEulersTeorem}, er da \[ \left( g_{i}^{n} \right)^{n^{-1}} \equiv g_{i} \pmod{n}. \] Dermed er \[ \left( r_{i} \right)^{n^{-1}} \equiv g_{i} \pmod{pq}. \] %
%
Siden både $s_{i} < pq$ og $g_{i} < pq$, følger det fra Proposisjon \ref{ProposisjonResterEquivModNHvisOgBareHvisLik} at $s_{i} = g_{i}$. Det vil si: ved å kryptere heltallet $g_{i}$ til heltallet $r_{i}$, og ved å da dekryptere $r_{i}$, får vi tilbake $g_{i}$. Dermed er det Korollar \ref{KorollarEulersTeorem} som fastlår at RSA-algoritmen virker: når vi dekryptere en melding som har blitt kryptert, får vi tilbake den opprinnelige meldingen. Siden det er Eulers teorem som fører til Korollar \ref{KorollarEulersTeorem}, er det Eulers teorem som ligger egentlig bak RSA-algoritmen. 
\end{merknad} 

\begin{merknad} La merke til at, for å dedusere at $s_{i} = g_{i}$, er det nødvendig at $g_{i} < pq$ og at $s_{i} < pq$. Det er derfor vi sørge for dette i Steg (2) når vi kryptere, og i Steg (1) når vi dekryptere. 

Det er ikke faktisk nødvendig at $0 \leq r_{i} < pq$. Algoritmen virker ved å sende et hvilket som helst heltall som er kongruent til $g_{i}^{n}$ til person B. For å sørge for at meldingen er sikker, bør vi imidlertid ikke sende $g_{i}^{n}$ selv til Person B: da kan koden knekkes ved å ta den vanlige $n$-te roten til hvert heltall i den krypterte meldingen. Så lenge vi velge $r_{i}$ til å være noe annet, for eksempel til å være et heltall som er mindre enn $pq$, unngår vi dette problemet, fordi det ikke finnes en effektiv algoritme for å finne «$n$-te røtter» i modulær aritmetikk. \end{merknad} 

\begin{merknad} Det er ikke noe spesielt med å bruke to primtall $p$ og $q$ i RSA-algoritmen. Et hvilket som helst naturlig tall kan benyttes istedenfor. Da erstatter vi $(p-1)(q-1)$ med $\phi(n)$. 

Derimot gjør dette ikke mye fra synspunktet av kryptografi. Hvis det hadde vært en effektiv algoritme for å faktorisere $pq$, hadde det nesten sikkert vært en effektiv algoritme for å finne en primtallsfaktorisering til et hvilket som helst naturlig tall. 

Når RSA-algoritmen implementeres i praksis, kan det dessuten være nyttig å benytte et produkt av to primtall istedenfor et hvilket som helst naturlig tall. \end{merknad} 

\begin{eks} Anta at person A ønsker å sende meldingen «Elsker deg!» til person B, og å kryptere meldingen ved å benytte RSA-algoritmen. La oss anta at person B har $(17,3)$ som privat nøkkel, og $(51,7)$ som offentlig nøkkel. Vi har: \[ (17-1) \cdot (3-1) = 16 \cdot 2  = 32, \] Siden $7 < 16$ og $\sfd{7,32}=1$, er denne nøkkelen gyldig. 

Siden \[ 7 \cdot -9 = -63 \equiv 1 \pmod{32}, \] og siden $-9 \equiv 23 \pmod{32}$, er $7^{-1} = 23$.

Først oversetter person A meldingen «Elsker deg!» til heltall, ved å benytte Tabell \ref{TabellMulighetSymbolerTilHeltall}. Tabell \ref{TabellElskerDegOversettelsen} viser oversettelsen. %
%
\begin{table}[h]
\centering
\begin{tabular}{ll}
\toprule
Symbol & Tilsvarende heltall \\
\midrule
E & 5 \\
L & 12 \\
S & 19  \\
K & 11 \\
E & 5 \\
R & 18 \\
  & 0 \\
D & 4 \\
E & 5 \\
G & 7 \\
! & 42 \\
\bottomrule
\end{tabular}
\caption{Oversettelsen av meldingen}
\label{TabellElskerDegOversettelsen}
\end{table} %
%
Dermed blir meldingen: $5 \enskip 12 \enskip 19 \enskip 11 \enskip 5 \enskip 18 \enskip 0 \enskip 4 \enskip 5 \enskip 7 \enskip 42$.  

Da finner person A et heltall $r_{i}$ slik at \[ g_{i}^{7} \equiv r_{i} \pmod{51} \] for hvert par sifre $g_{i}$ i den oversatte meldingen. Tabell \ref{TabellHvordanKryptereElskerDeg} viser resultatene. %
%
\begin{table}[h]
\centering
\begin{tabular}{ll}
\toprule
$g_{i}$ & $r_{i}$ \\
\midrule
6 & 44 \\
12 & 24 \\
19 & 43 \\
11 & 20 \\
5 & 44 \\
18 & 18 \\
0 & 0 \\
4 & 13 \\
5 & 44 \\
7 & 46 \\
42 & 15 \\
\bottomrule
\end{tabular}
\caption{Hvordan kryptere meldingen}
\label{TabellHvordanKryptereElskerDeg}
\end{table} %
%
Utregningene gjennomføres på den vanlige måten. For eksempel: %
%
\begin{align*} %
%
6^{7} &= \left( 6^{3} \right)^{2} \cdot 6 \\
      &= 216^{2} \cdot 6 \\
      &\equiv 12^{2} \cdot 6 \\
      &= 144 \cdot 6 \\
      &\equiv (-9) \cdot 6 \\
      &= -54 \\
      &\equiv 48 \pmod{51} 
\end{align*} %
%
og %
%
\begin{align*} %
%
42^{7} &\equiv (-9)^{7} \\
       &= \left( (-9)^{3} \right)^{2} \cdot (-9) \\
       &= (-729)^{2} \cdot (-9) \\ 
       &\equiv (-15)^{2} \cdot (-9) \\
       &= 225 \cdot (-9) \\
       &\equiv 21 \cdot (-9) \\
       &= -189 \\
       &\equiv 15 \pmod{51}. 
\end{align*} %
%
Dermed blir den krypterte meldingen: $48 \enskip 24 \enskip 43 \enskip 20 \enskip 44 \enskip 18 \enskip 00 \enskip 13 \enskip 44 \enskip 46 \enskip 15$. 

Når person B mottar denne krypterte meldingen, dekrypterer han eller hun den. For å gjøre dette, finner han eller hun, for hvert par sifre $r_{i}$ i den krypterte meldingen, et heltall $s_{i}$ slik at \[ r_{i}^{23} \equiv s_{i} \pmod{51}. \]  Ut ifra Merknad \ref{MerknadHvorforRSAAlgoritmenVirker}, kommer han eller hun til å få $g_{i}$. Det vil si: han eller hun kommer til å få tilbake meldingen: $6 \enskip 12 \enskip 19 \enskip 11 \enskip 5 \enskip 18 \enskip 0 \enskip 4 \enskip 5 \enskip 7 \enskip 42$.  

Da oversetter han eller hun heltallene til symboler ved å benytte Tabell \ref{TabellMulighetSymbolerTilHeltall}. Han eller hun får meldingen: «Elsker deg!».

\end{eks}

\begin{eks} Anta at person B har fått meldingen \[ 45 \enskip 9 \enskip 44 \enskip 44 \enskip 41 \enskip 0 \enskip 48 \enskip 4 \enskip 45 \enskip 70 \] fra person A. Anta at den offentlige nøkkelen til person B er $(77,17)$, og at den private nøkkelen til person B er $(11,7)$. Vi har: $(11-1) \cdot (7-1) = 10 \cdot 6 =60$. Siden $17 < 60$ og $\sfd{17,60}=1$, er denne nøkkelen gyldig.

Ved for eksempel å benytte Euklids algoritmen, får vi at \[ 17 \cdot (-7) \equiv 1 \pmod{60}. \] Siden \[ -7 \equiv 53 \pmod{60}, \] er $17^{-1} = 53$. 

For å dekryptere meldingen, finner person B, for hvert par sifre $r_{i}$ i den krypterte meldingen, et heltall $s_{i}$ slik at \[ r_{i}^{53} \equiv s_{i} \pmod{77}. \] Tabell \ref{TabellHvordanDeKryptereLykkeTil} viser resultatene. % 
%
\begin{table}[h]
\centering
\begin{tabular}{ll}
\toprule
$r_{i}$ & $s_{i}$ \\
\midrule
45 & 12 \\
9 & 25 \\
44 & 11 \\
44 & 11 \\
41 & 6 \\
0 & 0 \\
48 & 20 \\
4 & 9 \\
45 & 12 \\
70 & 42 \\
\bottomrule
\end{tabular}
\caption{Hvordan dekryptere meldingen}
\label{TabellHvordanDeKryptereLykkeTil}
\end{table} %
%
Dermed blir meldingen: $12 \enskip 25 \enskip 11 \enskip 11 \enskip 6 \enskip 18 \enskip 0 \enskip 20 \enskip 9 \enskip 12 \enskip 42$. 

Nå oversetter person B denne meldingen til symboler, ved å benytte Tabell \ref{TabellMulighetSymbolerTilHeltall}. Tabell \ref{TabellLykkeTilOversettelsen} viser oversettelsen. % 
%
\begin{table}[h]
\centering
\begin{tabular}{ll}
\toprule
Heltall & Tilsvarende symbol \\
\midrule
12 & L \\
25 & Y \\
11 & K \\
11 & K \\
6 & E \\
0 &  \\
20 & T \\
9 & I \\
12 & L \\
42 & ! \\
\bottomrule
\end{tabular}
\caption{Oversettelsen av meldingen}
\label{TabellLykkeTilOversettelsen}
\end{table} %
%
Person B får altså meldingen: «Lykke til!».
\end{eks}

\begin{merknad} La $(m,n)$ være en offentlig nøkkel. Dersom vi kan finne de to primtallene $p$ og $q$ slik at $m=pq$, kan vi regne ut $n^{-1}$. Da kan vi knekke koden til meldinger som blir kryptert ved å benytte denne offentlige nøkkelen. 

Som nevnt i Merknad \ref{MerknadDelerProduktetOgIkkeDeIndividuellePrimtallene}, finnes det imidlertid ikke en effektiv algoritme for å finne $p$ og $q$. Så lenge vi velger $p$ og $q$ til å være store nok, kommer til og med den kraftigste datamaskinen som finnes i dag ikke til å ha en sjanse til å finne $p$ og $q$, med mindre den blir utrolig heldig! 

I dag er $p$ og $q$ mer enn store nok om de har rundt 250 sifre.
   
\end{merknad} 

\begin{eks} Anta at person B har fått meldingen \[ 2 \enskip 20 \enskip 9 \enskip 0 \enskip 25 \enskip 21 \enskip 13 \enskip 35 \] fra person A. Anta at den offentlige nøkkelen til person B er $(55,13)$.

 Anta at person C ønsker å knekke koden til meldingen. Da må han eller hun finne primtall $p$ og $q$ slik at $55=pq$. Han eller hun kommer fram til: $p=5$ og $q=11$. Nå regner han eller hun ut $13^{-1}$. Vi har: \[ (5-1) \cdot (11 -1) = 4 \cdot 10 = 40. \] Siden \[ 13 \cdot 3 = 39 \equiv -1 \pmod{40}, \] er $x=-3$ en løsning til kongruensen \[ 13x \equiv 1 \pmod{40}. \] Siden \[ -3 \equiv 37 \pmod{40}, \] er da $n^{-1}=37$. Alternativt kan Euklids algoritme benyttes for å komme fram til dette. 

For å dekryptere meldingen, finner person C, for hvert par sifre $r_{i}$ i den krypterte meldingen, et heltall $s_{i}$ slik at \[ r_{i}^{37} \equiv s_{i} \pmod{55}. \] Tabell \ref{TabellHvordanDeKryptereGodTur} viser resultatene. % 
%
\begin{table}[h]
\centering
\begin{tabular}{ll}
\toprule
$r_{i}$ & $s_{i}$ \\
\midrule
2 & 7 \\
20 & 15 \\
9 & 4 \\
0 & 0 \\
25 & 20 \\
21 & 21 \\
13 & 18 \\
35 & 40 \\
\bottomrule
\end{tabular}
\caption{Hvordan dekryptere meldingen}
\label{TabellHvordanDeKryptereGodTur}
\end{table} %
%
Dermed blir meldingen: $07 \enskip 15 \enskip 4 \enskip 0 \enskip 20 \enskip 21 \enskip 18 \enskip 40$. 

Nå oversetter person C denne meldingen til symboler, ved å benytte Tabell \ref{TabellMulighetSymbolerTilHeltall}. Tabell \ref{TabellGodTurOversettelsen} viser oversettelsen. % 
%
\begin{table}[h]
\centering
\begin{tabular}{ll}
\toprule
Heltall & Tilsvarende symbol \\
\midrule
7 & G \\
15 & O \\
04 & D \\
0 &  \\
20 & T \\
21 & U \\
18 & R \\
40 & . \\
\bottomrule
\end{tabular}
\caption{Oversettelsen av meldingen}
\label{TabellGodTurOversettelsen}
\end{table} %
%
Person C finner altså at meldingen er: «God tur.».

\end{eks}

\begin{merknad} Når RSA-algoritmen benyttes i praksis, må oversettelsen fra symboler til heltall gjøres på en mer sikker måte enn å benytte en tabell som Tabell \ref{TabellMulighetSymbolerTilHeltall}. Ett problem er at hadde det vært mulig å for eksempel gjette at en kryptert gruppe sifre som dukker opp ofte er et mellomrom, eller en vokal. Hvis man ser på nok meldinger, hadde det vært mulig å på denne måten gjette hvilke grupper krypterte heltall tilsvarer til hvilke symboler, og dermed dekryptere meldinger til person B. 

Et beslektet problem er at en person som ønsker å knekke koden til meldinger til person B kan for eksempel sende, for hvert symbol, en melding til person B som består av oversettelsen av dette enkelte symbolet: en melding som består kun av oversettelsen av «a», og så en melding som består kun av oversettelsen av «b», osv. Da får han eller hun de gruppene krypterte heltall som tilsvarer til hvert symbol, og dermed kan han eller hun dekryptere en hvilken som helst melding til person B. 

Disse to måter å dekryptere meldinger må alltid tas i betraktning i kryptografi. Det finnes måter å oversette meldinger fra symboler til heltall som er like sikre som RSA-algoritmen selv.  

\end{merknad} 
%forelesning24
