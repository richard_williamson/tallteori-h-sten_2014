%forelesning19A
\section{Eulers kriterium}

\begin{merknad} Følgende proposisjon er kjernen til teorien for kvadratiske rester. Kanskje ser beviset ikke så vanskelig ut, men la merke til at det bygger på Proposisjon \ref{}, det vil si at det finnes en primitiv rot modulo et hvilket som helst primtall. Vi måtte jobbe ganske harde å bevise at dette er sant. Beviset bygger også på Fermats lille teorem.     \end{merknad}

\begin{prpn} \label{ProposisjonEulersKriterium}  La $p$ være et primtall slik at $p > 2$. La $a$ være et heltall slik at det ikke er sant at $p \mid a$. Da er $a$ en kvadratisk rest modulo $p$ hvis og bare hvis \[ a^{\frac{p-1}{2}} \equiv 1 \pmod{p}. \]  \end{prpn}

\begin{proof} Anta først at $a$ er en kvadratisk rest modulo $p$. Da er det et heltall $x$ slik at \[ x^{2} \equiv a \pmod{p}. \] Da er \[ a^{\frac{p-1}{2}} \equiv \left( x^{2} \right)^{\frac{p-1}{2}} = x^{2 \left( \frac{p-1}{2} \right)} = x^{p-1} \pmod{p}. \]  Ut ifra Korollar \ref{KorollarFermatsLilleTeoremPMinus1} er \[ x^{p-1} \equiv 1 \pmod{p}. \] Dermed er \[ a^{\frac{p-1}{2}} \equiv 1 \pmod{p}. \]

Anta istedenfor at \[ a^{\frac{p-1}{2}} \equiv 1 \pmod{p}. \] Ut ifra Proposisjon \ref{} finnes det en primitiv rot modulo $p$. La oss betegne denne primitive roten som $x$. Ut ifra Proposisjon \ref{ProposisjonHvertHeltallKongPrimitivRotOpphoeydINoe} finnes det et heltall $t$ slik at $1 \leq t \leq p-1$ og \[  x^{t} \equiv a \pmod{p}. \] %
%
Da er \[ \left( x^{t} \right)^{\frac{p-1}{2}} \equiv a^{\frac{p-1}{2}} \pmod{p}, \] altså \[ x^{t \left(\frac{p-1}{2} \right)} \equiv a^{\frac{p-1}{2}} \pmod{p}. \] Siden \[ a^{\frac{p-1}{2}} \equiv 1 \pmod{p}, \] følger det at \[ x^{t\left( \frac{p-1}{2} \right)} \equiv 1 \pmod{p}. \] %
%
Ut ifra Proposisjon \ref{ProposisjonOrdenenTilDelerEnHvilkenSomHelstPotensSomGir1ModuloP} har vi da: $\orden{x,p} \mid t\left( \frac{p-1}{2} \right)$. Siden $x$ er en primitiv rot modulo $p$, er $\orden{x,p} = p-1$. Da har vi: $p-1 \mid t \left( \frac{p-1}{2} \right)$. Dermed finnes det et heltall $k$ slik at \[ t \left( \frac{p-1}{2} \right) = k \cdot (p-1). \] Da er \[ t(p-1) = 2k \cdot (p-1). \] Det følger fra Proposisjon \ref{ProposisjonKanDeleBeggeSideneAvEnLigningMedEtHeltall} at $t=2k$. 

Vi har: \[ \left( x^{k} \right)^{2} = x^{2k} = x^{t} \equiv a \pmod{p}, \] altså \[ \left( x^{k} \right)^{2} \equiv a \pmod{p}. \] Med andre ord er $y=x^{k}$ en løsning til kongruensen \[ y^{2} \equiv a \pmod{p}. \] Dermed er $a$ en kvadratisk rot modulo $p$.
 
\end{proof}

\begin{terminologi} Proposisjon \ref{ProposisjonEulersKriterium} kalles {\em Eulers kriterium}. \end{terminologi}

\begin{eks} Ut ifra Eksempel \ref{EksempelKvadratiskeResterModulo11} er $5$ en kvadratisk rest modulo $11$. Proposisjon \ref{ProposisjonEulersKriterium} fastslår at \[ 5^{\frac{11-1}{2}} \equiv 1 \pmod{11}, \] altså at \[ 5^{5} \equiv 1 \pmod{11}. \] Vi har: \[ 5^{5} = \left( 5^{2} \right)^{2} \cdot 5 = 25^{2} \cdot 5 \equiv 3^{2} \cdot 5 = 9 \cdot 5 = 45 \equiv 1 \pmod{11}. \] Dermed er det riktignok sant at \[ 5^{5} \equiv 1 \pmod{11}. \]  \end{eks} 

\begin{eks} Ut ifra Eksempel \ref{EksempelKvadratiskeResterModulo5} er $3$ ikke en kvadratisk rest modulo $5$. Proposisjon \ref{ProposisjonEulersKriterium} fastslår at det ikke er sant at \[ 3^{\frac{5-1}{2}} \equiv 1 \pmod{5}, \] altså at det ikke er sant at \[ 3^{2} \equiv 1 \pmod{5}. \] Vi har:  $3^{2} = 9$ og \[ 9 \equiv 4 \pmod{5}. \] Siden det ikke er sant at \[ 4 \equiv 1 \pmod{5}, \] er det riktignok ikke sant at \[ 3^{2} \equiv 1 \pmod{5}. \] \end{eks} 

\begin{eks} \label{Eksempel3IkkeKvRestModulo31} Proposisjon \ref{ProposisjonEulersKriterium} fastslår at $3$ er en kvadratisk rest modulo $31$ hvis og bare hvis \[ 3^{\frac{31-1}{2}} \equiv 1 \pmod{31}, \] altså hvis og bare hvis \[ 3^{15} \equiv 1 \pmod{31}. \] Vi har: \[ 3^{3} = 27 \equiv -4 \pmod{31}. \] Da er %
%
\begin{align*} %
%
3^{15} &= 3^{9} \cdot 3^{6} \\
       &= \left(3^{3} \right)^{3} \cdot \left(3^{3} \right)^{2} \\
       &\equiv (-4)^{3} \cdot (-4)^{2} \\
       &= (-64) \cdot 16 \\
       &\equiv (-2) \cdot 16 \\
       &= -32 \\
       &\equiv -1 \pmod{31},
\end{align*} %
%
altså \[ 3^{15} \equiv -1 \pmod{31}. \] Vi konkluderer at $3$ ikke er en kvadratisk rest modulo $31$. \end{eks} 

\begin{eks} \label{EksempelMinus5KvRestModulo127} Proposisjon \ref{ProposisjonEulersKriterium} fastslår at $-5$ er en kvadratisk rest modulo $127$ hvis og bare hvis \[ (-5)^{\frac{127-1}{2}} \equiv 1 \pmod{127}, \] altså hvis og bare hvis \[ (-5)^{63} \equiv 1 \pmod{127}. \] Vi har: %
%
\begin{align*} %
%
(-5)^{63} &= - \left( 5^{3} \right)^{21} \\
          &= - (125)^{21} \\
          &\equiv - (-2)^{21} \\
          &=  - \left( \left( -2 \right)^{7} \right)^{3} \\
          &= - (-128)^{3} \\
          &\equiv - (-1)^{3} = - (-1) \\
          &= 1 \pmod{127}, 
\end{align*} %
%
altså \[ (-5)^{63} \equiv 1 \pmod{127}. \] Vi konkluderer at $5$ er en kvadratisk rest modulo $127$. \end{eks} 

\begin{merknad} De siste to eksemplene viser at Proposisjon \ref{ProposisjonEulersKriterium} er en kraftig verktøy for å bestemme om et heltall er eller ikke er en kvadratisk rest modulo et primtall. Argumentet i Eksempel \ref{Eksempel3IkkeKvRestModulo31} er å foretrekke fremfor å vise at det ikke er sant at \[ x^{2} \equiv 3 \pmod{31} \] for hvert av de naturlige tallene $1$, $2$, $\ldots$, $30$. Argumentet i Eksempel \ref{EksempelMinus5KvRestModulo127} er å foretrekke fremfor å gå gjennom alle de naturlige tallene $1$, $2$, $\ldots$, $126$ til vi finner ett som er kongruent til $5$ når vi opphøyer det i andre potens. 

Derimot måtte vi være litt kreative for å regne ut $3^{15}$ modulo $31$ og $(-5)^{63}$ modulo $127$ i disse to eksemplene. Det er ikke alltid lett å fullføre slike utregninger. 

Imidlertid kan vi gå videre. Vi kommer til å se at vi kan bygge på Proposisjon \ref{ProposisjonEulersKriterium} for å komme fram til en metode for å bestemme om et heltall er eller ikke er en kvadratisk rest modulo et primtall, uten å regne ut i det hele tatt.

Først kommer vi til å gi et annet eksempel, litt mer teoretisk, på hvordan Proposisjon \ref{ProposisjonEulersKriterium} kan benyttes i praksis. Vi må gjøre noen forberedelser.     
\end{merknad}

\begin{merknad} Følgende proposisjon er veldig enkel. Likevel er den svært nyttig: vi kommer til å benytte den ofte i dette kapittelet. \end{merknad}

\begin{prpn} \label{ProposisjonPlussEllerMinus1KongFoererTilLike} La $n$ være et naturlig tall slik at $n > 2$. La $a$ være $1$ eller $-1$. La $b$ være $1$ eller $-1$. Da er \[ a \equiv b \pmod{n} \] hvis og bare hvis $a=b$. \end{prpn}

\begin{proof} Ett av følgende utsagn er sant:  %
%
\begin{itemize}

\item[(A)] $a=1$ og $b=1$;

\item[(B)] $a=1$ og $b=-1$;

\item[(C)] $a=-1$ og $b=1$;

\item[(D)] $a=-1$ og $b=-1$. 

\end{itemize} %
%
Anta først at (B) er sant. Hvis \[ 1 \equiv -1 \pmod{n}, \] er \[ 2 \equiv 0 \pmod{n}. \] Da har vi: $n \mid 2$. Ut ifra Proposisjon \ref{ProposisjonNDeleligMedLImplisererLMindreEnnEllerLiktN} er da $n \leq 2$. Imidlertid har vi antatt at $n > 2$. Siden antakelsen at (B) er sant fører til denne motsigelsen, konkluderer vi at (B) ikke er sant. 

Anta nå at (C) er sant. Hvis \[ -1 \equiv 1 \pmod{n}, \] er \[ -2 \equiv 0 \pmod{n}. \] Da har vi: $n \mid -2$. Det følger fra Proposisjon \ref{ProposisjonNDeleligMedLImplisererMinusNDeleligMedL} at vi da har: $n \mid 2$. Ut ifra Proposisjon \ref{ProposisjonNDeleligMedLImplisererLMindreEnnEllerLiktN} er da $n \leq 2$. Imidlertid har vi antatt at $n > 2$. Siden antakelsen at (C) er sant fører til denne motsigelsen, konkluderer vi at (C) ikke er sant. 

Således har vi bevist at \[ a \equiv b \pmod{n} \] hvis og bare hvis enten (A) eller (B) sant, altså hvis og bare hvis $a=b$.           
 
\end{proof}

\begin{lem} \label{Lemma1OgMinus1EnesteLoesningeneTilXIKvadratKong1ModP} La $p$ være et primtall slik at $p > 2$. Da er følgende sanne: %
%
\begin{itemize} 

\item[(1)] $x=1$ og $x=-1$ er løsninger til kongruensen \[ x^{2} \equiv 1 \pmod{p}; \]

\item[(2)] det ikke er sant at \[ 1 \equiv -1 \pmod{p}. \]

\item[(3)] enhver annen løsning til kongruensen \[ x^{2} \equiv 1 \pmod{p} \] er kongruent modulo $p$ til enten $1$ eller $-1$;

\end{itemize}
\end{lem}

\begin{proof} Siden $1^{2} = 1$ og $(-1)^{2} = 1$, er (1) sant. Ut ifra Proposisjon \ref{ProposisjonPlussEllerMinus1KongFoererTilLike} er (2) sant. Siden (1) og (2) er sanne, følger det fra Proposisjon \ref{ProposisjonLagrangesTeorem} (II) at (3) er sant.   \end{proof}

\begin{kor} \label{KorollarEulersKriterium} La $p$ være et primtall slik at $p > 2$. La $a$ være et heltall slik at det ikke er sant at $p \mid a$. Da er $a$ ikke er en kvadratisk rest modulo $p$ hvis og bare hvis \[ a^{\frac{p-1}{2}} \equiv -1 \pmod{p}. \] \end{kor} 

\begin{proof} Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Vi har: \[ \left( a^{\frac{p-1}{2}} \right)^{2} = a^{2 \cdot \left( \frac{p-1}{2} \right)} = a^{p-1}. \] Ut ifra Korollar \ref{KorollarFermatsLilleTeoremPMinus1}, er \[ a^{p-1} \equiv 1 \pmod{p}. \] Dermed er \[ \left( a^{\frac{p-1}{2}} \right)^{2} \equiv 1 \pmod{p}. \] Da følger fra Lemma \ref{Lemma1OgMinus1EnesteLoesningeneTilXIKvadratKong1ModP} at enten \[ a^{\frac{p-1}{2}} \equiv 1 \pmod{p} \] eller \[ a^{\frac{p-1}{2}} \equiv -1 \pmod{p}. \] 

\item[(2)] Ut ifra Proposisjon \ref{ProposisjonEulersKriterium} er $a$ ikke en kvadratisk rest modulo $p$ hvis og bare hvis det ikke er sant at \[ a^{\frac{p-1}{2}} \equiv 1 \pmod{p}. \] 

\end{itemize} %
%
Det følger fra (1) og (2) at $a$ ikke er en kvadratisk rest modulo $p$ hvis og bare hvis \[ a^{\frac{p-1}{2}} \equiv -1 \pmod{p}. \]   

\end{proof}

\begin{eks} Ut ifra Eksempel \ref{EksempelKvadratiskeResterModulo11} er $6$ ikke en kvadratisk rest modulo $11$. Da fastslår Korollar \ref{KorollarEulersKriterium} at \[ 6^{\frac{11-1}{2}} \equiv -1 \pmod{11}, \] altså \[ 6^{5} \equiv -1 \pmod{11}. \] Dette er riktignok sant: \[ 6^{5} = \left( 6^{2} \right)^{2} \cdot 6 = 36^{2} \cdot 6 \equiv 3^{2} \cdot 6 = 54 \equiv -1 \pmod{11}. \] \end{eks} 

\begin{eks} Korollar \ref{KorollarEulersKriterium} fastslår at $10$ ikke er en kvadratisk rest modulo $23$ hvis og bare hvis \[ 10^{\frac{23-1}{2}} \equiv -1 \pmod{23}, \] altså hvis og bare hvis \[ 10^{11} \equiv -1 \pmod{23}. \] Vi har: \[ 10^{4} = \left( 10^{2} \right)^{2} = 100^{2} \equiv 8^{2} = 64 \equiv -5 \pmod{23}. \] I tillegg har vi: \[ 10^{3} = 10^{2} \cdot 10 \equiv 8 \cdot 10 = 80 \equiv 11 \pmod{23}. \] Dermed er \[ 10^{11} = \left( 10^{4} \right)^{2} \cdot 10^{3} \equiv (-5)^{2} \cdot 11 = 25 \cdot 11 \equiv 2 \cdot 11 = 22 \equiv -1 \pmod{23}. \] Vi konkluderer at $10$ ikke er en kvadratisk rest modulo $23$.  \end{eks} 

\begin{prpn} \label{ProposisjonMinus1KvadratiskRestHvisOgBareHvisPrimtalletErKongruentTil1Modulo4} La $p$ være et primtall slik at $p > 2$. Da er $-1$ en kvadratisk rest modulo $p$ hvis og bare hvis \[ p \equiv 1 \pmod{4}. \]   \end{prpn}

\begin{proof} Ut ifra Proposisjon \ref{ProposisjonAlleHeltalleneKongruentTilEtHeltallMindreEnnN} er ett av følgende utsagn sant. %
%
\begin{itemize}

\item[(A)] $p \equiv 0 \pmod{4}$;

\item[(B)] $p \equiv 1 \pmod{4}$;

\item[(C)] $p \equiv 2 \pmod{4}$;

\item[(D)] $p \equiv 3 \pmod{4}$.

\end{itemize} %
%
Anta først at (A) er sant. Da har vi: $4 \mid p$. Siden $p$ er et primtall, er $1$ og $p$ de eneste naturlige tallene som deler $p$. Dermed er $p=4$. Imidlertid er $4$ ikke et primtall. Siden antakelsen at (A) er sant fører til denne motsigelsen, konkluderer vi at (A) ikke er sant.    

Anta nå at (C) er sant. Siden $2 \mid 2$ og $2 \mid 4$, følger det da fra Proposisjon \ref{ProposisjonXKongYModNOgYOgNDeleligMedLFoererTilXKong0ModL} at \[ p \equiv 0 \pmod{2}. \] Derfor har vi: $2 \mid p$. Siden $p$ er et primtall, er $1$ og $p$ de eneste naturlige tallene som deler $p$. Dermed er $p=2$. Imidlertid har vi antatt at $p > 2$. Siden antakelsen at (C) er sant fører til denne motsigelsen, konkluderer vi at (C) ikke er sant. 

Anta nå at (D) er sant. Hvis \[ p \equiv 3 \pmod{4}, \] har vi: $4 \mid p-3$. Dermed finnes det et heltall $k$ slik at $p-3 = 4k$, altså slik at $\frac{p-3}{2} = 2k$. Da er %
%
\begin{align*} %
%
(-1)^{\frac{p-1}{2}} &= (-1)^{\frac{p-3}{2} + 1} \\
                     &= (-1)^{\frac{p-3}{2}} \cdot (-1)^{1} \\
                     &= (-1)^{2k} \cdot (-1) \\
                     &= \left( (-1)^{2} \right)^{k} \cdot (-1) \\
                     &= 1^{k} \cdot (-1) \\
                     &= 1 \cdot (-1) \\
                     &= -1.
\end{align*} %
%
Siden $p > 2$ og $-1 \not= 1$, følger det fra Proposisjon \ref{ProposisjonPlussEllerMinus1KongFoererTilLike} at det ikke er sant at \[ -1 \equiv 1 \pmod{p}. \] Dermed er det ikke sant at \[ (-1)^{\frac{p-1}{2}} \equiv 1 \pmod{p}. \] Det følger fra Proposisjon \ref{ProposisjonEulersKriterium} at $-1$ ikke er en kvadratisk rest modulo $p$.

Anta nå at (B) er sant. Hvis \[ p \equiv 1 \pmod{4}, \] har vi: $4 \mid p-1$. Dermed finnes det et heltall $k$ slik at $p-1 = 4k$, altså slik at $\frac{p-1}{2} = 2k$. Da er %
%
\begin{align*} %
%
(-1)^{\frac{p-1}{2}} &= (-1)^{2k} \\
                     &= \left( (-1)^{2} \right)^{k} \\
                     &= 1^{k}  \\
                     &= 1.
\end{align*} %
%
Det følger fra Proposisjon \ref{ProposisjonEulersKriterium} at $1$ er en kvadratisk rest modulo $p$.

\end{proof}

\begin{eks} Vi har: \[ 7 \equiv 3 \pmod{4}. \] Dermed er det ikke sant at \[ 7 \equiv 1 \pmod{4}. \] Da fastslår Proposisjon \ref{ProposisjonMinus1KvadratiskRestHvisOgBareHvisPrimtalletErKongruentTil1Modulo4} at $-1$ ikke er en kvadratisk rest modulo $7$. Dette er riktignok sant: hvis $-1$ hadde vært en kvadratisk rest, hadde så, ut ifra Proposisjon \ref{ProposisjonKanSjekkeOmKvadratiskRestVedAASePaaDeNaturligeTalleneMindreEnnEllerLikePMinus1}, $6$ vært en kvadratisk rest, og fra Eksempel \ref{EksempelKvadratiskeResterModulo7} vet vi at dette ikke er tilfellet. \end{eks}

\begin{eks} Vi har: \[ 13 \equiv 1 \pmod{4}. \] Da fastslår Proposisjon \ref{ProposisjonMinus1KvadratiskRestHvisOgBareHvisPrimtalletErKongruentTil1Modulo4} at $-1$ er en kvadratisk rest modulo $13$. Dette er riktignok sant: \[ 5^{2} = 25 \equiv -1 \pmod{13}. \] \end{eks}

\begin{prpn} \label{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4} La $n$ være et naturlig tall. Da finnes det et primtall $p$ slik at $p > n$ og \[ p \equiv 1 \pmod{4}. \]  \end{prpn}

\begin{proof} La $q$ være produktet av alle primtallene som er mindre enn eller like $n$, og som er kongruent til $1$ modulo $4$. Ut ifra Teorem \ref{TeoremAritmetikkensFundamentalteoremI}, finnes det et naturlig tall $t$ og primtall $p_{1}$, $\ldots$, $p_{t}$ slik at \[ (2q)^{2}+1 = p_{1} \cdots p_{t}. \] %
%
Anta at \[ p_{1} \equiv 0 \pmod{2}. \] Da er \[ p_{1} \cdots p_{t} \equiv 0 \cdot \left( p_{2} \cdots p_{t} \right) \pmod{2}, \] altså \[ (2q)^{2} + 1 \equiv 0 \pmod{2}. \] Siden $2 \mid (2q)^{2}$, er imidlertid \[ (2q)^{2} + 1 \equiv 1 \pmod{2}. \] Ut ifra Proposisjon \ref{ProposisjonResterEquivModNHvisOgBareHvisLik} kan det ikke være sant at både \[ (2q)^{2} + 1 \equiv 0 \pmod{2} \] og \[  (2q)^{2} + 1 \equiv 1 \pmod{2}. \] Siden antakelsen at \[ p_{1} \equiv 0 \pmod{2} \] fører til denne motsigelsen, konkluderer vi at det ikke er sant at \[ p_{1} \equiv 0 \pmod{2}. \] Vi konkluderer at $p_{1} > 2$. 

Siden \[ (2q)^{2} + 1 =  \left( p_{2} \cdots p_{t} \right) p_{1}, \] har vi: $p_{1} \mid (2q)^{2} + 1$. Dermed er \[ (2q)^{2} \equiv -1 \pmod{p_{1}}, \] altså $-1$ er en kvadratisk rest modulo $p_{1}$. Siden $p_{1}$ er et primtall og $p > 2$, følger det fra Proposisjon \ref{ProposisjonMinus1KvadratiskRestHvisOgBareHvisPrimtalletErKongruentTil1Modulo4} at \[ p_{1} \equiv 1 \pmod{4}. \]

Anta at $p_{1} \leq n$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Ut ifra definisjonen til $q$, følger det da at $p_{1} \mid q$. Fra Korollar \ref{KorollarNOgMDeleligeMedLImplisererMNDeleligMedL} har vi da: $p_{1} \mid q \cdot (-4q)$, altså $p_{1} \mid -(2q)^{2}$.

\item[(2)]  Siden vi i tillegg vet at $p_{1} \mid (2q)^{2} + 1$, følger det fra (1) og Proposisjon \ref{ProposisjonNOgMDeleligeMedLImplisererMPlussNDeleligMedL} at $p_{1} \mid \big( (2q)^{2} + 1 \big) + \big( (-(2q)^{2} \big)$, altså at $p_{1} \mid 1$. 

\end{itemize} %
%
Det kan ikke være sant at både $p_{1} \mid 1$ og $p_{1} > 2$. Siden antakelsen at $p_{1} \leq n$ fører til denne motsigelsen, konkluderer vi at det ikke er sant at $p_{1} \leq n$. Derfor er $p_{1} > n$.     

\end{proof}

\begin{merknad} Med andre ord fastslår Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4} at det finnes uendelig mange primtall som er kongruent til $1$ modulo $4$. Sammenlign med Teorem \ref{TeoremUendeligMangePrimtall}, Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil3Modulo4}, og Oppgave \ref{OppgaveUendeligMangePrimtallKongruentTil5Modulo6}.  \end{merknad}

\begin{merknad} Det er ikke noe spesielt med $p_{1}$ i beviset for Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4}. Det samme argumentet viser at $p_{i} > n$ for alle primtallene $p_{1}$, $p_{2}$, $\ldots$, $p_{t}$ som dukker opp i primtallsfaktorisingen til $(2q)^{2}+1$ i beviset. \end{merknad} 

\begin{eks} \label{EksempelUendeligMangePrimtallKong1Mod4NLik30} La oss gå gjennom beviset for Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4} når $n=30$. Det finnes fire primtall som er mindre enn eller likt $30$ og som er kongruent til $1$ modulo $4$, nemlig $5$, $13$, $17$, og $29$. La $q$ være produktet av disse primtallene, altså \[ q=5 \cdot 13 \cdot 17 \cdot 29. \] Da er $(2q)^{2}+1$ likt $4107528101$. Beviset for Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4} fastslår at hvert primtall i en primtallsfaktorisering av $(2q)^{2}+1$, altså av $4107528101$, er større enn $30$. Vi har: \[ 4107528101 = 37 \cdot 173 \cdot 641701, \] og både $37$, $173$, og $641701$ er primtall. Med andre ord, er primtallet $p_{1}$ i beviset for Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4} likt $37$ i dette tilfellet: det er riktignok sant at $37 > 30$. \end{eks} 

\begin{merknad} Kanskje ser beviset for Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4} lettere ut enn beviset for Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil3Modulo4}. Imidlertid er dette villedende: beviset for Proposisjon \ref{ProposisjonUendeligMangePrimtallKongruentTil1Modulo4} bygger på Proposisjon \ref{ProposisjonEulersKriterium}, som er et ganske dypt resultat.   \end{merknad}
%forelesning19A
