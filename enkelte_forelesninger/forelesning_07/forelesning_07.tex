%forelesning07
\section{Absoluttverdien}

\begin{defn} \label{DefinisjonAbsoluttverdien} La $n$ være et heltall. Da er {\em absoluttverdien til $n$}: %
%
\begin{itemize}

\item[(1)] $n$ dersom $n \geq 0$;

\item[(2)] $-n$ dersom $n < 0$.

\end{itemize} 

\end{defn}

\begin{merknad} Med andre ord får vi absoluttverdien til $n$ ved å fjerne minustegnet hvis $n < 0$, og ved å gjøre ingenting hvis $n > 0$. \end{merknad}

\begin{notn} La $n$ være et heltall. Vi betegner absoluttverdien til $n$ som $\left| n \right|$.  \end{notn}

\begin{eks} Vi har: $\left| 3 \right| = 3$. \end{eks}

\begin{eks} Vi har: $\left| -3 \right| = 3$. \end{eks}

\begin{eks} Vi har: $\left| 0 \right| = 0$. \end{eks}

\begin{eks} Vi har: $\left| -7 \right| = 7$. \end{eks}

\begin{eks} Vi har: $\left| 151 \right| = 151$. \end{eks}

\section{Divisjonsalgoritmen} \label{DelDivisjonsalgoritmen}

\begin{merknad} \label{MerknadDivisjonBarneskolen} La $l$ og $n$ være naturlige tall. Fra barneskolen kjenner du til at vi alltid kan finne et naturlig tall $k$ og et naturlig tall $r$ slik at: %
%
\begin{itemize} 

\item[(1)] $n = kl + r$,

\item[(2)] $0 \leq r < l$. 

\end{itemize} %
%
Det naturlige tallet $k$ kalles {\em kvotient}, og det naturlige tallet $r$ kalles {\em rest}. \end{merknad}

\begin{eks} La $n$ være $5$, og la $l$ være $3$. Da er $k=1$ og $r=2$, siden vi har: %
%
\begin{itemize}

\item[(1)] $5 = 1 \cdot 3 + 2$,

\item[(2)] $0 \leq 2 < 3$.

\end{itemize}
\end{eks}

\begin{eks} La $n$ være $18$, og la $l$ være $5$. Da er $k=3$ og $r=3$, siden vi har: %
%
\begin{itemize}

\item[(1)] $18 = 3 \cdot 5 + 3$,

\item[(2)] $0 \leq 3 < 5$.

\end{itemize}

\end{eks}

\begin{merknad} På barneskolen lærte du en metode for å finne $k$ og $r$. Men hvordan vet vi at metoden alltid virker? Med andre ord, hvordan vet vi at vi alltid kan finne naturlige tall $k$ og $r$ som oppfyller kravene (1) og (2) i Merknad \ref{MerknadDivisjonBarneskolen}? 

I denne delen av kapittelet skal vi {\em bevise} ved induskjon at det finnes, for alle naturlige tall $n$ og $l$, naturlige tall $k$ og $r$ slik at (1) og (2) i Merknad \ref{MerknadDivisjonBarneskolen} er sanne. Det følgende lemmaet er kjernen i beviset for Proposisjon \ref{ProposisjonDivisjonsalgoritme}. \end{merknad}

\begin{lem} \label{LemmaDivisjonsalgoritmen} La $n$ være et heltall slik at $n \geq 0$. La $l$ være et naturlig tall. Anta at det finnes et heltall $k$ og et heltall $r$ slik at: %
%
\begin{itemize}

\item[(1)] $n=kl + r$,

\item[(2)] $0 \leq r < l$,

\item[(3)] $k \geq 0$.

\end{itemize} %
%
Da finnes det et heltall $k'$ og et heltall $r'$ slik at: %
%
\begin{itemize}

\item[(I)] $n+1=k'l + r'$,

\item[(II)] $0 \leq r' < l$.

\item[(III)] $k' \geq 0$.

\end{itemize} 
 
\end{lem}

\begin{proof} Siden $0 \leq r < l$, er et av de følgende utsagnene sant: %
%
\begin{itemize}

\item[(A)] $r < l-1$;

\item[(B)] $r=l-1$. 

\end{itemize} %
%
Vi skal gjennomføre beviset i disse to tilfellene hver for seg. %

Anta først at (A) er tilfellet. La da $k'$ være $k$, og la $r'$ være $r+1$. Vi gjør følgende observasjoner. %
%
\begin{itemize} %
%
\item[(i)] Fra (1) har vi: \[ n + 1 = (kl +r) + 1. \] Derfor er: %
%
\begin{align*} %
%
n+1 &= (kl +r) + 1 \\ 
    &= kl + (r+1) \\ 
    &= k'l + r'. 
\end{align*} %
%
Dermed oppfyller $k'$ og $r'$ kravet (I). 

\item[(ii)] Fra (2) har vi: \[ 0 \leq r. \] Derfor er %
%
\begin{align*} %
%
0 &\leq r \\
  &\leq r+1 \\
  &= r'.
\end{align*} %
%
Siden vi har antatt at (A) er sant, vet vi også at \[ r < l-1. \] Det følger at \[ r+1 < l, \] altså at \[ r' < l. \] %
%
Dermed har vi bevist at \[ 0 \leq r' < l. \] Således oppfyller $r'$ kravet (II). 

\item[(iii)] Fra (3) har vi: \[ k \geq 0. \] Siden $k' = k$, har vi altså: \[ k' \geq 0. \] Dermed oppfyller $k'$ kravet (III).  

\end{itemize} %
%
Fra (i) -- (iii) konkluderer vi at lemmaet er sant i tilfellet (A).

Anta nå at (B) er tilfellet. La da $k'$ være $k+1$, og la $r'$ være $0$. Vi gjør følgende observasjoner. %
%
\begin{itemize} %
%
\item[(i)] Fra (1) har vi: \[ n + 1 = (kl +r) + 1. \] Siden vi har antatt at (B) er sant, er $r=l-1$. Derfor er %
%
\begin{align*} %
%
n+1 &= (kl +r) + 1 \\ 
    &= \big( kl + (l-1) \big) + 1  \\ 
    &= kl +l - 1 + 1 \\
    &= (k+1)l + 0 \\
    &= k'l + r'.
\end{align*} %
%
Dermed oppfyller $k'$ og $r'$ kravet (I). 

\item[(ii)] Siden $l$ er et naturlig tall, er $0 < l$. Siden $r'=0$, er derfor $r' < l$. I tillegg er $0 \leq  0$, altså $0 \leq r'$. Dermed er \[ 0 \leq r' < l. \] Således oppfyller $r'$ kravet (II). 

\item[(iii)] Fra (3) har vi: \[ k \geq 0. \] Siden $k' = k+1$, deduserer vi at \[ k' \geq 0. \] Dermed oppfyller $k'$ kravet (III).  

\end{itemize} %
%
Fra (i) -- (iii) konkluderer vi at lemmaet er sant i tilfellet (B).

\end{proof}

\begin{prpn} \label{ProposisjonDivisjonsalgoritme} La $n$ være et heltall slik at $n \geq 0$. La $l$ være et naturlig tall. Da finnes det et heltall $k$ og et heltall $r$ slik at:

\begin{itemize}

\item[(I)] $n=kl + r$,

\item[(II)] $0 \leq r < l$,

\item[(III)] $k \geq 0$.

\end{itemize}

\end{prpn}

\begin{proof} Først sjekker vi at proposisjonen er sann når $n=0$. I dette tilfellet er utsagnet at det finnes, for et hvilket som helst naturlig tall $l$, et heltall $k$ og et heltall $r$ slik at:

\begin{itemize}

\item[(1)] $0=kl + r$

\item[(2)] $0 \leq r < l$,

\item[(3)] $k \geq 0$.

\end{itemize} %
%
La $k$ være $0$, og la $r$ være $0$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(i)] Vi har: %
%
\begin{align*} %
%
kl + r &= 0 \cdot l + 0 \\
       &= 0 + 0 \\
       &= 0.
\end{align*} %
%
Dermed oppfyller $k$ og $r$ kravet (1).

\item[(ii)] Siden $l$ er et naturlig tall, er $0 < l$. Siden $r=0$, er derfor $r < l$. I tillegg er $0 \leq 0$, altså $0 \leq r$. Dermed er \[ 0 \leq r < l. \] Således oppfyller $r$ kravet (2).

\item[(iii)] Vi har: $0 \geq 0$. Siden $k=0$, er derfor $k \geq 0$. Dermed oppfyller $k$ kravet (3).
    
\end{itemize} %
%
Fra (i) -- (iii) konkluderer vi at utsagnet er sant.

Anta nå at proposisjonen har blitt bevist når $n$ er et gitt helltall $m$ slik at $m \geq 0$. Således har det blitt bevist at det finnes, for et hvilket som helst naturlig tall $l$, et heltall $k$ og et heltall $r$ slik at: %
%
\begin{itemize}

\item[(1)] $m=kl + r$,

\item[(2)] $0 \leq r < l$,

\item[(3)] $k \geq 0$.

\end{itemize} %
%
Da følger det fra Lemma \ref{LemmaDivisjonsalgoritmen} at det finnes et heltall $k'$ og et heltall $r'$ slik at: %
%
\begin{itemize}

\item[(1)] $m+1=k'l + r'$,

\item[(2)] $0 \leq r' < l$,

\item[(3)] $k' \geq 0$.

\end{itemize} %
%
Dermed er proposisjonen sann når $n=m+1$. 

Ved induksjon konkluderer vi at proposisjonen er sann når $n$ er et hvilket som helst naturlig tall.
\end{proof}

\begin{terminologi} I Merknad \ref{MerknadInduksjonIde} så vi at induksjon gir en algoritme for å konstruere et bevis for en matematisk påstand. Således gir beviset for Proposisjon \ref{ProposisjonDivisjonsalgoritme} en algoritme for å finne $k$ og $r$. Denne algoritmen kalles noen ganger {\em divisjonsalgoritmen}. \end{terminologi}

\begin{eks} \label{EksempelDivisjonsalgoritmeNLik3LLik2} La oss se hvordan divisjonsalgoritmen ser ut når $n=3$ og $l=2$. %
%
\begin{itemize} %
%
\item[(1)] Vi begynner med å observere at: \[ 0 = 0 \cdot 2 + 0. \] 

\item[(2)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 1 = 0 \cdot 2 + 1. \]

\item[(3)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (B), observerer vi at det følger at \[ 2 = 1 \cdot 2 + 0. \] 

\item[(4)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 3 = 1 \cdot 2 + 1. \] 

\end{itemize} %
%
Dermed er $k=1$ og $r=1$. 

\end{eks}

\begin{eks} La oss se hvordan divisjonsalgoritmen ser ut når $n=6$ og $l=4$. %
%
\begin{itemize} %
%
\item[(1)] Vi begynner med å observere at: \[ 0 = 0 \cdot 4 + 0. \] 

\item[(2)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 1 = 0 \cdot 4 + 1. \]

\item[(3)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 2 = 0 \cdot 4 + 2. \] 

\item[(4)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 3 = 0 \cdot 4 + 3. \] 

\item[(5)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (B), observerer vi at det følger at \[ 4 = 1 \cdot 4 + 0. \] 

\item[(6)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 5 = 1 \cdot 4 + 1. \] 

\item[(7)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 6 = 1 \cdot 4 + 2. \] 

\end{itemize} %
%
Dermed er $k=1$ og $r=2$. 

\end{eks}

\begin{eks} \label{EksempelDivisjonsalgoritmeNLik7LLik3} La oss se hvordan divisjonsalgoritmen ser ut når $n=7$ og $l=3$. %
%
\begin{itemize} %
%
\item[(1)] Vi begynner med å observere at: \[ 0 = 0 \cdot 3 + 0. \] 

\item[(2)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 1 = 0 \cdot 3 + 1. \]

\item[(3)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 2 = 0 \cdot 3 + 2. \] 

\item[(4)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (B), observerer vi at det følger at \[ 3 = 1 \cdot 3 + 0. \] 

\item[(5)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 4 = 1 \cdot 3 + 1. \] 

\item[(6)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 5 = 1 \cdot 3 + 2. \] 

\item[(7)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (B), observerer vi at det følger at \[ 6 = 2 \cdot 3 + 0. \] 

\item[(8)] Som i beviset for Lemma \ref{LemmaDivisjonsalgoritmen} i tilfellet (A), observerer vi at det følger at \[ 7 = 2 \cdot 3 + 1. \] 

\end{itemize} %
%
Dermed er $k=2$ og $r=1$. 

\end{eks}

\begin{kor} \label{KorollarDivisjonsalgoritmeHeltall} La $n$ være et heltall. La $l$ være et heltall slik at $l \not= 0$. Da finnes det et heltall $k$ og et heltall $r$ slik at:

\begin{itemize}

\item[(I)] $n=kl + r$,

\item[(II)] $0 \leq r < \left| l \right| $.

\end{itemize}

\end{kor}

\begin{proof} Ett av følgende utsagn er sant: %
%
\begin{itemize}

\item[(A)] $l>0$ og $n \geq 0$; 

\item[(B)] $l<0$ og $n \geq 0$;

\item[(C)] $l>0$ og $n < 0$;

\item[(D)] $l<0$ og $n < 0$;

\end{itemize} %
%
Anta først at (A) er tilfellet. Da er $l$ et naturlig tall. Det følger fra Proposisjon \ref{ProposisjonDivisjonsalgoritme} at det finnes et heltall $k'$ og et heltall $r'$ slik at:

\begin{itemize}

\item[(i)] $n=k'l + r'$,

\item[(ii)] $0 \leq r' < l$.

\end{itemize} %
%
Siden $\left| l \right| = l$, deduserer vi at proposisjonen er sann i dette tilfellet, ved å la $k$ være $k'$ og å la $r$ være $r'$.

Anta nå at (B) er tilfellet. Da er $-l$ et naturlig tall. Det følger fra Proposisjon \ref{ProposisjonDivisjonsalgoritme} at det finnes et heltall $k'$ og et heltall $r'$ slik at:

\begin{itemize}

\item[(i)] $n=k' \cdot (-l) + r'$,

\item[(ii)] $0 \leq r' < -l$.

\end{itemize} %
%
Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Det følger fra (i) at \[ n = (-k') \cdot l + r'. \] 

\item[(2)] Vi har: $\left| l \right| = -l$. Derfor følger det fra (ii) at \[ 0 \leq r' < \left| l \right|. \]

\end{itemize} %
%
Dermed er proposisjonen sann i dette tilfellet også, ved å la $k$ være $-k'$ og å la $r$ være $r'$.
 
Anta nå at (C) er tilfellet. Da er $-n \geq 0$. Det følger fra Proposisjon \ref{ProposisjonDivisjonsalgoritme} at det finnes et heltall $k'$ og et heltall $r'$ slik at:

\begin{itemize}

\item[(i)] $-n=k' \cdot l + r'$,

\item[(ii)] $0 \leq r' < l$.

\end{itemize} %
%
Ett av følgende utsagn er sant. %
%
\begin{itemize} 

\item[(a)] $r'=0$.

\item[(b)] $0 < r' < l$.

\end{itemize} %
%
Anta først at $r'=0$. Det følger fra (i) at \[ n = (-k') \cdot l. \] Dermed er proposisjonen sann i dette tilfellet, ved å la $k$ være $k'$, og å la $r$ være $0$.

Anta nå at $0 < r' < l$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Det følger fra (i) at %
%
\begin{align*} %
%
n &= -k' \cdot l - r' \\
  &= -k' \cdot l - l + l -r' \\
  &= (-k'-1) \cdot l + (l-r').  
\end{align*} 

\item[(2)] Siden $0 < r' < l$, er $0 < l -r' < l$.

\end{itemize} %
%
Dermed er proposisjonen sann i dette tilfellet også, ved å la $k$ være $-k'-1$, og å la $r$ være $l-r'$.
 Således har vi bevist at proposisjonen er sann i tilfellet (C).  

Anta nå at (D) er tilfellet. Da er $-n \geq 0$. I tillegg er $-l$ et naturlig tall. Det følger fra Proposisjon \ref{ProposisjonDivisjonsalgoritme} at det finnes et heltall $k'$ og et heltall $r'$ slik at:

\begin{itemize}

\item[(i)] $-n=k' \cdot (-l) + r'$,

\item[(ii)] $0 \leq r' < -l$.

\end{itemize} %
%
Ett av følgende utsagn er sant. %
%
\begin{itemize} 

\item[(a)] $r'=0$.

\item[(b)] $0 < r' < -l$.

\end{itemize} %
%
Anta først at $r'=0$. Det følger fra (i) at \[ n = k' \cdot l. \] I tillegg har vi: $\left| l \right| = -l$. Dermed er proposisjonen sann i dette tilfellet, ved å la $k$ være $k'$, og å la $r$ være $0$.

Anta nå at $0 < r' < -l$. Vi gjør følgende observasjoner. %
%
\begin{itemize}

\item[(1)] Det følger fra (i) at %
%
\begin{align*} %
%
n &= k' \cdot l - r' \\
  &= k' \cdot l + l - l -r' \\
  &= (k'+1) \cdot l + (-l - r').
\end{align*}  

\item[(2)] Siden $0 < r' < -l$, er $0 < -l -r' < -l$.

\item[(3)] Vi har: $\left| l \right| = -l$. Derfor følger det fra (2) at \[ 0 < -l - r' < \left| l \right|. \]

\end{itemize} %
%
Fra (1) og (3) konkluderer vi at proposisjonen er sann i dette tilfellet også, ved å la $k$ være $k'+1$, og å la $r$ være $-l-r'$. Således har vi bevist at proposisjonen er sann i tilfellet (D). 

\end{proof}

\begin{eks} \label{EksempelDivisjonsalgoritmenNLikMinus5LLik2} La $n =-5$, og la $l$ være $2$. For å få heltall $k$ og $r$ slik at \[ -5 = k \cdot 2 + r \] og $0 \leq r < 2$, fastslår beviset for Korollar \ref{KorollarDivisjonsalgoritmeHeltall} at vi kan gjøre følgende. %
%
\begin{itemize}

\item[(1)] Benytt divisjonsalgoritmen i tilfellet $n=5$ og $l=2$. Vi hopper over detaljene. Resultatet er: \[ 5 = 2 \cdot 2 + 1. \]

\item[(2)] Observer at det følger fra (1) at %
%
\begin{align*} %
%
-5 &= -2 \cdot 2 - 1 \\
   &= -2 \cdot 2 - 2 + 2 -1 \\
   &= (-2-1) \cdot 2 + (2-1) \\
   &= (-3) \cdot 2 + 1.
\end{align*} 

\end{itemize} %
%
Dermed er \[ -5 = (-3) \cdot 2 + 1, \] og $0 \leq 1 < 2$. Således er $k=-3$ og $r=1$.

\end{eks}

\begin{eks} La $n = 8$, og la $l$ være $-3$. For å få heltall $k$ og $r$ slik at \[ 8 = k \cdot (-3) + r \] og $0 \leq r < 3$, fastslår beviset for Korollar \ref{KorollarDivisjonsalgoritmeHeltall} at vi kan gjøre følgende. %
%
\begin{itemize}

\item[(1)] Benytt divisjonsalgoritmen i tilfellet $n=8$ og $l=3$. Vi hopper over detaljene. Resultatet er: \[ 8 = 2 \cdot 3 + 2. \]

\item[(2)] Observer at det følger fra (1) at \[ 8 = (-2) \cdot (-3) + 2. \]

\end{itemize} %
%
I tillegg er $0 \leq 2 < 3$. Således er $k=-2$ og $r=2$.

 \end{eks}

\begin{eks} \label{EksempelDivisjonsalgoritmenNLikMinus7LLikMinus4} La $n =-7$, og la $l$ være $-4$. For å få heltall $k$ og $r$ slik at \[ -7 = k \cdot (-4) + r \] og $0 \leq r < 4$, fastslår beviset for Korollar \ref{KorollarDivisjonsalgoritmeHeltall} at vi kan gjøre følgende. %
%
\begin{itemize}

\item[(1)] Benytt divisjonsalgoritmen i tilfellet $n=7$ og $l=4$. Vi hopper over detaljene. Resultatet er: \[ 7 = 1 \cdot 4 + 3. \]

\item[(2)] Observer at det følger fra (1) at %
%
\begin{align*} %
%
-7 &= 1 \cdot (-4) - 3 \\
   &= 1 \cdot (-4) + (-4) - (-4) -3 \\
   &= (1+1) \cdot (-4) + (4-3) \\
   &= 2 \cdot (-4) + 1.
\end{align*} 

\end{itemize} %
%
Dermed er \[ -7 = 2 \cdot (-4) + 1, \] og $0 \leq 1 < 4$. Således er $k=2$ og $r=1$.

\end{eks}
%forelesning07
